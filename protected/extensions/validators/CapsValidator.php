<?php

class CapsValidator extends CValidator
{
    public $coeff = 1.5;        // Сколько заглавных букв может быть в одном слове

    protected function validateAttribute($object, $attribute)
    {
        $str = $object->$attribute;
        $wordsArr = preg_split('/[\s,.]+/imxu', $str);           // Количество слов
        $diffCount = 0;
        for ($i=0; $i < mb_strlen($str, 'UTF-8'); $i++) {
            $char = mb_substr($str, $i, 1, 'UTF-8');
            if ($char != mb_strtolower($char, 'UTF-8'))
                $diffCount++;
        }
        $diffCount = floor($diffCount - count($wordsArr) * $this->coeff);     // Считаем количество измененных букв
        if ($diffCount > 0)
            $this->addError($object, $attribute, Yii::t('admin', 'Слишком много заглавных букв в поле {attribute}', [
                '{attribute}' => $object->getAttributeLabel($attribute)
            ]));
    }
}
