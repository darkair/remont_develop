<?php

/**
 * Список документов
 */
class DocumentsList extends CActiveRecord
{
    const TYPE_NONE         = 0;
    const TYPE_GRATITUDE    = 1;            // Свидетельства
    const TYPE_CERTIFICATE  = 2;            // Благодарности

    public $_docs = [];                     // UploadedFile[]
    public $_removeDocsFlags = [];          // bool

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'documentslist';
    }

    public function behaviors()
    {
        return array(
            'documentsListBehavior' => array(
                'class'                 => 'application.behaviors.DocumentsListBehavior',
                'storagePath'           => 'documentsList',
                'docsField'             => 'docs',
                'documentsItemClass'    => 'DocumentsItem',
            ),
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'DocumentsListLang',
                'langTableName'         => 'documentslist_lang',
                'langForeignKey'        => 'documentsListId',
                'localizedAttributes'   => array('title'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->documentsListBehavior->documentsListLabels(),
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Заголовок списка',
            )),
            array(
                'type' => 'Тип',
                'visible'   => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->documentsListBehavior->documentsListRules(),
            array(
                array('title', 'safe'),
                array('type', 'numerical'),
                array('visible', 'boolean'),
                array('title', 'ext.validators.CapsValidator'),

                array('title, visible', 'safe', 'on'=>'search'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function byType($type)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.type = '.$type,
        ));
        return $this;
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.visible', $this->visible);
        $criteria->compare($alias.'.title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getDocumentsStorePath()
    {
        return $this->documentsListBehavior->getStorePath();
    }

    public function getDocumentAbsoluteUrl($filename)
    {
        return $this->documentsListBehavior->getDocumentAbsoluteUrl($filename);
    }

    public function getDocumentsItems()
    {
        return $this->documentsListBehavior->getDocumentsItems();
    }

    public function getDocumentOriginalFileName($filename)
    {
        return $this->documentsListBehavior->getDocumentOriginalFileName($filename);
    }

    public function getDocumentFileNameByFilePart($filePart)
    {
        return $this->documentsListBehavior->getDocumentFileNameByFilePart($filePart);
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }
}
