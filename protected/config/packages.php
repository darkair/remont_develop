<?php
return [                
    'react' => [
        'baseUrl' => 'https://fb.me/',
        'js' => [
            'react-15.0.0.js',
            'react-dom-15.0.0.js',
        ],
        'depends' => [
            'babel',
        ]
    ],
    'babel' => [
        'baseUrl' => '//cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.34/',
        'js' => [
            'browser.min.js'
        ]
    ],
    'tween-max' => [
        'baseUrl' => '//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/',
        'js' => [
            'TweenMax.min.js',
            'plugins/ColorPropsPlugin.min.js',
            'plugins/CSSRulePlugin.min.js',
            'plugins/EaselPlugin.min.js',
        ]
    ],
    'animate' => [
        'baseUrl' => '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/',
        'css' => [
            'animate.min.css',
        ]
    ],
    'fotorama' => [
        'baseUrl' => '//cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/',
        'js' => [
            'fotorama.min.js',
        ],
        'css' => [
            'fotorama.min.css',
        ]
    ],

    'viewport-checker' => [
        'basePath' => 'ext.viewport-checker',
        'js' => [
            'src/jquery.viewportchecker.js',
        ],
        'depends' => [
            'jquery',
            'animate'
        ]
    ],
    'html2image' => [
        'basePath' => 'ext.html2image',
        'js' => [
            'html2canvas.min.js',
            'canvas2image.js'
        ],
    ],
    'underscore' => [
        'basePath' => 'application.views.js',
        'js' => [
            'underscore.js'
        ]
    ],
    'debug' => [
        'basePath' => 'application.views.js',
        'js' => [
            'debug.js'
        ]
    ]
];