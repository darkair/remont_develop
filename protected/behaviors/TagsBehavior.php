<?php

class TagsBehavior extends CActiveRecordBehavior
{
    public $className = 'Tags';
    public $tagsField = 'tags';
    public $innerTagsField = '_tags';
    public $tag2objTable = '';
    public $label = 'Теги';

    public function tagsLabels()
    {
        return array(
            $this->tagsField => $this->label,
        );
    }

    public function tagsRules()
    {
        return array(
            array($this->innerTagsField, 'safe'),
        );
    }

    public function afterFind($event)
    {
        if (Yii::app()->request->getParam($this->innerTagsField) !== null)
            return;

        $arr = [];
        foreach ($this->owner->{$this->tagsField} as $tag) {
            $arr[] = $tag->name; 
        }
        $this->owner->{$this->innerTagsField} = implode(',', $arr);
    }

    public function afterSave($event)
    {
        if (empty($this->tag2objTable))
            return;

        $class = $this->className;

        $newTags = empty($this->owner->{$this->innerTagsField})
            ? []
            : explode(',', $this->owner->{$this->innerTagsField});
        array_walk($newTags, function(&$item) {
            $item = trim($item);
        });
        $removeTagsIds = [];

        foreach ($this->owner->{$this->tagsField} as $tag) {
            $isExist = false;
            foreach ($newTags as $k=>$tagName) {
                if ($tag->name == $tagName) {
                    unset($newTags[$k]);
                    $isExist = true;
                    break;
                }
            }
            if (!$isExist)
                $removeTagsIds[] = $tag->id;
        }

        // Remove old links
        $criteria=new CDbCriteria();
        $criteria->addInCondition('tagId', $removeTagsIds)
                 ->addColumnCondition(array('objId'=>$this->owner->getPrimaryKey()));
        $this->owner->commandBuilder->createDeleteCommand($this->tag2objTable, $criteria)->execute();

        // add new entries to relation table
        $newTagsIds = [];
        foreach ($newTags as $tagName) {
            $tag = $class::model()->byName($tagName)->find();
            if (!$tag) {
                $tag = new $class();
                $tag->name = $tagName;
                if (!$tag->save())
                    throw new CHttpException(500, 'Unable to save tag <'.$tagName.'>');
            }
            $newTagsIds[] = $tag->id;
        }
        foreach($newTagsIds as $tagId) {
            $this->owner->commandBuilder->createInsertCommand($this->tag2objTable, array(
                'objId' => $this->owner->getPrimaryKey(),
                'tagId' => $tagId,
            ))->execute();
        }
    }

    public function getAllTags()
    {
        $class = $this->className;
        
        $criteria = new CDbCriteria;
        $criteria->alias = 't';
        $criteria->join = 'right join '.$this->tag2objTable.' as t2o on t2o.tagId = t.id';
        $criteria->group = 't2o.tagId';
        return $class::model()->findAll($criteria);
    }

    public function getAllTagsName()
    {
        $tags = $this->getAllTags();
        return array_map(function($tag) {
            return $tag->name;
        }, $tags);
    }
}
