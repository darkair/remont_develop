<?php
/**
 * Редактируемый список дочерних элементов
 */
class AdminChildObjListWidget extends ExtendedWidget
{
    public $form;
    public $model;
    public $attribute;
    public $innerChildField = '';
    public $valueField = 'id';
    public $visibleField = 'visible';
    public $textField = 'title';
    public $childClass = null;
    public $editUrl = '';

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('bootbox');
        AdminComponent::getInstance()->assetsRegistry->registerPackage('jquery.ui');
    }

    public function run()
    {
        if (empty($this->model) || !is_object($this->model)) {
            echo 'Error: model incorrect';
            return;
        }

        $childClass = $this->childClass;

        // Берем все объекты, несмотря на их видимость, чтобы можно было на клиенте решать, что же с ними делать
        $listData = [];
        foreach ($childClass::model() as $model) {
            $value      = CHtml::value($model, $this->valueField);
            $text       = CHtml::value($model, $this->textField);
            $visible    = CHtml::value($model, $this->visibleField);
            $listData[$value] = ['text' => $text, 'visible'=>$visible];
        }

        $this->render('childObjList', array(
            'form'                  => $this->form,
            'model'                 => $this->model,
            'modelName'             => get_class($this->model),
            'attribute'             => $this->attribute,
            'data'                  => $this->model->{$this->attribute},
            'selectData'            => $listData,      
            'innerChildField'       => $this->innerChildField,
            'valueField'            => $this->valueField,
            'visibleField'          => $this->visibleField,
            'textField'             => $this->textField,
            'childClass'            => $this->childClass,
            'editUrl'               => $this->editUrl,
        ));
    }
}
