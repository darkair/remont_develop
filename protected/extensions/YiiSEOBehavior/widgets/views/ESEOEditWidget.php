<fieldset class='form-group panel'>
    <legend>Seo</legend>

<!--
    <div class="row">
        <div class='control-label col-sm-3'>
            <?php echo CHtml::activeLabel($model->seoData, 'title'); ?>
        </div>
        <div class="controls col-sm-9">
            <?php echo CHtml::activeTextArea($model->seoData, 'title', array('cols'=>60, 'rows'=>'1')); ?>
        </div>
    </div>
-->

    <div class="row">
        <div class='control-label col-sm-3'>
            <?php echo CHtml::activeLabel($model->seoData, 'keywords'); ?>
        </div>
        <div class="controls col-sm-9">
            <?php echo CHtml::activeTextArea($model->seoData, 'keywords', array('cols'=>60, 'rows'=>'1')); ?>
        </div>
    </div>

    <div class="row">
        <div class='control-label col-sm-3'>
            <?php echo CHtml::activeLabel($model->seoData, 'description'); ?>
        </div>
        <div class="controls col-sm-9">
            <?php echo CHtml::activeTextArea($model->seoData, 'description', array('cols'=>60, 'rows'=>'1')); ?>
        </div>
    </div>

</fieldset>