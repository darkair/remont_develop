<?php

class MenuItem extends CActiveRecord
{
    const IMAGE_W = 28;
    const IMAGE_H = 28;

    public $_image = null; //UploadedFile[]
    public $_removeImageFlag = false; // bool

    private $articleByLink = null;


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    public function tableName()
    {
        return 'menuitem';
    }

    public function behaviors()
    {
        return array(
            'manyToMany' => array(
                'class' => 'lib.ar-relation-behavior.EActiveRecordRelationBehavior',
            ),
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageBehavior',
                'storagePath' => 'menu',
                'imageWidth' => self::IMAGE_W,
                'imageHeight' => self::IMAGE_H,
                'imageField' => 'image',
                'imageLabel' => 'Иконка'
            ),
            'orderBehavior' => array(
                'class' => 'application.behaviors.OrderBehavior',
            ),
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'MenuItemLang',
                'langTableName'         => 'menuitem_lang',
                'langForeignKey'        => 'menuItemId',
                //'langField'           => 'lang_id',
                'localizedAttributes'   => array('name'),
                //'localizedPrefix'     => 'l_',
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                //'createScenario'      => 'insert',
                //'localizedRelation'   => 'i18nPost',
                //'multilangRelation'   => 'multilangPost',
                //'forceOverwrite'      => false,
                //'forceDelete'         => true, 
                'dynamicLangClass'      => true, //Set to true if you don't want to create a 'PostLang.php' in your models folder
            ),
        );
    }

    public function relations()
    {
        return array(
            'children' => array(self::HAS_MANY, 'menuitem', 'parentItemId', 'order'=>'children.orderNum ASC'),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->imageBehavior->imageLabels(),
            $this->orderBehavior->orderLabels(),
            $this->languageBehavior->languageLabels(array(
                'name' => 'Текст'
            )),
            array(
                'menuId' => 'Меню',
                'parentItemId' => 'Родительский раздел',
                'active' => 'Активный',
                'visible' => 'Показывать',
                'link' => 'Ссылка',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->imageBehavior->imageRules(),
            $this->orderBehavior->orderRules(),
            array(
                array('menuId, name', 'required'),
                array('link', 'safe'),
                array('active, visible', 'boolean'),
                array('parentItemId', 'numerical', 'integerOnly'=>true),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onlyLinks' => array(
                'select' => $alias.'.id, '.$alias.'.link',
            ),
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
            'active' => array(
                'condition' => $alias.'.active = 1',
            ),
            'orderDefault' => array(
                'order' => $alias.'.menuId ASC, '.$alias.'.parentItemId ASC, '.$alias.'.orderNum ASC',
            ),
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function search($menuId)
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->condition = $alias.'.menuId=:menuId and '.$alias.'.parentItemId=0';
        $criteria->params = array(':menuId'=>$menuId);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            //'pagination'=>array(
            //    'pageSize'=>20,
            //),
            'sort' => array(
                'defaultOrder' => array(
                    'menuId' => CSort::SORT_ASC,
                    'orderNum' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function byParent($parent)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.parentItemId = '.$parent,
        ));
        return $this;
    }

    public function byMenuIds($menuIds)
    {
        $menuIds = is_array($menuIds) ? $menuIds : [$menuIds];
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.menuId in ('.implode(',', $menuIds).')',
        ));
        return $this;
    }

    public function byLink($link)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.link = "'.$link.'"',
        ));
        return $this;
    }

    public function byLinks($links)
    {
        $links = is_array($links) ? $links : [$links];
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.link in ("'.implode('","', $links).'")',
        ));
        return $this;
    }

    public function getIsNotFixed()
    {
        return !Yii::app()->urlManager->isFixedUrl($this->link);
    }

    public function getAddLink()
    {
        if (!$this->articleByLink)
            $this->articleByLink = Article::model()->byLink($this->link)->find();
        return empty($this->articleByLink) ? '/admin/articles/Articles/add/?link='.$this->link : '';
    }

    public function getEditLink()
    {
        if (!$this->articleByLink)
            $this->articleByLink = Article::model()->byLink($this->link)->find();
        return empty($this->articleByLink) ? '' : '/admin/articles/Articles/edit/?id='.$this->articleByLink->id;
    }

    public function getIconStorePath()
    {
        return $this->imageBehavior->getStorePath();
    }

    public function getIconUrl()
    {
        return $this->imageBehavior->getImageUrl();
    }

    public function setAttribute($name, $value)
    {
        $this->orderBehavior->orderSetAttribute($name, $value);
        return parent::setAttribute($name, $value);
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }
}
