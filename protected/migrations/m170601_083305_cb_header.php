<?php

Yii::import('modules.contentBlocks.models.ContentBlocks');

class m170601_083305_cb_header extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createContentBlock(
            'Текст в шапке',
            'Мы &mdash; удобный сервис организации ремонта&nbsp;в&nbsp;Москве',
            ContentBlocks::POS_HEADER_SLOGAN
        );
    }

    public function safeDown()
    {
        $this->deleteContentBlock(ContentBlocks::POS_HEADER_SLOGAN);
    }
}