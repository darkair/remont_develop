<?php

class m170601_081728_remove_param_costs extends CDbMigration
{
    public function safeUp()
    {
        $this->dropColumn('roomparam', 'minCost');
        $this->dropColumn('roomparam', 'maxCost');
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE roomparam ADD COLUMN `minCost` int(11) NOT NULL DEFAULT 0 COMMENT 'Стоимость услуги' AFTER image");
        $this->execute("ALTER TABLE roomparam ADD COLUMN `maxCost` int(11) NOT NULL DEFAULT 0 COMMENT 'Стоимость услуги' AFTER minCost");
    }
}