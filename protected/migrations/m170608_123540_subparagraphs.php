<?php

class m170608_123540_subparagraphs extends ExtendedDbMigration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `subparagraphs` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `title`         varchar(255) NOT NULL DEFAULT ''    COMMENT 'Заголовок подпункта',
                `cost`          int(11) NOT NULL DEFAULT 0          COMMENT 'Стоимость услуги',
                `orderNum`      int(10) NOT NULL DEFAULT 0          COMMENT 'Порядок сортировки',
                `visible`       tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `orderNum` (`orderNum`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->dropTable('subparagraphs');
    }
}