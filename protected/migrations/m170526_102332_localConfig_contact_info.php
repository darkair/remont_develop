<?php

class m170526_102332_localConfig_contact_info extends CDbMigration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO localconfig
                (`name`, `value`, `module`, `description`, `example`, `type`)
            VALUES
                ("email", "info@roomsme.ru", "contact-info", "Основная почта", "noreply@site.ru", "string"),
                ("phone", "+7 922 122 6111", "contact-info", "Основной телефон", "+7 999 123 4567", "string")
        ');
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM localconfig WHERE name IN ('email', 'phone') AND module='contact-info'
        ");
    }
}