<?php

class AdminRoomTypeController extends MAdminController
{
    public $modelName = 'RoomType';
    public $modelHumanTitle = array('тип квартиры', 'типов квартир', 'типов квартир');

    public function getEditFormElements($model)
    {
        return array(
            'roomsNumber' => ['type' => 'textField'],
            'square' => ['type' => 'textField'],
            'squareMin' => ['type' => 'textField'],
            'complectationCost' => ['type' => 'textField'],
            'fullremontCost' => ['type' => 'textField'],
            'roomCompartmentsList' => [
                'type' => 'childList',
                'htmlOptions' => [
                    'innerChildField'   => '_innerRoomCompartments',
                    'valueField'        => 'id',
                    'textField'         => 'title',
                    'childClass'        => 'RoomCompartment',
                    'editUrl'           => '/remont/adminRoomCompartment',
                    'useTitle'          => true,
                ]
            ],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            $this->getOrderColumn(),
            'roomsNumber',
            'square',
            'squareMin',
            'complectationCost',
            'fullremontCost',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $model->initRoomCompartments($model->_innerRoomCompartments);
        parent::beforeSave($model);
    }
}
