<?php

/**
 * Контентный блок
 */
class ContentBlocks extends CActiveRecord
{
    const POS_NONE                      = 0;
    const POS_HEADER_SLOGAN             = 1;
    const POS_HEADER_TEXT_1             = 2;

    const POS_CHECKBOX_PROJECT          = 10;
    const POS_POPOVER_PROJECT           = 11;
    const POS_CHECKBOX_COMPLECTATION    = 12;
    const POS_POPOVER_COMPLECTATION     = 13;
    const POS_CHECKBOX_NADZOR           = 14;
    const POS_POPOVER_NADZOR            = 15;
    const POS_CHECKBOX_FULLREMONT       = 16;
    const POS_POPOVER_FULLREMONT        = 17;
    const POS_CHECKBOX_STYLES           = 18;
    const POS_POPOVER_STYLES            = 19;

    const POS_TITLE_PROJECT             = 30;
    const POS_TITLE_COMPLECTATION       = 31;
    const POS_TITLE_NADZOR              = 32;
    const POS_TITLE_FULLREMONT          = 33;
    const POS_TITLE_STYLES              = 34;

    // POS_CONST => 'Name'
    private static $posNames = array(
        self::POS_HEADER_SLOGAN             => 'Заголовок текста в шапке',
        self::POS_HEADER_TEXT_1             => 'Текст в шапке',
        self::POS_CHECKBOX_PROJECT          => 'Проект: Описание чекбокса',
        self::POS_CHECKBOX_COMPLECTATION    => 'Комплектация: Описание чекбокса',
        self::POS_CHECKBOX_NADZOR           => 'Надзор: Описание чекбокса',
        self::POS_CHECKBOX_FULLREMONT       => 'Полный ремонт: Описание чекбокса',
        self::POS_CHECKBOX_STYLES           => 'Стили: Описание чекбокса',
        self::POS_POPOVER_PROJECT           => 'Проект: Всплывающая подсказка',
        self::POS_POPOVER_COMPLECTATION     => 'Комплектация: Всплывающая подсказка',
        self::POS_POPOVER_NADZOR            => 'Надзор: Всплывающая подсказка',
        self::POS_POPOVER_FULLREMONT        => 'Полный ремонт: Всплывающая подсказка',
        self::POS_POPOVER_STYLES            => 'Стили: Всплывающая подсказка',
        self::POS_TITLE_PROJECT             => 'Проект: Название чекбокса',
        self::POS_TITLE_COMPLECTATION       => 'Комплектация: Название чекбокса',
        self::POS_TITLE_NADZOR              => 'Надзор: Название чекбокса',
        self::POS_TITLE_FULLREMONT          => 'Полный  Название чекбокса',
        self::POS_TITLE_STYLES              => 'Стили: Название чекбокса',
    );

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'contentblocks';
    }

    public function behaviors()
    {
        return array(
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'ContentBlocksLang',
                'langTableName'         => 'contentblocks_lang',
                'langForeignKey'        => 'cbId',
                'localizedAttributes'   => array('title', 'text'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Заголовок',
                'text'      => 'Текст'
            )),
            array(
                'position'  => 'Место размещения',
                'visible'   => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array(
            array('title, text', 'safe'),
            array('visible', 'boolean'),
            array('position', 'numerical', 'integerOnly'=>true),

            array('visible', 'safe', 'on'=>'search'),
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function byPosition($pos)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.position = '.$pos,
        ));
        return $this;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }

    public static function getPosNames()
    {
        return self::$posNames;
    }
}
