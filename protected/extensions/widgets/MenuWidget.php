<?php

Yii::import('application.models.Menu');

class MenuWidget extends ExtendedWidget
{
    public $template = '';
    public $options = [];                   // добавочные параметры, передаваемые в шаблон
    public $disableEmptyLinks = false;      // Отключить ссылки, ведущие в никуда
    public $enablePrefetch = false;         // Подключить предзагрузку пунктов меню

    // $menuId can be integer or array
    public $menuId = Menu::NONE;

    private static $url = null;

    public function run()
    {
        if (empty($this->template))
            return;

        $items = $this->getMenuItems();

        $this->beforeRender($items);
        $this->render($this->template, array_merge($this->options, ['items'=>$items]));
    }

    protected function beforeRender(&$itemsArr)
    {
    }

    /**
     * Получить структуру всего меню
     */
    protected function getMenuItems($parentId = 0)
    {
        $menuIds = is_array($this->menuId) ? $this->menuId : array($this->menuId);
        $items = MenuItem::model()
            ->onSite()
            ->byParent($parentId)
            ->byMenuIds($menuIds)
            ->orderDefault()
            ->findAll();
        
        $arr = array();
        foreach ($items as $item) {
            if ($this->disableEmptyLinks  &&  $parentId) {
                $isFixed = Yii::app()->urlManager->isFixedUrl($item->link);
                if (!$isFixed) {
                    $isArticleUrl = Article::model()->byLink($item->link)->count() ? true : false;
                    if (!$isArticleUrl)
                        $item->active = false;
                }
            }
            $arr[] = $this->createMenuItemData($item);
        }
        return $arr;
    }

    /**
     * Получить структуру текущего пункта меню
     * NOTE: Вынесено в отдельный метод, чтобы можно было получать структуру отдельного пункта меню
     */
    protected function createMenuItemData(&$item)
    {
        if (strpos($item->link, 'http://') === 0 || strpos($item->link, 'https://') === 0) {
            $link = $item->link;
            $blank = 1;
        } else {
            $link = array('/'.$item->link);    // Роуты с языком
            $link = Yii::app()->params['baseUrl'].CHtml::normalizeUrl($link);
            $blank = 0;
        }

        // Выделяем родительские элементы
        $select = false;
        $children = $this->getMenuItems($item->id);
        foreach ($children as $child) {
            if ($child['select']) {
                $select = true;
                break;
            }
        }

        if ($this->enablePrefetch) {
            PrefetchHelper::linkPrefetch( Yii::app()->createAbsoluteUrl($link) );
        }

        return array(
            'name'      => $item->name,
            'link'      => $link,
            'blank'     => $blank,
            'iconUrl'   => $item->getIconUrl(),
            'disabled'  => $item->active ? 0 : 1,
            'select'    => $select || $this->getSelect($item),
            'children'  => $children,
        );
    }

    public function getMenuTitle()
    {
        if ($this->menuId == Menu::NONE || is_array($this->menuId))
            return '';
        $menu = Menu::model()->findByPk($this->menuId);
        if (!$menu)
            return '';
        return $menu->name;
    }

    private function getSelect(&$item)
    {
        if (self::$url === null)
            self::$url = Yii::app()->request->getUrlWithoutLanguage();

        $link = trim($item->link, '/');
        if (empty($link))
            return false;
        
        return (strpos(self::$url, trim($item->link, '/')) === 0) ? true : false;
    }
}
