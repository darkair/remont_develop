SHELL:=/bin/bash
site_host = roomsme.test

define git_remote =
	bash -c 'git remote add upstream $(git remote get-url origin | sed "s|_develop.git|.git|g")'
endef

build: init git app

init:
	$(value git_remote) 
	git fetch --progress --prune origin
	git fetch --progress --prune upstream
	git checkout -b upstream-master upstream/master-upstream
	git checkout master

git:
	git pull
	git submodule init
	git submodule update --recursive

app:
	npm i
	./develop build
	./develop up -d
	./develop exec -u www-data php ./composer.phar selfupdate
	./develop exec -u www-data php ./composer.phar install
	cp ./robots.dist.txt ./robots.txt
	sed -i "s|%site_host%|$(site_host)|g" ./robots.txt
	cp ./protected/config/params-global.php ./protected/config/params.php
	sed -i "s/.*dbHost.*/'dbHost' => 'db',/g" ./protected/config/params.php
	sed -i "s/.*dbName.*/'dbName' => 'site',/g" ./protected/config/params.php
	sed -i "s/.*dbLogin.*/'dbLogin' => 'darkair',/g" ./protected/config/params.php
	sed -i "s/.*dbPassword.*/'dbPassword' => 'darkair',/g" ./protected/config/params.php
	./develop exec -u www-data php ./protected/yiic migrate --interactive=0
	./develop exec -u www-data php ./protected/yiic createauthitems --email=darkair2@gmail.com --password=darkair

php:
	./develop exec -u www-data php bash