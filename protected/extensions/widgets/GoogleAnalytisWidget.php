<?php

class GoogleAnalytisWidget extends ExtendedWidget
{
    public function run()
    {
        PrefetchHelper::dnsPrefetch('//google-analytics.com/');

        $this->render('googleAnalytics');
    }
}
