var Debug = function()
{
    this.$el = null;
    this.sizeId = null;

    this.init = function($el)
    {
        this.$el = $el;
        return this;
    }

    this.size = function()
    {
        var self = this;

        self.$el.css({'position': 'relative'});

        self.sizeId = Math.floor(Math.random()*100000000);
        var $size = $('<div id="'+self.sizeId+'" style="position: absolute; right: 0; top: 0; border: 1px dashed #666; font-size: 12px; padding: 0px 4px; margin: 0; background: #eee;"></div>')
        self.$el.append($size);

        $(window).resize(function() {
            var w = self.$el.width();
            var h = self.$el.height();
            $('#'+self.sizeId).html(w+' x '+h);
        }).resize();

        return this;
    }

    this.outline = function(color='#ccc')
    {
        this.$el.css({'outline': '1px dotted '+color});
        return this;
    }
}
