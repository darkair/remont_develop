<?php

Yii::import('zii.widgets.CMenu');

class AdminMenuWidget extends CMenu
{
    public $vertical = false;
    private $level = 0;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        // Запрещаем bootstrap'у очищать стили меню после клика, чтобы стрелочка текущего элемента не прыгала
        $script = <<<EOF
            $(document).on('hide.bs.dropdown', function(e) {
                e.preventDefault();
            });
EOF;
        Yii::app()->clientScript->registerScript(
            __CLASS__ . '#' . $this->getId(),
            $script
        );


        if (!$this->vertical) {
            $this->submenuHtmlOptions = array(
                'class' => "dropdown-menu dropdown-light-blue dropdown-caret submenu-right"
            );
        } else {
            $this->submenuHtmlOptions = array(
                'class' => 'submenu'
            );
        }
        $this->itemCssClass = 'dropdown';

        $classes = array('nav');
        $classes[] = $this->vertical ? 'nav-list' : 'navbar-nav';

        if (!empty($classes)) {
            $classes = implode(' ', $classes);
            if (isset($this->htmlOptions['class']))
                $this->htmlOptions['class'] .= ' ' . $classes;
            else
                $this->htmlOptions['class'] = $classes;
        }
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
     * @return string the rendered item
     */
    protected function renderMenuItem($item)
    {
        if (isset($item['icon'])) {
            if (strpos($item['icon'], 'icon') === false && strpos($item['icon'], 'fa') === false)
                $item['icon'] = 'fa fa-' . implode(' fa-', explode(' ', $item['icon']));
            $item['label'] = '<i class="menu-icon ' . $item['icon'] . '"></i> ' . $item['label'];
        }

        if (!isset($item['linkOptions']))
            $item['linkOptions'] = array();

        if (isset($item['items']) && !empty($item['items'])) {
            if (empty($item['url']))
                $item['url'] = '#';

            if (isset($item['linkOptions']['class']))
                $item['linkOptions']['class'] .= ' dropdown-toggle';
            else
                $item['linkOptions']['class'] = 'dropdown-toggle';

            $item['linkOptions']['data-toggle'] = 'dropdown';
            if (!$this->vertical)
                $item['label'] .= ' <span class="caret"></span>';
        }

        if (isset($item['url'])) {
            $text = $item['label'];
            if ($this->vertical) {
                if (isset($item['items']) && count($item['items'])) {
                    $text .= '<b class="arrow fa fa-angle-down"></b>';
                }
                // Пропускаем первые два уровня, на остальных делаем стрелки
                for ($i=2; $i < $this->level; $i++) { 
                    $text = '<i class="menu-icon fa fa-caret-right"></i>' . $text;
                }
            }
            return CHtml::link($text, $item['url'], $item['linkOptions']);
        }
        return $item['label'];
    }

    /**
     * Recursively renders the menu items.
     * @param array $items the menu items to be rendered recursively
     */
    protected function renderMenuRecursive($items)
    {
        $this->level++;

        $count=0;
        $n=count($items);
        foreach($items as $item)
        {
            $count++;
            $options=isset($item['itemOptions']) ? $item['itemOptions'] : array();
            $class=array();
            if($item['active'] && $this->activeCssClass!='')
                $class[]=$this->activeCssClass;
            if($count===1 && $this->firstItemCssClass!==null)
                $class[]=$this->firstItemCssClass;
            if($count===$n && $this->lastItemCssClass!==null)
                $class[]=$this->lastItemCssClass;
            if($this->itemCssClass!==null)
                $class[]=$this->itemCssClass;

            if($item['active'] && isset($item['items']) && count($item['items'])) {
                $class[] = 'open';
            }

            if($class!==array()) {
                if(empty($options['class']))
                    $options['class']=implode(' ',$class);
                else
                    $options['class'].=' '.implode(' ',$class);
            }

            echo CHtml::openTag('li', $options);

            $menu=$this->renderMenuItem($item);
            if(isset($this->itemTemplate) || isset($item['template'])) {
                $template=isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template,array('{menu}'=>$menu));
            } else {
                echo $menu;
            }

            if(isset($item['items']) && count($item['items'])) {
                echo "\n".CHtml::openTag('ul',isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions)."\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag('ul')."\n";
            }

            echo CHtml::closeTag('li')."\n";
        }

        $this->level--;
    }
}