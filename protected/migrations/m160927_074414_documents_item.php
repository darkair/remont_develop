<?php

class m160927_074414_documents_item extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `documentsitem` (
                `id`        int(11) NOT NULL AUTO_INCREMENT,
                `title`     text NOT NULL COMMENT 'Заголовок',
                `filename`  varchar(128) NOT NULL DEFAULT '' COMMENT 'Имя файла',
                `orig`      varchar(255) NOT NULL DEFAULT '' COMMENT 'Оригинальное имя файла',
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `documentsitem_lang` (
                `l_id` int(11) NOT NULL AUTO_INCREMENT,
                `documentsItemId` int(11) NOT NULL,
                `lang_id` varchar(6) NOT NULL,
                `l_title` text NOT NULL,
                PRIMARY KEY (`l_id`),
                KEY `documentsItemId` (`documentsItemId`),
                KEY `lang_id` (`lang_id`),
                CONSTRAINT `fk_documentsitem_lang` FOREIGN KEY (`documentsItemId`) REFERENCES `documentsitem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('documentsitem_lang');
        $this->dropTable('documentsitem');
    }
}
