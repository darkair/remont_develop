<?php

Yii::import('modules.remont.models.*');

class RoomsApiController extends Controller
{
    /**
     * Getting common info about rooms
     */
    public function actionGetRoomsInfo()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(404);
        }
        echo json_encode( $this->listData(RoomType::model()->onSite()->orderDefault()->findAll(), 'roomsNumber', ['square', 'squareMin'], 'id') );
        Yii::app()->end();
    }

    /**
     * Getting total data about rooms
     */
    public function actionGetRoomsData()
    {
        if (!Yii::app()->request->isPostRequest || !Yii::app()->request->isAjaxRequest)
            throw new CHttpException(404);

        $roomId = Yii::app()->request->getPost('roomId');
        if (!$roomId)
            throw new CHttpException(404);

        $roomType = RoomType::model()->onSite()->findByPk($roomId);
        if (!$roomType)
            throw new CHttpException(404);

        $res = [];
        foreach ($roomType->roomCompartmentsList as $compartment) {
            if (!$compartment->visible) {
                continue;
            }

            $blocksArr = [];
            foreach ($compartment->roomBlocksList as $roomBlock) {
                if (!$roomBlock->visible) {
                    continue;
                }

                $groupsArr = [];
                foreach ($roomBlock->roomGroupsList as $roomGroup) {
                    $roomParamsArr = [];
                    foreach ($roomGroup->roomParamsList as $param) {
                        if (!$param->visible) {
                            continue;
                        }
                        $roomParamsArr[] = [
                            'id' => $param->id,
                            'title' => $param->title,
                            'imageUrl' => $param->getImageUrl(),
                        ];
                    }
                    $groupsArr[] = [
                        'title' => $roomGroup->title,
                        'desc' => $roomGroup->desc,
                        'visible' => $roomGroup->visible,
                        'params' => $roomParamsArr,
                    ];
                }
                $blocksArr[] = [
                    'id' => $roomBlock->id,
                    'title' => $roomBlock->title,
                    'imageUrl' => $roomBlock->getImageUrl(),
                    'groups' => $groupsArr,
                ];
            }
            $res[] = [
                'title' => $compartment->title,
                'blocks' => $blocksArr,
            ];
        }
        echo json_encode($res);
        Yii::app()->end();
    }

    private static function listData($models, $valueField, $textFields, $groupField='')
    {
        $listData = array();
        foreach ($models as $model) {
            $group = CHtml::value($model, $groupField);
            $value = CHtml::value($model, $valueField);

            $text = [];
            foreach ($textFields as $textField) {
                $text[$textField] = CHtml::value($model, $textField);
            }
            $listData[$group][$value] = $text;
        }
        return $listData;
    }
}