<?php

class AdminRoomCompartmentController extends MAdminController
{
    public $modelName = 'RoomCompartment';
    public $modelHumanTitle = array('помещение', 'помещения', 'помещений');

    public function behaviors()
    {
        return array(
        );
    }

    public function getEditFormElements($model)
    {
        return array(
            'title' => ['type' => 'textField'],
            'roomBlocksList' => [
                'type' => 'childList',
                'htmlOptions' => [
                    'innerChildField'   => '_innerRoomBlocks',
                    'valueField'        => 'id',
                    'textField'         => 'title',
                    'additionalFields'  => ['projectCost', 'nadzorCost'],
                    'childClass'        => 'RoomBlock',
                    'editUrl'           => '/remont/adminRoomBlock',
                    'useTitle'          => true,
                ]
            ],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'title',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $model->initRoomBlocks($model->_innerRoomBlocks);
        parent::beforeSave($model);
    }
}
