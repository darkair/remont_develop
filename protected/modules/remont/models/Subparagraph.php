<?php

/**
 * Тип комнаты
 */
class Subparagraph extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'subparagraphs';
    }

    public function behaviors()
    {
        return array(
            'orderBehavior' => array(
                'class' => 'application.behaviors.OrderBehavior',
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->orderBehavior->orderLabels(),
            array(
                'title' => 'Заголовок подпункта',
                'cost' => 'Стоимость услуги',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->orderBehavior->orderRules(),
            array(
                array('title', 'required'),
                array('cost', 'numerical', 'integerOnly'=>true),
                array('visible', 'boolean'),

                array('title, cost, visible', 'safe', 'on'=>'search'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
            'orderDefault' => array(
                'order' => $alias.'.orderNum ASC',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.title', $this->title, true);
        $criteria->compare($alias.'.cost', $this->cost);
        $criteria->compare($alias.'.visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'orderNum' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function setAttribute($name, $value)
    {
        $this->orderBehavior->orderSetAttribute($name, $value);
        return parent::setAttribute($name, $value);
    }
}
