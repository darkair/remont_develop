<?php

abstract class AdminUploadFileController extends MAdminController
{
    public $modelName = '';
    public $modelHumanTitle = array('файл', 'файла', 'файлов');
    public $allowedRoles = 'admin, manager';
    public $allowedActions = 'edit, fileUpload, fileRemove';
    public $defaultAction = 'edit';

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('bootbox');
        //AdminComponent::getInstance()->assetsRegistry->registerPackage('jquery.ui');
    }

    public function actionEdit($createNew = false)
    {
        $storePath = $this->getStorePath();
        $filePdf = false;
        if ($objs = glob($storePath."*.pdf")) {
            foreach($objs as $obj) {
                if (is_file($obj)) {
                    $filePdf = basename($obj);
                    break;
                }
            }
        }

        $this->render(
            '/uploadFile',
            [
                'files' => empty($filePdf) ? [] : [$filePdf],
                'routePath' => $this->getRoutePath()
            ]
        );
    }


    public function actionFileRemove()
    {
        if (!Yii::app()->request->isAjaxRequest  ||  !Yii::app()->request->isPostRequest)
            throw new CHttpException(404);

        $storePath = $this->getStorePath();
        CFileHelper::removeDirectory($storePath);
        
        echo CJSON::encode([]);
        Yii::app()->end();
    }

    public function actionFileUpload()
    {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(404);

        // our operation result including `status` and `message` which will be sent to browser
        $result = array();
        $file = $_FILES['file'];

        if (is_string($file['name'])) {
            //single file upload, file['name'], $file['type'] will be a string
            $result[] = $this->validateAndSave($file);
        } else
        if (is_array($file['name'])) {
            //multiple files uploaded
            $fileCount = count($file['name']);

            for ($i = 0; $i < $fileCount; $i++) {
                $fileInfo = array(
                    'name'      => $file['name'][$i],
                    'type'      => $file['type'][$i],
                    'size'      => $file['size'][$i],
                    'tmp_name'  => $file['tmp_name'][$i],
                    'error'     => $file['error'][$i]
                );
                $result[] = $this->validateAndSave($fileInfo);
            }
        }

        echo CJSON::encode($result);
        Yii::app()->end();
    }

    private function validateAndSave($file)
    {
        // then there is an error
        if (!preg_match('/^application\/pdf/', $file['type'])  ||  !preg_match('/\.pdf$/', $file['name']))
            return ['error' => Yii::t('admin', 'Не верный формат файла')];

        // if size is larger than what we expect
        if ($file['size'] > 10*1024*1024)
            return ['error' => Yii::t('admin', 'Файл не должен превышать 10Мб')];

        // if there is an unknown error or temporary uploaded file is not what we thought it was
        if ($file['error'] != 0 || !is_uploaded_file($file['tmp_name']))
            return ['error' => Yii::t('admin', 'При загрузке файла произошла ошибка')];

        $storePath = $this->getStorePath();
        CFileHelper::removeDirectory($storePath);
        if (!is_dir($storePath))
            mkdir($storePath, 0755, true);

        $filename = preg_replace('/[^\w\.\- ]/', '', $file['name']);
        $savePath = $storePath . $filename;

        if (!move_uploaded_file($file['tmp_name'], $savePath))
            return ['error' => Yii::t('admin', 'Ошибка сохранения файла')];

        return [
            'success' => Yii::t('admin', 'Файл был успешно загружен'),
            'filename' => $filename,
        ];
    }

    abstract protected function getStorePath();         // return Yii::getPathOfAlias('webroot.store.pdf').'/';
    abstract protected function getRoutePath();         // return 'adminUploadSmthFile';
}
