<?php

class AdminRoomParamController extends MAdminController
{
    public $modelName = 'RoomParam';
    public $modelHumanTitle = array('параметр', 'параметра', 'параметров');
    public $allowedActions = 'add,edit,delete,update,addRaw,editRaw';

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageControllerBehavior',
                'imageField' => 'image',
                'imageWidth' => RoomParam::IMAGE_W,
                'imageHeight' => RoomParam::IMAGE_H,
                'resizeType' => 'cover'
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array(
            '_image' => array(
                'class' => 'ext.ImageFileRowWidget',
                'uploadedFileFieldName' => '_image',
                'removeImageFieldName' => '_removeImageFlag',
            ),
            'title' => ['type' => 'textField'],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'title',
            $this->getImageColumn('image', 'getImageUrl()'),
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->imageBehavior->imageBeforeSave($model, $model->getImageStorePath());
        parent::beforeSave($model);
    }
}
