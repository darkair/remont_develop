<?php

class EFileHelper
{
    /**
     * @param string $fileName
     * @return string
     */
    public static function getExtensionByMimeType($fileName)
    {
        $mimeTypes = require(Yii::getPathOfAlias('system.utils.mimeTypes') . '.php');
        $unsetArray = array('jpe', 'jpeg');
        foreach ($unsetArray as $key)
            unset($mimeTypes[$key]);

        $mimeType = CFileHelper::getMimeType($fileName);
        return (string)array_search($mimeType, $mimeTypes);
    }

    /**
     * @param string $contentDisposition 'inline'|'attachment'
     */
    public static function getFile($filePath, $fileName, $origFileName, $contentDisposition = 'attachment')
    {
        // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        // если этого не сделать файл будет читаться в память полностью!
        if (ob_get_level())
            ob_end_clean();

        if (!file_exists($filePath))
            return false;

        // Get mime type
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
        $mimeType = finfo_file($finfo, $filePath);
        finfo_close($finfo);

        // заставляем браузер показать окно сохранения файла
        header('Content-Description: File Transfer');
        header('Content-Type: '.$mimeType);
        header("Content-Disposition: {$contentDisposition}; filename=\"{$origFileName}\"; filename*=utf-8''{$origFileName}");
        header('Content-Transfer-Encoding: chunked');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filePath));

        // читаем файл и отправляем его пользователю
        readfile($filePath);

        return true;
    }
}
