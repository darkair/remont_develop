<?php

class FeedbackForm extends CFormModel
{
    public $name;
    public $text;
    public $phone;
    public $email;
    public $section;
    public $verifyCode;

    public function rules()
    {
        return array(
            ['name', 'safe'],
            //array('name', 'type', 'type'=>'string', 'allowEmpty'=>false),
            //array('name', 'required', 'message'=>Yii::t('app', 'Введите ваше имя')),
            
            //array('email', 'email'),
            //array('phone, email', 'onPhoneEmailValidator'),
            ['email', 'safe'],
            ['phone', 'required', 'message'=>Yii::t('app', 'Укажите телефон')],

            array('text', 'type', 'type'=>'string', 'allowEmpty'=>false),
            array('text', 'required', 'message'=>Yii::t('app', 'Напишите сообщение')),
            array('text', 'length', 'min'=>16, 'tooShort'=>Yii::t('app', 'Напишите сообщение в развернутом виде')),
            array('section', 'required'),
            ['verifyCode', 'safe']
            //array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
        );
    }

    public function onPhoneEmailValidator($attribute, $param)
    {
        if (empty($this->phone) && empty($this->email)) {
            $this->addError('phone', Yii::t('app', 'Заполните это поле'));
            $this->addError('email', Yii::t('app', 'Заполните это поле'));
        }
    }


    public function attributeLabels()
    {
        return array(
            'name' => Yii::t('app', 'Ваше имя'),
            'text' => Yii::t('app', 'Сообщение'),
            'phone' => Yii::t('app', 'Ваш телефон'),
            'email' => Yii::t('app', 'E-mail'),
            'section' => Yii::t('app', 'Выберите тему обращения'),
            'verifyCode' => Yii::t('app', 'Введите код с картинки'),
        );
    }
}
