<?php

class AdminFaqController extends MAdminController
{
    public $modelName = 'Faq';
    public $modelHumanTitle = array('элемент', 'элементов', 'элементов');
    public $allowedActions = 'add,edit,delete,update';

    public function getEditFormElements($model)
    {
        return array(
            'question' => ['type' => 'textArea'],
            'answer' => ['type' => 'ckEditor'],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'question',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }
}
