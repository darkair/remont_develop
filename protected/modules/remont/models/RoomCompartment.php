<?php

/**
 * Помещение
 */
class RoomCompartment extends CActiveRecord
{
    public $roomBlocksList = [];
    public $_innerRoomBlocks = [];

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'roomcompartment';
    }

    public function behaviors()
    {
        return array(
            'childListBehavior' => array(
                'class'                 => 'application.behaviors.ChildListBehavior',
                'label'                 => 'Комнатные блоки',
                'childField'            => 'roomBlocks',
                'childObjField'         => 'roomBlocksList',
                'innerChildField'       => '_innerRoomBlocks',
                'childClass'            => 'RoomBlock',
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->childListBehavior->childListLabels(),
            array(
                'title' => 'Заголовок',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->childListBehavior->childListRules(),
            array(
                array('title', 'required'),
                array('visible', 'boolean'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

//        $alias = $this->getTableAlias();
//        $criteria->compare($alias.'.position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function initRoomBlocks($ids)
    {
        $this->childListBehavior->initObjects($ids);
    }
}
