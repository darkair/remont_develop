<?php

class ChildListBehavior extends CActiveRecordBehavior
{
    public $childClass = null;
    public $label = '';
    public $childField = '';            // Поле для JSON
    public $childObjField = '';         // Поле для объектов
    public $innerChildField = '';       // Внутреннее поле для редактирования
    public $deleteChildren = false;     // Удалять или нет дочерние элементы
    public $unique = true;              // Оставлять объекты в единственном экземпляре или разрешать дублировать


    public function childListLabels()
    {
        $arr = array(
            $this->childObjField => $this->label,
        );
        return $arr;
    }

    public function childListRules()
    {
        $rules = array(
            array($this->childField, 'safe'),
            array($this->innerChildField, 'safe')
        );
        return $rules;
    }

    public function afterFind($event)
    {
        $owner = $this->owner;
        $childField = $this->childField;

        if (!empty($owner->{$childField})) {
            $owner->{$childField} = CJSON::decode($owner->{$childField});
            if (!is_array($owner->{$childField}))
                $owner->{$childField} = array();

            $this->initObjects($owner->{$childField});
        }
    }

    public function beforeSave($event)
    {
        $arr = $this->unique
            ? array_unique($this->owner->{$this->childField})
            : $this->owner->{$this->childField};
        $this->owner->{$this->childField} = CJSON::encode($arr);
    }

    public function beforeDelete($event)
    {
        if ($this->deleteChildren) {
            // FIX: Здесь может не быть дочерних объектов по неизвестным пока причинам
            foreach ($this->owner->{$this->childObjField} as $obj) {
                if ($obj)
                    $obj->delete();
            }
        }
    }

    public function initObjects($ids)
    {
        $class = $this->childClass;
        if (!$class)
            return false;

        $owner = $this->owner;
        $childField = $this->childField;
        $childObjField = $this->childObjField;

        if (!empty($ids) && is_array($ids)) {
            reset($ids);
            if (count($ids) == 1 && current($ids) === "")
                $ids = [];

            $owner->{$childField} = $ids;

            $owner->{$childObjField} = [];
            foreach ($owner->{$childField} as $id) {
                $obj = $class::model()->findByPk($id);
                // Из-за того, что нет обратной связи объектов списка с самим списком, при удалении объекта в списке возникает невалидная ссылка, что приводит к пустому объекту, его не надо размещать в конечном списке
                if ($obj) {
                    if ($this->unique) {
                        $owner->{$childObjField}[$id] = $obj;
                    } else {
                        $owner->{$childObjField}[] = $obj;
                    }
                }
            }
        }
    }
}
