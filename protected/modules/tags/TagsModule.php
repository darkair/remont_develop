<?php

class TagsModule extends CWebModule
{
    public $defaultController = 'admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.tags.models.*');
    }
}
