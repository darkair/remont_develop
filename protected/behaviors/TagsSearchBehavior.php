<?php

class TagsSearchBehavior extends CActiveRecordBehavior
{
    const TAGS_DELIMITER = ',';

    public $tagsField = 'tags';
    public $tagsTable = 'tags';
    public $tag2objTable = '';

    public function tagsLabels()
    {
        return array(
            $this->tagsField => Yii::t('app', 'По тегам'),
        );
    }

    public function tagsRules()
    {
        return array(
            array($this->tagsField, 'safe'),
        );
    }

    public function tagsStr2Array($str)
    {
        $delimiter = self::TAGS_DELIMITER;
        $tags = explode($delimiter, trim(trim($str), $delimiter));
        array_walk($tags, function(&$tag) {
            $tag = trim($tag);
        });
        return $tags;
    }

    public function setCriteria(&$criteria)
    {
        if (!empty($this->owner->{$this->tagsField})) {
            $tags = $this->tagsStr2Array($this->owner->{$this->tagsField});
            $criteria->join = 'left join '.$this->tag2objTable.' as t2o on t2o.objId = t.id left join '.$this->tagsTable.' as tt on tt.id = t2o.tagId';
            $criteria->addInCondition('tt.name', $tags);
            
            // Убираем дубликаты
            $criteria->group = 't.id';

            // Обязательно задаем основную таблицу для выборки, чтобы не было duplicate `id`
            $criteria->select = 't.*';
        }
    }
}
