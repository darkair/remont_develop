<?php

class ReviewsModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.reviews.models.*');
    }
}
