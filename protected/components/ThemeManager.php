<?php

class ThemeManager extends CThemeManager
{
    public $themeClass='Theme';
    public $subthemeFunc = null;

    public function getTheme($name)
    {
        $themePath = $this->getBasePath().DIRECTORY_SEPARATOR.$name;
        if (is_dir($themePath)) {
            $class = Yii::import($this->themeClass, true);
            $subtheme = (is_callable($this->subthemeFunc))
                ? call_user_func($this->subthemeFunc)
                : '';
            return new $class($name, $themePath, $this->getBaseUrl().'/'.$name, $subtheme);
        } else {
            throw new CException(Yii::t('yii','Theme directory "{directory}" does not exist.',array('{directory}'=>$name)));
        }
    }
}
