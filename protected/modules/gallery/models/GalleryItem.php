<?php

/**
 * Отдельная картинка галереи
 */
class GalleryItem extends CActiveRecord
{
    // Размер изображений в галерее
    const IMAGE_W = 1280;
    const IMAGE_H = 1280;

    public $url = '';       // Урл до картинки, формируется при получении GalleryItem через галерею

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'galleryitem';
    }

    public function behaviors()
    {
        return array(
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'GalleryItemLang',
                'langTableName'         => 'galleryitem_lang',
                'langForeignKey'        => 'galleryItemId',
                'localizedAttributes'   => array('title'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Подпись',
            )),
            [
                'fileName'  => 'Имя файла',
            ]
        );
    }

    public function rules()
    {
        return array_merge(
            array(
                array('title', 'safe'),
                array('fileName', 'safe'),
                array('title', 'ext.validators.CapsValidator'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }
}
