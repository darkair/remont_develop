var constructor = {
    o: {
        roomDataLink: '',
        sendFormLink: '',
        savePictureLink: '',
        recalcLink: '',
        city: ''
    },
    $roomList: null,
    $roomAddServices: null,
    $content: null,
    $loader: null,
    $form: null,
    $floatPanel: null,
    $fixedPanel: null,
    $resultPanel: null,
    $roomsSendForm: null,
    isLoading: false,
    additionalCheckboxes: {
        styles:         true,
        project:        false,
        complectation:  false,
        nadzor:         false,
        fullremont:     false
    },
    popoverVisible: false,
    projectCost: 0,
    nadzorCost: 0,
    complectationCost: 0,
    complectationDefCost: 0,
    fullremontCost: 0,
    fullremontDefCost: 0,

    vueApp: null,

    // Хранение данных перед отправкой
    roomData: {},
    dataFor1C: {},

    init: function(options)
    {
        var self = this;

        self.o = $.extend(self.o, options);

        self.$roomList =        $('.room-constructor-widget #js-rooms-list');
        self.$roomSquare =      $('.room-constructor-widget #js-rooms-square');
        self.$roomAddServices = $('.room-constructor-widget #js-rooms-additional-service');
        self.$content =         $('.room-constructor-widget #js-room-content');
        self.$loader =          $('.room-constructor-widget #js-loader');
        self.$form =            $('.room-constructor-widget #js-form');
        self.$roomsSendForm =   $('.room-constructor-widget #js-rooms-send-form');
        self.$floatPanel =      $('.room-constructor-widget .float-panel');
        self.$fixedPanel =      $('.room-constructor-widget .fixed-panel');
        self.$resultPanel =     $('.room-constructor-widget .result-panel');

        self.$roomList.find('[type=radio][name="roomType"]').change(function() {
            if (self.isLoading)
                return false;
            self.isLoading = true;
            self.$roomList.find('[type=radio][name="roomType"]').attr({'disabled':'disabled'});
            self.$loader.addClass('is-active');

            // Set square
            self.$roomList.find('[type=text][name="square"]')
                .val($(this).data('square'))
                .data('square-min', $(this).data('square-min'));

            $.post(
                self.o.roomDataLink,
                {roomId: this.value},
                function(data) {
                    self.$content.html(data);
                    self.$roomList.find('[type=radio][name="roomType"]').removeAttr('disabled');
                    self.isLoading = false;
                    self.$loader.removeClass('is-active');

                    self.preloadImages();
                },
                'html'
            );
        });

        // Изменение площади
        self.$roomSquare.find('[type=text][name="square"]').change(function() {
            var square = self.getSquare();
            var squareMin = $(this).data('square-min');

            if (square < squareMin) {
                square = squareMin;
                $(this).val(squareMin);
            }

            if (self.dataFor1C.square !== undefined)
                self.dataFor1C.square = square;

            self.sendRecalc();
        });

        self.$roomList.find('[type=radio][name="roomType"]:first').trigger('click');

        // Получение общей информации о выбраных опциях
        $('body').on("roomConstructorResultInfo", function(ev, msg, data, projectCost, nadzorCost, complectationCost, fullremontCost) {
            self.$resultPanel.find('.result-panel-inner').html(msg);
            self.roomData       = data;
            self.projectCost    = projectCost;
            self.nadzorCost     = nadzorCost;

            // Запоминаем на случай отсутствия связи с сервером
            self.complectationDefCost  = complectationCost;
            self.fullremontDefCost     = fullremontCost;

            self.sendRecalc();

            // Дополнительно перерисовываем панель, на случай если при обновлении страницы
            // придет 404 и sendRecalc() не сможет обновить данные
            self.recalcCost();
        });

        self.initAdditionalServices();
        self.initForm();
        self.initFloatPanel();
        //self.initTether();
        self.initVue();
    },

    initAdditionalServices: function()
    {
        var self = this;

        var $styles         = self.$roomAddServices.find('[name="styles"]');
        var $project        = self.$roomAddServices.find('[name="project"]');
        var $complectation  = self.$roomAddServices.find('[name="complectation"]');
        var $nadzor         = self.$roomAddServices.find('[name="nadzor"]');
        var $fullremont     = self.$roomAddServices.find('[name="fullremont"]');
        var $subitems       = self.$roomAddServices.find('.subitems');

        $styles.change(function() {
            var checked = $(this).is(":checked");
            self.additionalCheckboxes.styles = checked;
            self.recalcCost();           
        });
        $project.change(function() {
            var checked = $(this).is(":checked");
            self.additionalCheckboxes.project = checked;            
            self.recalcCost();           
        });
        $complectation.change(function() {
            var checked = $(this).is(":checked");
            self.additionalCheckboxes.complectation = checked;            
            self.recalcCost();           

            if (!self.additionalCheckboxes.project)
                $project.trigger('click');

            if (checked) {
                $project.attr({disabled: 'disabled'});
            } else {
                $project.removeAttr('disabled');
                $project.trigger('click');
            }
        });
        $nadzor.change(function() {
            var checked = $(this).is(":checked");
            self.additionalCheckboxes.nadzor = checked;
            self.recalcCost();           

            if (!self.additionalCheckboxes.complectation)
                $complectation.trigger('click');

            if (checked) {
                $project.attr({disabled: 'disabled'});
                $complectation.attr({disabled: 'disabled'});
            } else {
                $project.removeAttr('disabled');
                $complectation.removeAttr('disabled');
                $complectation.trigger('click');
            }
        });
        $fullremont.change(function() {
            var checked = $(this).is(":checked");
            self.additionalCheckboxes.fullremont = checked;
            self.recalcCost();           

            if (!self.additionalCheckboxes.nadzor)
                $nadzor.trigger('click');

            if (checked) {
                $project.attr({disabled: 'disabled'});
                $complectation.attr({disabled: 'disabled'});
                $nadzor.attr({disabled: 'disabled'});
                $subitems.slideDown();

                // Отключаем галочки подпунктов
                $subitems.find('input:checked').trigger('click');
            } else {
                $project.removeAttr('disabled');
                $complectation.removeAttr('disabled');
                $nadzor.removeAttr('disabled');
                $subitems.slideUp();

                // Отключаем галочки подпунктов
                //$subitems.find('input:checked').trigger('click');

                // NOTE: или использовать такой порядок вызова (три клика на complectation)
                //       или только на nadzor (текущая логика nadzor отключит кнопку complectation)
                //$complectation.trigger('click');
                $nadzor.trigger('click');
            }
        });

        // Обработка подпунктов
        $subitems.find('input[type="checkbox"]').change( function() {
            self.sendRecalc();
        });

        // Init popovers
        var hidePopovers = function() {
            var arr = $('[data-toggle="popover"]');
            arr.each(function() {
                var el = $(this);
                if (el.find('+ .popover').is(':visible'))
                    el.popover('hide');
                    el.removeClass('select');
            });
        }

        $('[data-toggle="popover"]').click(function() {
            var visible = $(this).find('+ .popover').is(':visible');            
            if (visible) {
                hidePopovers();
            } else {
                // need to visible, check previously popover
                if (self.popoverVisible)
                    hidePopovers();
                $(this).popover('show');
                $(this).addClass('select');
            }

            self.popoverVisible = !visible;
            return false;
        });

        // Закрываем окно при нажатии вне него
        $(document).click(function (e) {
            if (!self.popoverVisible)
                return true;        // continue process

            var arr = $('[data-toggle="popover"]');
            arr.each(function() {
                var el = $(this);
                if (el.has(e.target).length)
                    return false;

                var container = el.find('+ .popover');
                if (container.is(':visible')) {
                    if (container.has(e.target).length === 0) {
                        el.trigger('click');
                        return false;
                    }
                }
            });
            return false;       // cancel click flow
        });
    },

    initForm: function()
    {
        var self = this;
        $("[name='phone']").mask("+7 (999) 999-9999");

        self.$form.validate({
            //focusInvalid: false,
            rules: {
                square: {
                    required: true,
                    number: true
                },
                name: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            submitHandler: function(form) {

                // Send pictures
                // Get a unique number from timestamp
                var ts = new Date().getTime();

                var elems = $(".room-data .room-image");
                var count = elems.length;
                var prevTop = $(window).scrollTop();

                elems.each( function(idx, obj) {
                    $(window).scrollTop(0);
                    $(obj).addClass('no-shadow');
                    
                    html2canvas(obj, {
                    }).then(function(canvas) {
                        $(window).scrollTop(prevTop);
                        $(obj).removeClass('no-shadow');

                        // Add watermark
                        var img = new Image();
                        img.src = '../../img/logo.png';
                        img.onload = function() {
                            var imgW = canvas.width*0.25;
                            var imgH = img.height * imgW/img.width;

                            var context = canvas.getContext('2d');
                            context.drawImage(img, canvas.width*0.70, canvas.height*0.85, imgW, imgH);

                            var dataImage = canvas.toDataURL("image/png");

                            $.ajax({
                                type: "POST",
                                url: self.o.savePictureLink,
                                data: { 
                                    data: dataImage,
                                    hash: ts,
                                }
                            }).done(function(fileName) {
                                count--;

                                // Only if last image was send, then we send a form
                                if (count == 0) {
                                    self.sendForm(ts);
                                }
                            }).fail(function (jqXHR, textStatus) {
                                $('#js-modal-error-window')
                                    .modal('show')
                                    .on('hidden.bs.modal', function (e) {
                                        document.location.reload();
                                    });
                            });
                        }
                    });
                });
            }
        });

        // Перехватываем кнопку отправить в плавающей панели
        self.$form.find('button#js-float-panel-submit-btn').click(function() {
            self.$form.find('button#js-submit-btn').trigger('click');
        });

        // Отправляем вручную, чтобы ENTER на элементах не вызывал отправку
        self.$form.find('button#js-submit-btn').click(function() {
            self.$form.submit();
        });
    },

    initFloatPanel: function()
    {
        var self = this;
        var $floatPanelStart    = $('.room-constructor-widget #js-float-panel-start');
        var $floatPanelFinish   = $('.room-constructor-widget #js-float-panel-finish');
        
        var processFloatPanel = function() {
            $isStart = self.checkVisible($floatPanelStart, 'above', self.$floatPanel.height());
            $isFinish = self.checkVisible($floatPanelFinish, 'above', self.$floatPanel.height());
            if ($isStart) {
                self.$floatPanel.show();
                if ($isFinish) {
                    // Show static panel
                    self.$floatPanel.removeClass('float');
                } else {
                    // Show float panel
                    self.$floatPanel.addClass('float');
                }
            } else {
                self.$floatPanel.hide();
            }
        }
        $(window).scroll(processFloatPanel);
        processFloatPanel();
    },

    initTether: function()
    {
        new Tether({
            element: $('.modal-dialog').get(0),
            target: $('body').get(0),
            attachment: 'middle center',
            targetAttachment: 'middle center',
            targetModifier: 'visible'
        });
    },

    initVue: function()
    {
        var self = this;
        self.vueApp = new Vue({
            el: '#vue-rooms-send-form',
            data: {
                showEmail: false,
            }
        });
    },

    sendRecalc: function()
    {
        var self = this;
        
        var subitems = {};
        self.$roomAddServices.find('[name^="resultSubitem"]').each( function() {
            var res = /resultSubitem\[(\d+)\]/.exec($(this).attr('name'));
            if (res == null || res.length != 2) {
                console.error('Correct resultSubitem not found');
            } else {
                var checkboxId = $(this).attr('id');
                var checkboxEl = $(this).next('label[for="'+checkboxId+'"]');
                var checkboxTitle = $.trim(checkboxEl.text());
                subitems[checkboxTitle.split(' ').join('_')] = $(this).is(':checked');
            }
        });

        self.dataFor1C = {
            'rooms': self.getRoomType(),
            'square': self.getSquare(),
            'city': self.o.city,
            'data': self.roomData,
            'fullremont_subitems': subitems
        };
        var data = Object.assign({type:'calc'}, self.dataFor1C);
        
        $.post(
            self.o.recalcLink,
            {'data': data},
            function(data) {
                if (data.errCode != 200) {
                    // Какая-то ошибка, или по таймауту или метод не найден
                    self.recalcCost(true);
                } else {
                    if (data.error != '') {
                        console.error(data.error);
                        self.recalcCost(true);
                    } else {
                        var res = JSON.parse(data.res);
                        self.complectationCost  = res.material_price;
                        self.fullremontCost     = res.black_material_price + res.work_price;
                        self.recalcCost();
                    }
                }
            },
            'json'
        );
    },

    sendForm: function(ts)
    {
        var self = this;
        ts = ts || 0;

        // Отправляем данные для 1с
        var data = Object.assign({
            type: 'order',
            phone: self.$roomsSendForm.find('[name="phone"]').val(),
            email: self.$roomsSendForm.find('[name="email"]').val(),
            name: self.$roomsSendForm.find('[name="name"]').val()
        }, self.dataFor1C);
        //console.log(data);

        $.post(
            self.o.recalcLink,
            {'data': data},
            function(data) {
            },
            'json'
        ).done(function() {
            // Success
            $.ajax({
                type: "POST",
                url: self.o.sendFormLink,
                data: "hash=" + ts + "&" + self.$form.serialize(),
                success: function(data) {
                    // Send the event for Yandex Metrika
                    yaCounter44889733.reachGoal('ORDER');

                    // Send the event for FacebookPixel
                    fbq('track', 'Purchase', {value: 0.00, currency: 'RUB'});
                    fbq('track', 'Order');

                    // Show success modal window
                    $('#js-modal-window')
                        .modal('show')
                        //.on('hidden.bs.modal', function (e) {
                        //    document.location.reload();
                        //});
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('#js-modal-error-window')
                        .modal('show')
                        .on('hidden.bs.modal', function (e) {
                            document.location.reload();
                        });
                }
            });
        });
    },

    postProcessCheckBoxes: function()
    {
        var self = this;

        // Обновляем плавающую панель
        var $infoFixed = self.$fixedPanel.find('#js-fixed-panel-info');
        var $costFixed = self.$fixedPanel.find('#js-fixed-panel-cost');
        var $infoFloat = self.$floatPanel.find('#js-float-panel-info');
        var $costFloat = self.$floatPanel.find('#js-float-panel-cost');

        var cost = 0;
        cost += parseInt(self.$roomAddServices.find('[name="styles"]').data('cost'));
        cost += parseInt(self.$roomAddServices.find('[name="result_project"]').val());
        cost += parseInt(self.$roomAddServices.find('[name="result_complectation"]').val());
        cost += parseInt(self.$roomAddServices.find('[name="result_nadzor"]').val());
        cost += parseInt(self.$roomAddServices.find('[name="result_fullremont"]').val());
        $costFixed.html(self.formatNumber(cost, '&nbsp;'));
        self.$roomAddServices.find('[name="total_price"]').val(cost);

        // Обновляем fixed-panel
        var info = [];
        if (self.additionalCheckboxes.styles)
            info.push('выбор стиля');
        if (self.additionalCheckboxes.project)
            info.push('проект');
        if (self.additionalCheckboxes.complectation)
            info.push('комплектация');
        if (self.additionalCheckboxes.nadzor)
            info.push('надзор');
        if (self.additionalCheckboxes.fullremont)
            info.push('ремонт');
        $infoFixed.text(info.join(', '));

        // Обновляем float-panel
        cost = 0;
        cost += self.$roomAddServices.find('[name="project"]').data('cost');
        cost += self.$roomAddServices.find('[name="complectation"]').data('cost');
        cost += self.$roomAddServices.find('[name="nadzor"]').data('cost');
        cost += self.$roomAddServices.find('[name="fullremont"]').data('cost');
        $costFloat.html(self.formatNumber(cost, '&nbsp;'));

        info = [];
        info[0] = [];
        info[1] = [];
        info[0].push('<b>Проект ремонта:</b> '  + self.$roomAddServices.find('#js-project-cost').html() + '&nbsp;<span class="fa fa-rub"></span>');
        //info[0].push('Материалы '              + self.$roomAddServices.find('#js-complectation-cost').html() + '&nbsp;<span class="fa fa-rub"></span>');
        //info[1].push('Надзор '                 + self.$roomAddServices.find('#js-nadzor-cost').html() + '&nbsp;<span class="fa fa-rub"></span>');
        //info[1].push('Работы '                 + self.$roomAddServices.find('#js-fullremont-cost').html() + '&nbsp;<span class="fa fa-rub"></span>');
        info[0] = info[0].join(",&nbsp;");
        info[1] = info[1].join(",&nbsp;");
        $infoFloat.html(info.join("&nbsp;"));       // <br/>
    },

    getRoomType: function()
    {
        var self = this;
        return self.$roomList.find('[type=radio][name="roomType"]:checked + label').text();
    },

    getSquare: function()
    {
        var self = this;
        var $square = self.$roomSquare.find('[type=text][name="square"]');
        return parseInt($square.val());
    },

    // Пересчитываем стоимость комплектации и ремонта
    recalcCost: function(hasError)
    {
        var self = this;
        hasError = hasError || false;

        var square = self.getSquare();
        var stylesCost = 0;
        var projectCost = self.projectCost;
        var nadzorCost = self.nadzorCost;
        var complectationCost   = (hasError)? self.complectationDefCost * square    : self.complectationCost;
        var fullremontCost      = (hasError)? self.fullremontDefCost * square       : self.fullremontCost;

        var $fullRemontCostFrom = self.$roomAddServices.find('#js-fullremont-cost-from');
        var $fullComplectationCostFrom = self.$roomAddServices.find('#js-complectation-cost-from');
        if (hasError) {
            $fullRemontCostFrom.show();
            $fullComplectationCostFrom.show();
        } else {
            $fullRemontCostFrom.hide();
            $fullComplectationCostFrom.hide();
        }

        self.$roomAddServices.find('#js-project-cost')      .html(self.formatNumber(projectCost, '&nbsp;'));
        self.$roomAddServices.find('#js-nadzor-cost')       .html(self.formatNumber(nadzorCost, '&nbsp;'));
        self.$roomAddServices.find('#js-complectation-cost').html(self.formatNumber(complectationCost, '&nbsp;'));
        self.$roomAddServices.find('#js-fullremont-cost')   .html(self.formatNumber(fullremontCost, '&nbsp;'));

        // Запоминаем цены, чтобы при нажатии галочек выставить их
        self.$roomAddServices.find('[name="project"]')      .data('cost', projectCost);
        self.$roomAddServices.find('[name="nadzor"]')       .data('cost', nadzorCost);
        self.$roomAddServices.find('[name="complectation"]').data('cost', complectationCost);
        self.$roomAddServices.find('[name="fullremont"]')   .data('cost', fullremontCost);

        // Выставляем новую стоимость в поля формы
        self.$roomAddServices.find('[name="result_styles"]')        .val(self.additionalCheckboxes.styles ? stylesCost : 0);
        self.$roomAddServices.find('[name="result_project"]')       .val(self.additionalCheckboxes.project ? projectCost : 0);
        self.$roomAddServices.find('[name="result_complectation"]') .val(self.additionalCheckboxes.complectation ? complectationCost : 0);
        self.$roomAddServices.find('[name="result_nadzor"]')        .val(self.additionalCheckboxes.nadzor ? nadzorCost : 0);
        self.$roomAddServices.find('[name="result_fullremont"]')    .val(self.additionalCheckboxes.fullremont ? fullremontCost : 0);

        self.postProcessCheckBoxes();
    },

    preloadImages: function()
    {
        var self = this;
        var paths  = [];
        
        // <img>
        self.$content.find('img').each(function() {
            paths.push($(this).attr('src'));
        });
        // <input data-image>
        self.$content.find('input[data-image]').each(function() {
            paths.push($(this).data('image'));
        });
         
        $.preloadImages(paths, function () {
            //console.log('ALL IMAGES LOADED');
        });
    },

    formatNumber: function(nStr, sep)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.'+x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + sep + '$2');
        }
        return x1 + x2;
    },

    checkVisible: function(elm, evalType, offsBottom)
    {
        evalType = evalType || "visible";
        offsBottom = offsBottom || 0;

        var vpH = $(window).height(), // Viewport Height
            st = $(window).scrollTop(), // Scroll Top
            y = $(elm).offset().top,
            elementHeight = $(elm).height();

        if (evalType === "visible") return ((y < (vpH + st - offsBottom)) && (y > (st - elementHeight)));
        if (evalType === "above") return ((y < (vpH + st - offsBottom)));
    }
};

