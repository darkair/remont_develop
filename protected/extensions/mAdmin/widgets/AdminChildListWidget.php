<?php
/**
 * Редактируемый список дочерних элементов
 */
class AdminChildListWidget extends ExtendedWidget
{
    public $form;
    public $model;
    public $attribute;
    public $innerChildField = '';
    public $valueField = 'id';
    public $visibleField = 'visible';
    public $textField = 'title';
    public $additionalFields = [];
    public $childClass = null;
    public $editUrl = '';
    public $useEditRaw = false;
    public $useTitle = false;

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('bootbox');
        AdminComponent::getInstance()->assetsRegistry->registerPackage('jquery.ui');
    }

    public function run()
    {
        if (empty($this->model) || !is_object($this->model)) {
            echo 'Error: model incorrect';
            return;
        }

        $childClass = $this->childClass;

        // Берем все объекты, несмотря на их видимость, чтобы можно было на клиенте решать, что же с ними делать
        $listData = [];
        foreach ($childClass::model()->findAll() as $model) {
            $value      = CHtml::value($model, $this->valueField);
            $text       = CHtml::value($model, $this->textField);
            $visible    = CHtml::value($model, $this->visibleField);
            $listData[$value] = ['text' => $text, 'visible'=>$visible];
        }

        $this->render('childList', array(
            'form'                  => $this->form,
            'model'                 => $this->model,
            'modelName'             => get_class($this->model),
            'attribute'             => $this->attribute,
            'data'                  => $this->model->{$this->attribute},
            'selectData'            => $listData,
            'innerChildField'       => $this->innerChildField,

            'childModel'            => $childClass::model(),
            'useTitle'              => $this->useTitle ? 1 : 0,
            'valueField'            => $this->valueField,
            'textField'             => $this->textField,       
            'visibleField'          => $this->visibleField,
            'dataFields'            => array_merge([$this->textField], $this->additionalFields),

            'editUrl'               => $this->editUrl,
            'useEditRaw'            => $this->useEditRaw ? 1 : 0,
        ));
    }
}
