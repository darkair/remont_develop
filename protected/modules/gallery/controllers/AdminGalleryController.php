<?php

Yii::import('modules.gallery.models.*');

class AdminGalleryController extends MAdminController
{
    public $modelName = 'Gallery';
    public $modelHumanTitle = array('галерею', 'галерей', 'галерей');
    public $allowedRoles = 'admin, moderator';
    public $allowedActions = 'edit,update';

    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class' => 'application.behaviors.GalleryControllerBehavior',
                'imageWidth' => GalleryItem::IMAGE_W,
                'imageHeight' => GalleryItem::IMAGE_H,
                'galleryItemClass' => 'GalleryItem',
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array_merge(
            array(
                'visible'   => ['type' => 'checkBox'],
            ),
            $this->getLangField('title',    ['type' => 'textArea']),
            array(
                'images'    => [
                    'type' => 'gallery',
                    'htmlOptions' => [
                        'innerImagesField' => '_images',
                        'innerRemoveField' => '_removeGalleryImageFlags',
                        'galleryItemClass' => 'GalleryItem',
                    ],
                ],
            )
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'id',
            'title',
            $this->getVisibleColumn(),
            $this->getButtonsColumn()
        );
        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->galleryBehavior->galleryBeforeSave($model, $model->getGalleryStorePath());
        parent::beforeSave($model);
    }
}
