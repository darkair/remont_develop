<?php

class m161111_100212_main_menu extends ExtendedDbMigration
{
    public function safeUp()
    {
        $menu = array_merge(
            array('id' => Menu::MAIN_MENU),
            $this->createLangData('name', 'Главное меню', 'menu')               // Yii::t('menu', 'Главное меню')
        );
        $items = array(
//            array(
//                'link' => '',
//                'name' => '',
//                'name_en' => '',
//            ),
        );
        $this->createMenu($menu, $items);
    }

    public function safeDown()
    {
        $this->deleteMenu(Menu::MAIN_MENU);
    }
}
