<?php

Yii::import('modules.contentBlocks.models.ContentBlocks');

class m180910_083305_cb_header extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createContentBlock(
            'Текст в шапке',
            'У нас уже готовы визуализации и рассчитаны сметы материалов от поставщиков.',
            ContentBlocks::POS_HEADER_TEXT_1
        );
    }

    public function safeDown()
    {
        $this->deleteContentBlock(ContentBlocks::POS_HEADER_TEXT_1);
    }
}