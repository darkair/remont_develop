<?php

class m170831_173936_localconfig_1c_requisites extends CDbMigration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO localconfig
                (`name`, `value`, `module`, `description`, `example`, `type`)
            VALUES
                ("ip",          "148.251.146.89",   "1c", "IP",         "127.0.0.1", "string"),
                ("login",       "roomsme",          "1c", "Логин",      "somelogin", "string"),
                ("password",    "petrovich",        "1c", "Пароль",     "somepassword", "string")
        ');
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM localconfig WHERE name IN ('ip', 'login', 'password') AND module='1c'
        ");
    }
}