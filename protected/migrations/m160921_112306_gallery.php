<?php

Yii::import('modules.gallery.models.*');

class m160921_112306_gallery extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `gallery` (
                `id`        int(11) NOT NULL AUTO_INCREMENT,
                `type`      int(11) NOT NULL DEFAULT 0 COMMENT 'Тип',
                `title`     text NOT NULL COMMENT 'Заголовок',
                `images`    TEXT NOT NULL COMMENT 'Галлерея в JSON',
                `visible`   tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `gallery_lang` (
                `l_id` int(11) NOT NULL AUTO_INCREMENT,
                `galleryId` int(11) NOT NULL,
                `lang_id` varchar(6) NOT NULL,
                `l_title` text NOT NULL,
                PRIMARY KEY (`l_id`),
                KEY `galleryId` (`galleryId`),
                KEY `lang_id` (`lang_id`),
                CONSTRAINT `fk_gallery_lang` FOREIGN KEY (`galleryId`) REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");


        $this->execute("
            CREATE TABLE IF NOT EXISTS `galleryitem` (
                `id`        int(11) NOT NULL AUTO_INCREMENT,
                `title`     text NOT NULL COMMENT 'Заголовок',
                `fileName`  varchar(128) NOT NULL DEFAULT '' COMMENT 'Имя файла',
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `galleryitem_lang` (
                `l_id` int(11) NOT NULL AUTO_INCREMENT,
                `galleryItemId` int(11) NOT NULL,
                `lang_id` varchar(6) NOT NULL,
                `l_title` text NOT NULL,
                PRIMARY KEY (`l_id`),
                KEY `galleryItemId` (`galleryItemId`),
                KEY `lang_id` (`lang_id`),
                CONSTRAINT `fk_galleryitem_lang` FOREIGN KEY (`galleryItemId`) REFERENCES `galleryitem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->deleteAllModels('Gallery');
        $this->dropTable('gallery_lang');
        $this->dropTable('gallery');
        $this->dropTable('galleryitem_lang');
        $this->dropTable('galleryitem');
    }
}
