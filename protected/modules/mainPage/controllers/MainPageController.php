<?php

Yii::import('lib.SypexGeo.SxGeo22_API.SxGeo');

class MainPageController extends Controller
{
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $this->setBreadcrumbs([]);

        $this->render('/mainPage', []);
    }

    public function actionGetFile()
    {
        $fileName = Yii::app()->request->getQuery('filename');
        if (empty($fileName))
            throw new CHttpException(404);
        $path = Yii::app()->request->getQuery('path');
        if (empty($path))
            throw new CHttpException(404);

        $storePath = Yii::getPathOfAlias('webroot.store.pdf.'.$path).'/';
        $filePath = $storePath.$fileName;

        if (!EFileHelper::getFile($filePath, $fileName, $fileName))
            throw new CHttpException(404);
            
        Yii::app()->end();
    }
}