<?php

Yii::import('modules.feedback.models.FeedbackSection');

class m170423_103032_localconfig_remove_feedbackEmail extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->execute("
            DELETE FROM localconfig WHERE name IN ('feedbackEmail')
        ");

        $section = new FeedbackSection();
        $section->title = 'Общий раздел';
        $section->email = 'noreply@site.ru';
        $section->visible = 1;
        if (!$section->save())
            throw new CException('Failrure to save FeedbackSection');
    }

    public function safeDown()
    {
        $this->deleteAllModels('FeedbackSection');

        $this->execute('
            INSERT INTO localconfig
                (`name`, `value`, `module`, `description`, `example`, `type`)
            VALUES
                ("feedbackEmail", "noreply@site.ru", "contact-info", "Почта для обратной связи", "noreply@site.ru", "string")
        ');
    }
}
