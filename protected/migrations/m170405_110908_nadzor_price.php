<?php

class m170405_110908_nadzor_price extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE roomblock ADD COLUMN `nadzorCost`   int(10) NOT NULL DEFAULT 0 COMMENT 'Стоимость надзора' AFTER roomGroups");
        $this->execute("ALTER TABLE roomblock ADD COLUMN `projectCost`  int(10) NOT NULL DEFAULT 0 COMMENT 'Стоимость проекта' AFTER roomGroups");
    }

    public function safeDown()
    {
        $this->dropColumn('roomblock', 'nadzorCost');
        $this->dropColumn('roomblock', 'projectCost');
    }
}