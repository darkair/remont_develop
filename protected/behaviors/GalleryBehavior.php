<?php

/**
Usage:
Add to model:
    public $_images = [];                       // UploadedFile[]
    public $_removeGalleryImageFlags = [];      // bool

    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class'                 => 'application.behaviors.GalleryBehavior',
                'storagePath'           => '<STORAGE_PATH>',
                'imagesField'           => 'images',
            ),
        )
    }
    public function attributeLabels()
    {
        return array_merge(
            $this->galleryBehavior->galleryLabels(),
            ...
        );
    }
    public function rules()
    {
        return array_merge(
            $this->galleryBehavior->galleryRules(),
            ...
        );
    }
    public function getGalleryStorePath()
    {
        return $this->galleryBehavior->getStorePath();
    }

    public function getGalleryImageUrl($img)
    {
        return $this->galleryBehavior->getImageUrl($img);
    }

    public function getAbsoluteImagesUrl()
    {
        return $this->galleryBehavior->getAbsoluteImagesUrl();
    }

Add to admin controller:
    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class' => 'application.behaviors.GalleryControllerBehavior',
                'imageWidth' => <IMAGE_W>,
                'imageHeight' => <IMAGE_H>,
            ),
        );
    }
    public function getEditFormElements($model)
    {
        return array(
            'images'    => [
                'type' => 'gallery',
                'htmlOptions' => [
                    'innerImagesField' => '_images',
                    'innerRemoveField' => '_removeGalleryImageFlags'
                ],
            ],
            ...
        );
    }
    public function beforeSave($model)
    {
        $this->galleryBehavior->galleryBeforeSave($model, $model->getGalleryStorePath());
        parent::beforeSave($model);
    }
*/

class GalleryBehavior extends CActiveRecordBehavior
{
    public $galleryItemClass = null;
    public $storagePath = '';
    public $imagesField = '';
    public $innerImageField = '_images';
    public $innerRemoveField = '_removeGalleryImageFlags';


    public function galleryLabels()
    {
        $arr = array(
            $this->imagesField => Yii::t('app', 'Галерея'),
        );
        return $arr;
    }

    public function galleryRules()
    {
        $rules = array();
        $rules[] = array($this->innerImageField, 'safe');
        $rules[] = array($this->innerRemoveField, 'safe');
        return $rules;
    }

    public function getStorePath()
    {
        return Yii::getPathOfAlias('webroot.store.'.$this->storagePath).'/';
    }

    public function getImageUrl($img)
    {
        if (array_search($img, $this->owner->{$this->imagesField}) === false)
            return;
        // Проверяем на абсолютные пути (которые возникают, например, при импорте внешних картинок)
        if (preg_match('/^(http|https):\/\//i', $img))
            return $img;
        return CHtml::normalizeUrl('/store/'.str_replace('.', '/', $this->storagePath).'/'.$img);
    }

    public function getOriginalImageUrl($img)
    {
        if (array_search($img, $this->owner->{$this->imagesField}) === false)
            return;
        return CHtml::normalizeUrl('/store/'.str_replace('.', '/', $this->storagePath).'/original/'.$img);
    }

    public function getAbsoluteImagesUrl()
    {
        return array_map(function($imageName) {
            return $this->getImageUrl($imageName);
        }, $this->owner->{$this->imagesField});
    }

    public function getFirstImageUrl()
    {
        if (empty($this->owner->{$this->imagesField}) || !is_array($this->owner->{$this->imagesField}))
            return '';
        return $this->getImageUrl($this->owner->{$this->imagesField}[0]);
    }

    public function getGalleryItems()
    {
        $class = $this->galleryItemClass;
        if (!$class)
            return null;

        $items = [];
        foreach ($this->owner->{$this->imagesField} as $imgName) {
            $item = $this->getGalleryItemByFileName($imgName);
            if ($item) {
                $item->url = $this->getImageUrl($imgName);
                $items[] = $item;
            }
        }
        return $items;
    }

    public function getGalleryItemByFileName($imgName)
    {
        $class = $this->galleryItemClass;
        if (!$class)
            return null;

        return $class::model()->findByAttributes([
            'fileName' => $imgName
        ]);
    }

    public function afterDelete($event)
    {
        if (!empty($this->owner->{$this->imagesField})) {
            foreach ($this->owner->{$this->imagesField} as $imgName) {
                @unlink( $this->getStorePath().$imgName );
                @unlink( $this->getStorePath().'original/'.$imgName );
            }
        }
        $this->owner->{$this->imagesField} = [];
    }

    public function afterFind($event)
    {
        $this->owner->{$this->imagesField} = json_decode($this->owner->{$this->imagesField}, true);
        if (!is_array($this->owner->{$this->imagesField}))
            $this->owner->{$this->imagesField} = array();

        // Сбрасываем ключи, чтобы можно было брать первую картинку через индекс [0]
        $this->owner->{$this->imagesField} = array_values($this->owner->{$this->imagesField});
    }

    public function beforeSave($event)
    {
        $this->owner->{$this->imagesField} = json_encode($this->owner->{$this->imagesField});
    }
}