<?php

class LanguageWidget extends ExtendedWidget
{
    public function run()
    {
        $currentLang = Yii::app()->language;
        $languages = Yii::app()->params->languages;

        $langArr = array();
        foreach ($languages as $lang=>$name) {
//            $langArr[$lang] = Yii::app()->controller->createUrl('', array_merge($_GET, array('language'=>$lang)));
            $route = '/';
            if (Yii::app()->controller->module)
                $route .= Yii::app()->controller->module->id.'/';
            $route .= Yii::app()->controller->id.'/'.Yii::app()->controller->action->id;
            
            // NOTE:  CHtml::normalizeUrl([0=>$route, 'language'=>$lang]) не работает, т.к. не учитываются входные параметры например ID
            $langArr[$lang] = CHtml::normalizeUrl(array_merge([0=>$route], $_GET, ['language'=>$lang]));

            // Декодируем урл, чтобы параметры вида aaa/bbb отобразились правильно, а не через aaa%2Fbbb
            $langArr[$lang] = urldecode($langArr[$lang]);
        }

        $this->render('language', array(
            'currentLang' => $currentLang,
            'languages' => $langArr
        ));
    }
}
