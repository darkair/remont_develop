<?php

Yii::import('modules.articles.models.*');

class AdminArticlesController extends MAdminController
{
    public $modelName = 'Article';
    public $modelHumanTitle = array('статью', 'статьи', 'статей');
    public $allowedRoles = 'admin, moderator';

    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class' => 'application.behaviors.GalleryControllerBehavior',
                'imageWidth' => Article::IMAGE_W,
                'imageHeight' => Article::IMAGE_H,
            ),
        );
    }

    public function actionAdd()
    {
        if (isset($_POST[$this->modelName]))
            return parent::actionAdd();

        $link = Yii::app()->request->getQuery('link');

        $model = new $this->modelName();
        if ($link)
            $model->link = $link;

        $this->beforeEdit($model);
        $this->render(
            'crud/add',
            array(
                'model' => $model,
                'editFormElements' => $this->getEditFormElements($model),
            )
        );
    }

    public function getEditFormElements($model)
    {
        return array_merge(
            array(
                'visible'   => ['type' => 'checkBox'],
                'link'      => [
                    'type' => 'dropdownlist',
                    'data' => $this->getArticleLinks($model),
                    'htmlOptions' => [
                        'empty' => [''=>'[не выбран]'],
                        //'options' => [2=>['selected'=>true], 3=>['disabled'=>true]]
                    ]
                ],
            ),
            $this->getLangField('title',    ['type' => 'textArea']),
            $this->getLangField('text',     ['type' => 'ckEditor']),
            array(
                'coords'    => ['type' => 'yandexMap'],
                'images'    => [
                    'type' => 'gallery',
                    'htmlOptions' => [
                        'innerImagesField' => '_images',
                        'innerRemoveField' => '_removeGalleryImageFlags'
                    ],
                ],
                array(
                    'class' => 'ext.YiiSEOBehavior.widgets.ESEOEditWidget'
                ),
            )
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'id',
            'link',
            'title',
            $this->getVisibleColumn(),
            $this->getButtonsColumn()
        );
        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->galleryBehavior->galleryBeforeSave($model, $model->getGalleryStorePath());
        parent::beforeSave($model);
    }

    /**
     * Получить список урлов, доступных для изменения
     */
    private function getArticleLinks(&$model)
    {
        $links = [];
        $menuItems = MenuItem::model()->onlyLinks()->orderDefault()->findAll();
        foreach ($menuItems as $item) {
            if (Yii::app()->urlManager->isFixedUrl($item->link))
                continue;

            if (strpos($item->link, 'http://')===0 || strpos($item->link, 'https://')===0)
                continue;

            $links[$item->link] = $item->link;
        }

        // Исключаем уже назначенные урлы
        $articles = Article::model()->onlyLinks()->findAll();
        foreach ($articles as $article) {
            if ($article->link == $model->link)
                continue;
            
            if (isset($links[$article->link]))
                unset($links[$article->link]);
        }

        return $links;
    }
}
