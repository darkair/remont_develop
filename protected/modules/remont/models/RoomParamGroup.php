<?php

/**
 * Группы параметров
 */
class RoomParamGroup extends CActiveRecord
{
    public $roomParamsList = [];
    public $_innerRoomParams = [];

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'roomparamgroup';
    }

    public function behaviors()
    {
        return array(
            'childListBehavior' => array(
                'class'                 => 'application.behaviors.ChildListBehavior',
                'label'                 => 'Параметры',
                'childField'            => 'roomParams',
                'childObjField'         => 'roomParamsList',
                'innerChildField'       => '_innerRoomParams',
                'childClass'            => 'RoomParam',
                'deleteChildren'        => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->childListBehavior->childListLabels(),
            array(
                'title' => 'Заголовок',
                'titleInner' => 'Заголовок для внутреннего отображения',
                'desc' => 'Описание',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->childListBehavior->childListRules(),
            array(
                array('title, desc', 'required'),
                array('titleInner', 'required', 'except'=>'migrate'),
                array('visible', 'boolean'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

//        $alias = $this->getTableAlias();
//        $criteria->compare($alias.'.position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function initRoomParams($ids)
    {
        $this->childListBehavior->initObjects($ids);
    }
}
