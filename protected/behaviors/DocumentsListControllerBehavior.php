<?php

class DocumentsListControllerBehavior extends CBehavior
{
    public $documentsItemClass = null;
    public $docsField = 'docs';
    public $innerDocsField = '_docs';
    public $innerRemoveField = '_removeDocsFlags';

    public function docsListBeforeSave($model, $storagePath)
    {
        // Обрабатываем флаги удаления
        if (!empty($model->{$this->innerRemoveField})) {
            foreach ($model->{$this->innerRemoveField} as $fileName) {
                // removing file
                // set attribute to null
                @unlink( $storagePath.$fileName );

                // Remove document item
                if ($this->documentsItemClass !== null) {
                    $className = $this->documentsItemClass;
                    $docItem = $className::model()->findByAttributes([
                        'filename' => $fileName
                    ]);
                    if ($docItem)
                        $docItem->delete();
                }

                $docsArr = $model->{$this->docsField};
                $key = array_search($fileName, $docsArr);
                if ($key !== false)
                    unset($docsArr[$key]);
                $model->{$this->docsField} = $docsArr;
            }
        }

        // Обрабатываем новые файлы
        $model->{$this->innerDocsField} = CUploadedFile::getInstances($model, $this->innerDocsField);

        if ($model->validate(array($this->innerDocsField)) && !empty($model->{$this->innerDocsField})) {
            // saving file from CUploadFile instance $model->{$this->innerDocsField}
            if (!is_dir($storagePath))
                mkdir($storagePath, 0755, true);

            $files = [];
            foreach ($model->{$this->innerDocsField} as $file) {
                $origFileName = basename($file->name);
                $ext = strrchr($origFileName, '.');
                $fileName = md5(time().$origFileName).($ext?$ext:'');
                $file->saveAs($storagePath.$fileName);
                $files[] = $fileName;

                // Create documents item
                if ($this->documentsItemClass !== null) {
                    $className = $this->documentsItemClass;
                    $docItem = $className::model()->findByAttributes([
                        'filename' => $fileName
                    ]);
                    if (!$docItem) {
                        $docItem = new $className;
                        $docItem->setAttributes([
                            'title' => '',
                            'filename' => $fileName,
                            'orig' => $origFileName
                        ]);
                        $docItem->save();
                    }
                }
            }
            if (!is_array($model->{$this->docsField}))
                $model->{$this->docsField} = array();
            $model->{$this->docsField} = array_merge($model->{$this->docsField}, $files);
        }
    }
}