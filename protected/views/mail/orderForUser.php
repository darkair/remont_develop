<h2>Здравствуйте, <?= $name ?>!</h2>

Вы выбрали следующие параметры планировки:<br/>

Количество комнат: <?= $roomsNumber ?><br/>
Площадь: <?= $square ?> кв.м.<br/>
<hr/>

<h2>Что мы делаем</h2>

Выбор стилей: <?= $resultStyles ?><br/>
Готовый проект: <?= $resultProject ?><br/>
Комплектация: <?= $resultComplectation ?><br/>
Надзор: <?= $resultNadzor ?><br/>
Полный ремонт под ключ: <?= $resultFullremont ?><br/>
<?php
    if (!empty($subparagraphs)):
?>
В том числе:<br/>
<?= $subparagraphs ?><br/>
<?php
    endif;
?>

Итоговая стоимость: <?= $totalPrice ?><br/>

<h2>Параметры выбранных комнат</h2>

<?= $roomParam ?><br/>

<h2>Контакты</h2>

Телефон и Telegram: <?= $adminPhone ?><br/>
E-mail: <a href='mailto:roomsme.ru@gmail.com'>roomsme.ru@gmail.com</a><br/>
