<?php

Yii::import('modules.contentBlocks.models.ContentBlocks');

class m170622_103614_cb_checkbox_styles extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createContentBlock(
            'Описание чекбокса "стили"',
            '',
            ContentBlocks::POS_CHECKBOX_STYLES
        );
        $this->createContentBlock(
            'Подсказка чекбокса "стили"',
            'Выбор стиля - бесплатно.',
            ContentBlocks::POS_POPOVER_STYLES
        );
    }

    public function safeDown()
    {
        $this->deleteContentBlock(ContentBlocks::POS_CHECKBOX_STYLES);
        $this->deleteContentBlock(ContentBlocks::POS_POPOVER_STYLES);
    }
}