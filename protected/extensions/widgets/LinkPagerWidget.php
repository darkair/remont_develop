<?php

class LinkPagerWidget extends CLinkPager
{
    public $firstPageLabel = false;
    public $lastPageLabel = false;
    public $prevPageLabel = '<i class="fa fa-angle-double-left"></i>';
    public $nextPageLabel = '<i class="fa fa-angle-double-right"></i>';
    public $header = '';
    public $hiddenPageCssClass = 'disabled';
    public $innerLinkClass = 'btn no-border btn-white';

    public $useGlobalFormData = false;

    public function init()
    {
        parent::init();
        $this->htmlOptions['class'] = 'linkPager';

        $this->cssFile = Yii::app()->assetManager->publish(Yii::getPathOfAlias('ext.widgets.css')."/linkPager.css");
    }

    public function run()
    {
        $class = $this->htmlOptions['class'];

        $this->htmlOptions['class'] = $class.' hidden-xs';
        parent::run();

        $this->htmlOptions['class'] = $class.' visible-xs-inline-block';
        $this->maxButtonCount = 5;
        parent::run();

        if ($this->useGlobalFormData) {
            // Отправляем данные через форму
            $id = $this->getId();
            GlobalFormData::createSubmitScript(__CLASS__.'#'.$id, "#{$id} li a", 'href');
        }
    }

    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        if($hidden || $selected)
            $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        return '<li class="'.$class.'">'.
                    CHtml::link($label,$this->createPageUrl($page), ['class'=>$this->innerLinkClass]).'
                </li>';
    }
}
