<?php

class m170422_004836_localconfig_change_type extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE localconfig
            MODIFY COLUMN `type` ENUM('bool', 'int', 'fixedarray', 'dynamicarray', 'string', 'multilinestring', 'file', 'twopowarray', 'dropdown')
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE localconfig
            MODIFY COLUMN `type` ENUM('bool', 'int', 'fixedarray', 'dynamicarray', 'string', 'multilinestring', 'file', 'twopowarray')
        ");
    }
}