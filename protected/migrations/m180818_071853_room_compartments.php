<?php

class m180818_071853_room_compartments extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `roomcompartment` (
                `id`                int(11) NOT NULL AUTO_INCREMENT,
                `title`             varchar(255) NOT NULL DEFAULT ''    COMMENT 'Заголовок',
                `roomBlocks`        text NOT NULL                       COMMENT 'Id комнатных блоков в JSON',
                `visible`           tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8
        ");

        $sql = "SELECT id, title FROM roomblock";
        $blocks = $this->getDbConnection()->createCommand($sql)->queryAll();
        $arr = [];
        foreach ($blocks as $block) {
            $this->insert('roomcompartment', [
                'title' => $block['title'],
                'roomBlocks' => json_encode(["1"=>$block['id']]),
                'visible' => true
            ]);
            $lastId = $this->getDbConnection()->createCommand("SELECT LAST_INSERT_ID()")->queryScalar();
            $arr[$block['id']] = $lastId;
        }

        $sql = "SELECT * FROM roomtype";
        $types = $this->getDbConnection()->createCommand($sql)->queryAll();
        foreach ($types as $roomType) {
            $blocks = json_decode($roomType['roomCompartments'], true);
            foreach ($blocks as &$block) {
                $block = $arr[$block];
            }
            $this->execute("
                UPDATE roomtype SET roomCompartments='".json_encode($blocks)."' WHERE id=".$roomType['id']
            );
        }
    }

    public function safeDown()
    {
        $sql = "SELECT * FROM roomcompartment";
        $blocks = $this->getDbConnection()->createCommand($sql)->queryAll();
        $arr = [];
        foreach ($blocks as $block) {
            $arr[$block['id']] = array_shift(json_decode($block['roomBlocks'], true));
        }
        $sql = "SELECT * FROM roomtype";
        $types = $this->getDbConnection()->createCommand($sql)->queryAll();
        foreach ($types as $roomType) {
            $blocks = json_decode($roomType['roomCompartments'], true);
            foreach ($blocks as &$block) {
                $block = $arr[$block];
            }
            $this->execute("
                UPDATE roomtype SET roomCompartments='".json_encode($blocks)."' WHERE id=".$roomType['id']
            );
        }
        $this->dropTable('roomcompartment');
    }
}