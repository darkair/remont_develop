<?php

/**
 * Комнатный блок
 */
class RoomParam extends CActiveRecord
{
    const IMAGE_W = 981;
    const IMAGE_H = 589;

    public $_image = null;
    public $_removeImageFlag = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'roomparam';
    }

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageBehavior',
                'storagePath' => 'rooms',
                'imageField' => 'image',
                'imageExt' => 'png, jpeg, jpg',
                'required' => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->imageBehavior->imageLabels(),
            array(
                'title' => 'Заголовок',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->imageBehavior->imageRules('', 'migrate'),
            array(
                array('image', 'safe', 'on'=>'migrate'),
                array('title', 'required'),
                array('visible', 'boolean'),
            )
        );
    }

    /*
     Отмечаем значком "required"
     */
    public function isAttributeRequired($attribute)
    {
        if (in_array($attribute, array('_image')))
            return true;
        return parent::isAttributeRequired($attribute);
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

//        $alias = $this->getTableAlias();
//        $criteria->compare($alias.'.position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getImageStorePath()
    {
        return $this->imageBehavior->getStorePath();
    }

    public function getImageUrl()
    {
        return $this->imageBehavior->getImageUrl();
    }
}
