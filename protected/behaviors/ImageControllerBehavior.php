<?php

/**
 * v2.0
 *
 * Изображение
    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageControllerBehavior',
                'imageField' => 'image',
                'imageWidth' => <MODEL>::IMAGE_W,
                'imageHeight' => <MODEL>::IMAGE_H,
                'resizeType' => 'cover',
            ),
        );
    }

 * добавить колонку в adminController::getTableColumns()
            $this->getImageColumn('image', 'getImageUrl()'),
 * добавить, если необходимо, поле в форму в adminController::getEditFormElements()
            '_image' => array(
                'class' => 'ext.ImageFileRowWidget',
                'uploadedFileFieldName' => '_image',
                'removeImageFieldName' => '_removeImageFlag',
            ),
 * добавить сохранение в Admin<SMTH>Controller
    public function beforeSave($model)
    {
        $this->imageBehavior->imageBeforeSave($model, $model->getImageStorePath());
        parent::beforeSave($model);
    }

 */

class ImageControllerBehavior extends CBehavior
{
    public $imageField = '';
    public $innerImageField = '_image';
    public $innerRemoveBtnField = '_removeImageFlag';
    public $imageWidth = null;
    public $imageHeight = null;
    public $resizeType = 'contain';     // contain / cover

    public function imageBeforeSave($model, $storagePath)
    {
        // path to original
        $storageOrig = $storagePath.'original/';

        // removing file & set attribute to null
        if ($model->{$this->innerRemoveBtnField}) {
            @unlink( $storagePath.$model->{$this->imageField} );
            @unlink( $storageOrig.$model->{$this->imageField} );
            $model->{$this->imageField} = null;
        }

        $model->{$this->innerImageField} = CUploadedFile::getInstance($model, $this->innerImageField);

        // Валидируем
        if ($model->validate(array($this->innerImageField)) && !empty($model->{$this->innerImageField})) {
            // Не помню зачем, но удаляем еще раз
            if ($model->{$this->imageField}) {
                @unlink( $storagePath.$model->{$this->imageField} );
                @unlink( $storageOrig.$model->{$this->imageField} );
                $model->{$this->imageField} = null;
            }
            
            // saving file from CUploadFile instance $model->{$this->innerImageField}
            if (!is_dir($storagePath))
                mkdir($storagePath, 0755, true);
            if (!is_dir($storageOrig))
                mkdir($storageOrig, 0755, true);

            $imageName = basename($model->{$this->innerImageField}->name);
            $ext = strrchr($imageName, '.');
            $imageName = md5(time().$imageName).($ext?$ext:'');

            $model->{$this->innerImageField}->saveAs( $storageOrig.$imageName );
            
            // Загружаем данные изображения
            $image = Yii::app()->image->load($storageOrig.$imageName);

            if (!empty($this->imageWidth) && !empty($this->imageHeight)) {
                $cW = $image->width / $this->imageWidth;
                $cH = $image->height / $this->imageHeight;
                switch ($this->resizeType) {
                    // Изображение всегда будет видно полностью, т.о. оно может быть меньше контента
                    case 'contain':
                        if ($cW > $cH) {
                            // Выравниваем по ширине
                            $resizeW = $this->imageWidth;
                            $resizeH = $image->height * $this->imageWidth / $image->width;
                        } else {
                            // Выравниваем по высоте
                            $resizeH = $this->imageHeight;
                            $resizeW = $image->width * $this->imageHeight / $image->height;
                        }
                        break;

                    // Изображение всегда заполняет весь контент, т.о. оно может быть обрезано
                    case 'cover':
                        if ($cW <= $cH) {
                            $resizeW = $this->imageWidth;
                            $resizeH = $image->height * $this->imageWidth / $image->width;
                        } else {
                            $resizeH = $this->imageHeight;
                            $resizeW = $image->width * $this->imageHeight / $image->height;
                        }
                        break;
                }
                $image->resize($resizeW, $resizeH)
                      ->crop($this->imageWidth, $this->imageHeight);
            } else
            if (!empty($this->imageHeight)) {
                // Resize by height
                $image->resize(null, $this->imageHeight);
            } else
            if (!empty($this->imageWidth)) {
                // Resize by width
                $image->resize($this->imageWidth, null);
            }

            // rewrite under other name
            $image->save($storagePath.$imageName);

            $model->{$this->imageField} = $imageName;
        }
    }
}