<?php
/**
 * Список документов
 */
class AdminDocumentsListWidget extends ExtendedWidget
{
    public $form;
    public $model;
    public $attribute;
    public $innerDocsField;
    public $innerRemoveField;
    public $documentsItemClass = null;

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('bootbox');
    }

    public function run()
    {
        if (empty($this->model) || !is_object($this->model)) {
            echo 'Error: model incorrect';
            return;
        }
        $this->render('documentsList', array(
            'form'                  => $this->form,
            'model'                 => $this->model,
            'attribute'             => $this->attribute,
            'modelName'             => get_class($this->model),
            'innerDocsField'        => $this->innerDocsField,
            'innerRemoveField'      => $this->innerRemoveField,
            'documentsItemClass'    => $this->documentsItemClass,
        ));
    }
}
