<?php

/**
 * Сортировка
 * подключить в behaviors:
            'orderBehavior' => array(
                'class' => 'application.behaviors.OrderBehavior',
            ),
 * добавить названия поля в attributeLabels:
            $this->orderBehavior->orderLabels(),
 * добавить правила в rules:
            $this->orderBehavior->orderRules(),
 * добавить установку аттрибута:
    public function setAttribute($name, $value)
    {
        $this->orderBehavior->orderSetAttribute($name, $value);
        return parent::setAttribute($name, $value);
    }
 * добавить сортировку по полю в scopes:
            'orderDefault' => array(
                'order' => $alias.'.orderNum ASC',
            ),
 * добавить сортировку в search:
            'sort' => array(
                'defaultOrder' => array(
                    'orderNum' => CSort::SORT_ASC,
                )
            )
 * добавить колонку в adminController::getTableColumns()
            $this->getOrderColumn(),

 */

class OrderBehavior extends CActiveRecordBehavior
{
    public $orderField = 'orderNum';
    public $defaultOrder = CSort::SORT_DESC;            // Insert new element into back or front
    private $moveToUp = true;

    public function orderLabels()
    {
        return array(
            $this->orderField => Yii::t('app', 'Порядок сортировки'),
        );
    }

    public function orderRules()
    {
        return array(
            array($this->orderField, 'numerical', 'integerOnly'=>true),
        );
    }

    public function orderSetAttribute($name, $value)
    {
        if ($name == $this->orderField) {
            // Выясняем, в какую сторону переместили запись (moveToUp - на возрастание)
            $this->moveToUp = $value < $this->owner->{$this->orderField}
                ? false
                : true;
        }
    }

    public function beforeSave($event)
    {
        if (empty($this->owner->{$this->orderField})) {
            if ($this->defaultOrder == CSort::SORT_ASC) {
                // Вставка в начало
                $sql = 'SELECT MIN('.$this->orderField.')-1 as '.$this->orderField.' FROM '.$this->owner->tableName();
            } else {
                // Вставка в конец
                $sql = 'SELECT MAX('.$this->orderField.')+1 as '.$this->orderField.' FROM '.$this->owner->tableName();
            }
            // Автоматическое выставление orderNum
            $orderNum = Yii::app()->db->createCommand($sql)->queryScalar();
            $this->owner->{$this->orderField} = ($orderNum === null) ? 1 : $orderNum;
        } else {
            // Проверяем существующий orderNum
            $sql = 'SELECT id FROM '.$this->owner->tableName().' WHERE '.$this->orderField.'='.$this->owner->{$this->orderField};
            $row = Yii::app()->db->createCommand($sql)->queryRow();
            if ($row  &&  $row['id'] != $this->owner->id) {
                // Пересортируем все записи до конца
                $sql = $this->moveToUp
                    ? 'UPDATE '.$this->owner->tableName().' SET '.$this->orderField.' = '.$this->orderField.'-1 WHERE '.$this->orderField.' <= '.$this->owner->{$this->orderField}
                    : 'UPDATE '.$this->owner->tableName().' SET '.$this->orderField.' = '.$this->orderField.'+1 WHERE '.$this->orderField.' >= '.$this->owner->{$this->orderField};
                Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }

    public function afterSave($event)
    {
        $sql = 'SELECT id, '.$this->orderField.' FROM '.$this->owner->tableName().' order by '.$this->orderField;
        $res = Yii::app()->db->createCommand($sql)->queryAll();
        
        // Переназначаем значение orderNum
        $index = 1;
        $valueStr = '';
        foreach ($res as $v) {
            if ($v[$this->orderField] != $index)
                $valueStr .= ' when id="'.$v['id'].'" then '.$index;
            $index++;
        }
        if (!empty($valueStr)) {
            $sql = 'UPDATE '.$this->owner->tableName().' SET '.$this->orderField.' = case '.$valueStr.' else '.$this->orderField.' end';
            Yii::app()->db->createCommand($sql)->execute();
        }
    }
}
