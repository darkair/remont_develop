<?php

Yii::import('modules.faq.models.*');

class m161231_100914_faq_data extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createFaq('Что включено в минимальный ремонт?', 'Описание, что включено в минимальный ремонт');
        $this->createFaq('Как работает надзор над проектом?', 'Описание, как работает надзор над проектом');
        $this->createFaq('Вы используете обои в отделке стен?', 'Мы считаем, что обои должны остаться в СССР вместе с потолочными люстрами и линолеумом, а их место должны занять более современные технические решения и материалы.<br/>И неплохо бы сразу рассказать, какие, что они дешевле и долговечней и вот это вот всё.');
        $this->createFaq('Зачем мне просто проект для ремонта?', 'Описание, зачем просто проект');
        $this->createFaq('Где вы будете покупать материалы?', 'Описание, откуда материалы');
        $this->createFaq('Кто будет делать ремонт в моей квартире?', 'Описание, кто будет делать ремонт');
    }

    public function safeDown()
    {
        $this->deleteAllModels('Faq');
    }

    private function createFaq($question, $answer)
    {
        $faq = new Faq();
        $faq->question = $question;
        $faq->answer = $answer;
        $faq->save();
    }
}