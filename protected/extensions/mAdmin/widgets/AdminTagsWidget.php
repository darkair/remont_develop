<?php
/**
 * Виджет тегов
 */

class AdminTagsWidget extends ExtendedWidget
{
    public $form;
    public $model;
    public $attribute;
    public $className = 'Tags';
    public $field = 'tags';
    public $innerField = '_tags';

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('tags');
    }


    public function run()
    {
        if (empty($this->model) || !is_object($this->model)) {
            echo 'Error: model incorrect';
            return;
        }

        $class = $this->className;

        $count = $class::model()->count();
        $tagsArr = [];
        $offs = 0;
        while ($offs < $count) {
            $criteria = new CDbCriteria();
            $criteria->select = 'name';
            $criteria->offset = $offs;
            $criteria->limit = 1000;
            $tags = $class::model()->findAll($criteria);

            $tagsArr = array_merge(
                $tagsArr,
                array_map(function(&$item) {
                    return $item->name;
                }, $tags)
            );
            $offs += 1000;
        }
        $this->render('tags', array(
            'form'              => $this->form,
            'model'             => $this->model,
            'attribute'         => $this->attribute,
            'tagsArr'           => "['".implode("','", $tagsArr)."']",
            'data'              => $this->model->{$this->field},
            'innerField'        => $this->innerField,
        ));
    }
}
