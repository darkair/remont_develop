<?php

Yii::import('modules.contentBlock.models.ContentBlocks');

class ContentBlocksWidget extends ExtendedWidget
{
    public $showTitle = false;
    public $position = ContentBlocks::POS_NONE;
    public $raw = false;

    public function run()
    {
        if ($this->position == ContentBlocks::POS_NONE)
            return;

        $contentBlock = ContentBlocks::model()->onSite()->byPosition($this->position)->find();
        if (empty($contentBlock))
            return;

        // Не выводим пустой блок совсем
        if (!$this->showTitle && empty($contentBlock->text))
            return;

        $text = $contentBlock->text;
        if ($this->raw) {
            $text = str_replace('</p>', '<br/>', $text);
            $text = str_replace('<p>', '', $text);
        }

        $this->render('contentBlock', array(
            'text' => $text,
            'title' => $contentBlock->title,
            'showTitle' => $this->showTitle,
            'showRaw' => $this->raw
        ));
    }
}
