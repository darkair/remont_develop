<?php
/**
 * Редактируемый список точек
 */
class AdminPathPointsWidget extends ExtendedWidget
{
    public $form;
    public $model;
    public $attribute;
    public $innerChildField = '';

    public function init()
    {
        parent::init();
        AdminComponent::getInstance()->assetsRegistry->registerPackage('bootbox');
        AdminComponent::getInstance()->assetsRegistry->registerPackage('jquery.ui');
    }

    public function run()
    {
        if (empty($this->model) || !is_object($this->model)) {
            echo 'Error: model incorrect';
            return;
        }

        $this->render('pathPoints', array(
            'form'                  => $this->form,
            'model'                 => $this->model,
            'modelName'             => get_class($this->model),
            'attribute'             => $this->attribute,
            'innerChildField'       => $this->innerChildField,

            'defaultLatitude'       => Yii::app()->params['defaultLatitude'],
            'defaultLongitude'      => Yii::app()->params['defaultLongitude'],
            'defaultZoom'           => Yii::app()->params['defaultZoom'],
        ));
    }
}
