<?php

Yii::import('modules.contentBlocks.models.ContentBlocks');

class m170623_080009_cb_checkbox_titles extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createContentBlock(
            'Название чекбокса "проект"',
            'Проект вашего ремонта',
            ContentBlocks::POS_TITLE_PROJECT
        );
        $this->createContentBlock(
            'Название чекбокса "комплектация"',
            'Комплект строительных материалов',
            ContentBlocks::POS_TITLE_COMPLECTATION
        );
        $this->createContentBlock(
            'Название чекбокса "надзор"',
            'Надзор и контроль качества',
            ContentBlocks::POS_TITLE_NADZOR
        );
        $this->createContentBlock(
            'Название чекбокса "полный ремонт"',
            'Полный ремонт под ключ',
            ContentBlocks::POS_TITLE_FULLREMONT
        );
        $this->createContentBlock(
            'Название чекбокса "стили"',
            'Выбор стиля',
            ContentBlocks::POS_TITLE_STYLES
        );
    }

    public function safeDown()
    {
        $this->deleteContentBlock(ContentBlocks::POS_TITLE_PROJECT);
        $this->deleteContentBlock(ContentBlocks::POS_TITLE_COMPLECTATION);
        $this->deleteContentBlock(ContentBlocks::POS_TITLE_NADZOR);
        $this->deleteContentBlock(ContentBlocks::POS_TITLE_FULLREMONT);
        $this->deleteContentBlock(ContentBlocks::POS_TITLE_STYLES);
    }
}