<?php

class OpenGraphWidget extends ExtendedWidget
{
    public $title = '';
    public $desc = '';
    public $image = null;
    public $url = null;             // Optional, only if need change default url of page
    public $type = 'article';       // https://developers.facebook.com/docs/reference/opengraph#object-type

    public function run()
    {
        $url = empty($this->url)
            ? Yii::app()->createAbsoluteUrl( Yii::app()->request->getPathInfo() )
            : $this->url;
        $siteName = Yii::app()->name;   // og:site_name

        echo CHtml::tag('meta', ['property'=>'og:url', 'content'=>$url]);
        echo CHtml::tag('meta', ['property'=>'og:type', 'content'=>$this->type]);
        echo CHtml::tag('meta', ['property'=>'og:site_name', 'content'=>$siteName]);
        if (!empty($this->title))
            echo CHtml::tag('meta', ['property'=>'og:title', 'content'=>$this->title]);
        if (!empty($this->desc))
            echo CHtml::tag('meta', ['property'=>'og:description', 'content'=>$this->desc]);
        if (!empty($this->image))
            echo CHtml::tag('meta', ['property'=>'og:image', 'content'=>rtrim(Yii::app()->createAbsoluteUrl($this->image), '/')]);
    }
}
