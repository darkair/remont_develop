<?php

class RemontModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.remont.models.*');
    }
}
