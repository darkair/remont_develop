<?php

Yii::import('modules.admin.controllers.*');

final class AdminUploadProjectFileController extends AdminUploadFileController
{
    protected function getStorePath()
    {
        return Yii::getPathOfAlias('webroot.store.pdf.project').'/';
    }

    protected function getRoutePath()
    {
        return 'adminUploadProjectFile';
    }
}
