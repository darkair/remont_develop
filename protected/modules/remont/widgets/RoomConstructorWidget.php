<?php

Yii::import('modules.remont.models.*');

class RoomConstructorWidget extends ExtendedWidget
{
    public $version = 1;

    public function run()
    {
        $projectFilePdf = $this->getPdfFile('webroot.store.pdf.project');
        $smetaFilePdf = $this->getPdfFile('webroot.store.pdf.smeta');

        $rooms = $this->listData(RoomType::model()->onSite()->orderDefault()->findAll(), 'roomsNumber', ['square', 'squareMin'], 'id');
        $subitems = Subparagraph::model()->onSite()->orderDefault()->findAll();

        if (isset($_GET['constructor'])) {
            $this->version = $_GET['constructor'];
        }
        $template = 'roomConstructor' . ($this->version == 1 ? '' : '2');

        $this->render($template, array(
            'rooms' => $rooms,
            'subitems' => $subitems,
            'projectFilePdf' => $projectFilePdf,
            'smetaFilePdf' => $smetaFilePdf
        ));
    }

    private static function listData($models, $valueField, $textFields, $groupField='')
    {
        $listData = array();
        foreach ($models as $model) {
            $group = CHtml::value($model, $groupField);
            $value = CHtml::value($model, $valueField);

            $text = [];
            foreach ($textFields as $textField) {
                $text[$textField] = CHtml::value($model, $textField);
            }
            $listData[$group][$value] = $text;
        }
        return $listData;
    }

    public function getPdfFile($alias)
    {
        $storePath = Yii::getPathOfAlias($alias).'/';
        $filePdf = false;
        $objs = glob($storePath."*.pdf");
        if ($objs) {
            foreach($objs as $obj) {
                if (is_file($obj)) {
                    $filePdf = basename($obj);
                    break;
                }
            }
        }
        return $filePdf;
    }
}
