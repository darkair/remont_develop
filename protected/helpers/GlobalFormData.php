<?php

/**
 * Сохранение и обработка параметров из разных виджетов
 */

class GlobalFormData
{
    const FORM_NAME = 'globalFormData';

    private static $data = [];

    public static function store(&$arr, $key)
    {
        if (!isset($arr[$key]))
            return;

        if (!is_array($arr[$key])) {
            self::$data[$key] = $arr[$key];
        } else {
            foreach ($arr[$key] as $k=>$v) {
                self::$data[$key.'['.$k.']'] = $v;
            }
        }
    }

    public static function createForm()
    {
        echo "<form method='POST' action='/' id='".self::FORM_NAME."'>";
        foreach (self::$data as $k => $v) {
            echo "<input type='hidden' name='".$k."' value='".$v."'>";
        }
        echo "</form>";
    }

    public static function createSubmitScript($id, $selector, $attr)
    {
        $formName = self::FORM_NAME;
        ob_start();
        echo <<<EOD
            jQuery('{$selector}').click(function() {
                var action = $(this).attr('{$attr}');
                $('form#{$formName}')
                    .attr('action', action)
                    .submit();
                return false;
            });
EOD;
        Yii::app()->getClientScript()->registerScript($id, ob_get_clean().';');
    }
}