<?php

Yii::import('modules.feedback.models.*');

class FeedbackWidget extends ExtendedWidget
{
    public function run()
    {
        Yii::app()->clientScript->registerCoreScript('maskedinput');

        $sections = FeedbackSection::model()->onSite()->orderDefault()->findAll();

        $model = new FeedbackForm;
        $this->render('feedback', array(
            'model' => $model,
            'sections' => CHtml::listData($sections, 'id', 'title')
        ));
    }
}
