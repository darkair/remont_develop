var reviews ={
    o: {
        containerSelector: ''
    },
    minHeight: 27*8,
    $widget: null,

    init: function(options) {
        var self = this;

        self.o = $.extend(self.o, options);
        self.$widget = $(self.o.containerSelector);

        self.initContent();
        self.initEvents();

        self.$widget.find('.readmore-hide').trigger('click');
    },

    initContent: function () {
        var self = this;

        self.$widget.find('.overflow-block').each( function() {
            var h = $(this).height();
            $(this)
                .data('height', h)
                .css('height', h);

            // Remove show/hide buttons if height less than minHeight
            if (h <= self.minHeight) {
                $(this).parent().find('.readmore-show, .readmore-hide').remove();
            }
        });
    },

    initEvents: function () {
        var self = this;

        self.$widget.on('click', '.readmore-show', function() {
            $(this).addClass('hide');
            $(this).parent().find('.readmore-hide').removeClass('hide');

            var $overflowBlock =  $(this).parent().find('.overflow-block');
            $overflowBlock.animate({height: $overflowBlock.data('height')}, 250);
            return false;
        });
        self.$widget.on('click', '.readmore-hide', function() {
            $(this).addClass('hide');
            $(this).parent().find('.readmore-show').removeClass('hide');
            $(this).parent().find('.overflow-block').animate({height: self.minHeight}, 250);
            return false;
        });
    }
}
