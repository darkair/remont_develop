<?php

class MailHelper
{
    public static function createYiiMailer($template, $subject, $email, $data)
    {
        // Отправка письма
        $mail = new YiiMailer($template, $data);

        $mail->setFrom(Yii::app()->params['adminEmail'], Yii::app()->params['appName']);
        $mail->setSubject($subject);
        $mail->setTo($email);
        
        // Для рабочей версии вызаваем отправку через mail()
        // Вообще отрубаем отправку через SMTP, чтобы можно было тестить на рабочем сервере
        if (true || !YII_DEBUG) {
            $mail->isMail();
        } else {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'noreply@roomsme.com';
            $mail->Password = 'mbdyGF2U87';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
        }

        return $mail;
    } 
}