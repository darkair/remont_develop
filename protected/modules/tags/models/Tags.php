<?php

class Tags extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'tags';
    }

    public function __toString()
    {
        return $this->name;
    }

    public function attributeLabels()
    {
        return array(
            'name' => 'Тег',
            'refCount' => 'Количество внешних ссылок'
        );
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('refCount', 'numerical', 'integerOnly'=>true),
            
            array('name', 'safe', 'on'=>'search'),
        );
    }

    public function byName($name)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.".name = :name",
            'params' => array(
                ':name' => $name
            )
        ));
        return $this;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }
}
