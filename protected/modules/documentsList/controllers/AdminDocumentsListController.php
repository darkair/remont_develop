<?php

Yii::import('modules.documentsList.models.*');

class AdminDocumentsListController extends MAdminController
{
    public $modelName = 'DocumentsList';
    public $modelHumanTitle = array('список документов', 'списка документов', 'документов');
    public $allowedRoles = 'admin, moderator';
    public $allowedActions = 'edit,update';

    public function behaviors()
    {
        return array(
            'documentsListBehavior' => array(
                'class' => 'application.behaviors.DocumentsListControllerBehavior',
                'documentsItemClass' => 'DocumentsItem',
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array_merge(
            array(
                'visible'   => ['type' => 'checkBox'],
            ),
            $this->getLangField('title',    ['type' => 'textField']),
            array(
                'docs'    => [
                    'type' => 'documentsList',
                    'htmlOptions' => [
                        'innerDocsField' => '_docs',
                        'innerRemoveField' => '_removeDocsFlags',
                        'documentsItemClass' => 'DocumentsItem',
                    ],
                ],
            )
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'id',
            'title',
            $this->getVisibleColumn(),
            $this->getButtonsColumn()
        );
        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->documentsListBehavior->docsListBeforeSave($model, $model->getDocumentsStorePath());
        parent::beforeSave($model);
    }
}
