<?php

return array(
    'appName'           => 'Roomsme',
    'yiiDebug'          => false,

    'dbHost'            => '127.0.0.1',
    'dbName'            => 'u58171',
    'dbLogin'           => 'u58171',
    'dbPassword'        => 'ahxaza5ouy',

    'imageDriver'       => 'GD', // Image_GD_Driver,Image_ImageMagick_Driver

    'baseUrl'           => '', // '/cabinet'

    'adminEmail'        => 'noreply@roomsme.ru',                // noreply@mfc.yekaterinburg.ru, Адрес, от которого отправлять письма

    'translateKey'      => 'trnsl.1.1.20160406T110544Z.e602431cea20f040.99d05e31800a73d996b1fb124c6e23e1c1019951',

    'defaultLatitude' => '56.839228812748',
    'defaultLongitude' => '60.608463496044',
    'defaultZoom' => 12,

    'languages' => array(
        'ru' => 'Russian',
    ),

    'jsonld' => array(
        '@type' => 'Organization',
        "contactPoint" => (object)array(
            "@type" => 'ContactPoint',
            "telephone" => "",
            "email" => "",
            "contactType" => 'RoomsMe'
        )
    )
);
