<?php

class AdminSubparagraphsController extends MAdminController
{
    public $modelName = 'Subparagraph';
    public $modelHumanTitle = array('подпункт', 'подпунктов', 'подпунктов');

    public function getEditFormElements($model)
    {
        return array(
            'title' => ['type' => 'textField'],
            //'cost' => ['type' => 'textField'],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            $this->getOrderColumn(),
            'title',
            //'cost',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }
}
