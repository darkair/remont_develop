<?php

class m170106_214958_seo_data extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `seo_data` (
              `model_name` varchar(50) NOT NULL,
              `model_id` int(12) NOT NULL,
              `title` text,
              `keywords` text,
              `description` text,
              PRIMARY KEY (`model_name`,`model_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('seo_data');
    }
}