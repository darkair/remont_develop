<?php

class m160727_123523_localconfig_email extends CDbMigration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO localconfig
                (`name`, `value`, `module`, `description`, `example`, `type`)
            VALUES
                ("feedbackEmail", "noreply@site.ru", "contact-info", "Почта для обратной связи", "noreply@ekburg.ru", "string")
        ');
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM localconfig WHERE name IN ('feedbackEmail')
        ");
    }
}
