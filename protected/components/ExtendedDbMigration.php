<?php

class ExtendedDbMigration extends CDbMigration
{
    protected function createLangData($name, $desc, $category)
    {
        $res = array();
        $res[$name] = Yii::t($category, $desc);
        foreach (Yii::app()->params['languages'] as $lang=>$langName) {
            $res[$name.'_'.$lang] = Yii::t($category, $desc, array(), null, $lang);
        }
        return $res;
    }

    protected function createMenu($menuArr, $items)
    {
        $menu = new Menu;
        $menu->multilang();
        $attr = array(
            'name' => $menuArr['name']
        );
        foreach (Yii::app()->params['languages'] as $lang=>$langName) {
            $attr['name_'.$lang] = $menuArr['name_'.$lang];
        }
        $menu->setAttributes($attr);
        $menu->id = $menuArr['id'];
        $menu->save();

        $orderNum = 1;
        $this->createMenuItems($menuArr['id'], $items, $orderNum);
    }

    protected function createMenuItems($menuId, $items, &$orderNum, $parentId = 0)
    {
        foreach ($items as $item) {
            $menuItem = new MenuItem;
            $menuItem->multilang();
            $attr = array(
                'menuId'        => $menuId,
                'name'          => $item['name'],
                'link'          => isset($item['link']) ? $item['link'] : '',
                'orderNum'      => $orderNum++,
                'active'        => isset($item['active']) ? $item['active'] : 1,
                'visible'       => 1,
                'parentItemId'  => $parentId
            );
            foreach (Yii::app()->params['languages'] as $lang=>$langName) {
                $str = isset($item['name_'.$lang]) ? $item['name_'.$lang] : '';
                $attr['name_'.$lang] = $str;
            }
            $menuItem->setAttributes($attr);
            $menuItem->save();

            if (isset($item['children']) && is_array($item['children']))
                $this->createMenuItems($menuId, $item['children'], $orderNum, $menuItem->id);
            else
            if (isset($item['items']) && is_array($item['items']))
                $this->createMenuItems($menuId, $item['items'], $orderNum, $menuItem->id);
        }
    }

    protected function deleteMenu($menuId)
    {
        $menuItems = MenuItem::model()->findAllByAttributes(array('menuId'=>$menuId));
        foreach ($menuItems as $item)
            $item->delete();
        Menu::model()->deleteByPk($menuId);
    }

    protected function deleteMenuItems($menuId, $links)
    {
        $menuItems = MenuItem::model()->byMenuIds($menuId)->byLinks($links)->findAll();
        foreach ($menuItems as $menuItem)
            $menuItem->delete();
    }

    protected function deleteAllModels($className)
    {
        $criteria = new CDbCriteria();
        $criteria->offset = 0;
        $criteria->limit = 20;

        do {
            $models = $className::model()->findAll($criteria);
            if (!count($models))
                break;
            foreach ($models as $model)
                $model->delete();
        } while(true);
    }

    /**
     * Создать связку с таблицей тегов
     */
    protected function createTag2Obj($tableName, $refCountField=null, $suffix='')
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `{$tableName}_tag2obj{$suffix}` (
                `objId`     int(11)     NOT NULL COMMENT 'Id объекта',
                `tagId`     int(11)     NOT NULL COMMENT 'Id тега',
                KEY `objId` (`objId`),
                KEY `tagId` (`tagId`),
                CONSTRAINT `fk_{$tableName}_objId{$suffix}` FOREIGN KEY (`objId`) REFERENCES `{$tableName}` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk_{$tableName}_tagId{$suffix}` FOREIGN KEY (`tagId`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        if ($refCountField !== null) {
            $this->execute("
                CREATE TRIGGER `insert_{$tableName}_tag2obj{$suffix}` AFTER INSERT ON `{$tableName}_tag2obj{$suffix}`
                FOR EACH ROW BEGIN
                    UPDATE tags SET {$refCountField}={$refCountField}+1 WHERE id = NEW.tagId;
                END;
            ");
            $this->execute("
                CREATE TRIGGER `delete_{$tableName}_tag2obj{$suffix}` BEFORE DELETE ON `{$tableName}_tag2obj{$suffix}`
                FOR EACH ROW BEGIN
                    UPDATE tags SET {$refCountField}={$refCountField}-1 WHERE tags.id = OLD.tagId;
                END;
            ");
        }
    }

    /**
     * Удалить связку с таблицей тегов
     */
    protected function dropTag2Obj($tableName, $suffix='')
    {
        //$this->execute("DROP TRIGGER `insert_{$tableName}_tag2obj`");
        //$this->execute("DROP TRIGGER `delete_{$tableName}_tag2obj`");
        $this->dropTable("{$tableName}_tag2obj{$suffix}");
    }

    /**
     * Создать контентный блок
     * $title: array[0=>..., 'en'=>...] or string
     * $text: array[0=>..., 'en'=>...] or string
     * $position: constant from ContentBlocks
     * $visible: boolean
     */
    protected function createContentBlock($title, $text, $position, $visible=true)
    {
        if (!is_array($title))
            $title = [0=>$title];
        if (!is_array($text))
            $text = [0=>$text];

        if (!isset($title[0]))
            throw new CException('Не задан заголовок');
        if (!isset($text[0]))
            throw new CException('Не задан текст');

        $attr = [];
        foreach ($title as $k=>$v) {
            if ($k===0)
                $attr['title'] = $v;
            else
                $attr['title_'.$k] = $v;
        }
        foreach ($text as $k=>$v) {
            if ($k===0)
                $attr['text'] = $v;
            else
                $attr['text_'.$k] = $v;
        }
        $attr['position'] = $position;
        $attr['visible'] = $visible;

        $cb = new ContentBlocks();
        $cb->setAttributes($attr);
        if (!$cb->save())
            throw new CException('Не получилось создать контентный блок');
    }

    /**
     * Удалить контентный блок
     */
    protected function deleteContentBlock($position)
    {
        $cb = ContentBlocks::model()->byPosition($position)->findAll();
        foreach ($cb as $block) {
            if (!$block->delete())
                throw new Exception('Не получилось удалить контентный блок');
        }
    }
}