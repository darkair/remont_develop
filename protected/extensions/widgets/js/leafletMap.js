var LeafletMap = function()
{
    this.o = {
        containerSelector: '',
        mapUniqId: '',
        modalId: '',
        centerLat: 0,
        centerLng: 0,
        zoom: 0,
        useCollapse: false,
        useMarkerClusterGroup: true,
        useSquareMarkers: false,
        markers: [],
        line: []
    }
    this.imgClass = 'leaflet-image';        // Class for processing click on the image
    this.map = 0;

    this.init = function(params)
    {
        $.extend(this.o, params);

        this.initMap();
        if (this.o.useCollapse)
            this.initExpandBtn();
    }

    this.initMap = function()
    {
        var self = this;

        var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            maxZoom: 18
        });
        var center = L.latLng(self.o.centerLat, self.o.centerLng);
        self.map = L.map(self.o.mapUniqId, {
            center: center,     // Нужно, когда нет линий и маркеров
            zoom: self.o.zoom,
            scrollWheelZoom: false,
            layers: [tiles],
            zoomAnimation: true
        });

        if (self.o.line.coords.length > 0)
            self.initLineMap();
        if (self.o.markers.length > 0)
            self.initMarkersMap();
    }

    this.initLineMap = function()
    {
        var self = this;

        var colors = ["#1db850", "#28caff", "#eb3c7e", "#185DBE"];
        var line = new L.Polyline(self.o.line.coords, {
            color: colors[0],       // Здесь можно считать число линий и устанавливать разный цвет
            opacity: 1,
            weight: 5
        });
        line.addTo(self.map);
        self.map.fitBounds(line.getBounds());
        line.bindPopup('<a href="'+self.o.line.link+'">' + self.o.line.title + '</a>');
    }

    this.initMarkersMap = function()
    {
        var self = this;

        var markers = (self.o.useMarkerClusterGroup) ? L.markerClusterGroup() : L.layerGroup();
        var mArr = self.o.markers;

        var minLat = null;
        var maxLat = null;
        var minLng = null;
        var maxLng = null;

        for (var i = mArr.length - 1; i >= 0; i--) {
            if (minLat==null || mArr[i].lat<minLat)     minLat = mArr[i].lat;
            if (maxLat==null || mArr[i].lat>maxLat)     maxLat = mArr[i].lat;
            if (minLng==null || mArr[i].lng<minLng)     minLng = mArr[i].lng;
            if (maxLng==null || mArr[i].lng>maxLng)     maxLng = mArr[i].lng;

            var m = (self.o.useSquareMarkers)
                ? L.marker(L.latLng( mArr[i].lat, mArr[i].lng ), {icon: L.divIcon({html: ' '})})
                : L.marker(L.latLng( mArr[i].lat, mArr[i].lng ));
            // Bind popup if title or image exist
            if (mArr[i].title || mArr[i].img) {
                var str = '';

                if (mArr[i].title) {
                    str += mArr[i].link
                        ? '<a href="'+mArr[i].link+'">' + mArr[i].title + '</a>'
                        : mArr[i].title;
                    str += '<br/>';
                }
                if (mArr[i].img) {
                    if (mArr[i].img)
                        str += '<img src="'+mArr[i].img+'" class="'+self.imgClass+'"">';
                }
                m.bindPopup(str);
            }
            markers.addLayer(m);
        };
        self.map.addLayer(markers);
        self.map.fitBounds([[minLat, minLng], [maxLat, maxLng]]);

        // Init image click
        $(self.o.containerSelector).delegate('.'+self.imgClass, 'click', function() {
            var $modal = $(self.o.containerSelector+' #'+self.o.modalId);
            $modal.find('.modal-body').html("<img width='100%' src='"+$(this).attr('src')+"'>");
            console.log($modal, $modal.find('.modal-body'), $(this).attr('src'));
            $modal.modal();
        });
    }

    this.initExpandBtn = function()
    {
        var self = this;

        var $expandBtn = $(self.o.containerSelector+' #js-expand-btn');
        var $collapseBtn = $(self.o.containerSelector+' #js-collapse-btn');
        var $map = $('#'+self.o.mapUniqId);
        $expandBtn.click(function() {
            $expandBtn.addClass('hide');
            $collapseBtn.removeClass('hide');
            $map.addClass('opened');
            setTimeout(function() {
                return self.map.invalidateSize(true);
            }, 200);
            return false;
        });
        $collapseBtn.click(function() {
            $expandBtn.removeClass('hide');
            $collapseBtn.addClass('hide');
            $map.removeClass('opened');
            setTimeout(function() {
                return self.map.invalidateSize(true);
            }, 200);
            return false;
        });
    }
}