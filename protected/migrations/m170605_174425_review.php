<?php

class m170605_174425_review extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `review` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `image`         varchar(100) NOT NULL               COMMENT 'Картинка',
                `text`          text                                COMMENT 'Отзыв',
                `info`          varchar(255)                        COMMENT 'Информация об авторе',
                `visible`       tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('review');
    }
}