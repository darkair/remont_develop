<?php

class m161231_091442_faq extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `faq` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `question`      TEXT NOT NULL    COMMENT 'Вопрос',
                `answer`        TEXT NOT NULL    COMMENT 'Ответ',
                `visible`       tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('faq');
    }
}