import Vue from 'vue'
import VueResource from 'vue-resource'      // Access to $http object in components
//import formatNumber from './../../node_modules/accounting-js/lib/formatNumber.js'        // Number formatter

// Фильтры надо определять ОБЯЗАТЕЛЬНО до Vue
Vue.use(VueResource);       // Добавляем поддержку $http

import RoomConstructor from './roomConstructor2.vue';

$(function() {
    if ($('#room-constructor').length) {
        new Vue({
            el: '#room-constructor',
            data: {
            },
            http: {
                emulateHTTP: true,
                emulateJSON: true
            },
            render: h => h(RoomConstructor)
        });
    }
});