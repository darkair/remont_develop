<?php

/**
 * Модуль списка документов
 */

class DocumentsListModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.documentsList.models.*');
    }
}
