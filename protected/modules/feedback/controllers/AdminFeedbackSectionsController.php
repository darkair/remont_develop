<?php

class AdminFeedbackSectionsController extends MAdminController
{
    public $modelName = 'FeedbackSection';
    public $modelHumanTitle = array('рубрику', 'рубрик', 'рубрик');
    public $allowedRoles = 'admin, moderator';

    public function behaviors()
    {
        return array(
        );
    }

    public function getEditFormElements($model)
    {
        return array_merge(
            array(
                'visible' => ['type' => 'checkBox']
            ),
            $this->getLangField('title', ['type' => 'textField']),
            array(
                'email' => ['type' => 'textField'],
            )
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            $this->getOrderColumn(),
            'title',
            'email',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }
}
