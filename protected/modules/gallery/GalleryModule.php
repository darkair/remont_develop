<?php

/**
 * Модуль галереи
 * Отдельные сущности галерей, которые можно вставлять в любое место страницы
 */

class GalleryModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.gallery.models.*');
    }
}
