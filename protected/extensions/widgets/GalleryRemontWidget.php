<?php

Yii::import('modules.gallery.models.*');

class GalleryRemontWidget extends ExtendedWidget
{
    public $items = [];

    public function init()
    {
        TwigFunctions::importResource('js', 'jquery.colorbox.js');
        TwigFunctions::importResource('css', 'colorbox.css');

        $this->items = [];
        $gallery = Gallery::model()->byType(Gallery::TYPE_MAIN_PAGE)->find();
        if ($gallery)
            $this->items = $gallery->getGalleryItems();

        parent::init();
    }

    public function run()
    {
        $this->render('galleryRemont', array(
            'items' => $this->items
        ));            
    }
}
