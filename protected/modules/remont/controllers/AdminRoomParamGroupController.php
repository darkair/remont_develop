<?php

class AdminRoomParamGroupController extends MAdminController
{
    public $modelName = 'RoomParamGroup';
    public $modelHumanTitle = array('группу параметров', 'группы параметров', 'групп параметров');
    public $allowedActions = 'add,edit,delete,update,editRaw';

    public function behaviors()
    {
        return array(
            'childObjListBehavior' => array(
                'class' => 'application.behaviors.ChildObjListControllerBehavior',
                'childListField' => 'roomParams',
                'innerField' => '_innerRoomParams',
                'childClass' => 'RoomParam',
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array(
            'title' =>          ['type' => 'textField'],
            'titleInner' =>     ['type' => 'textField', 'rowOptions' => ['hint'=>'<div class="inline" style="float:left; clear:both;">Необходимо для удобного отображения в админке.<br/>Например: "Кухня: плинтусы"</div>']],
            'desc' =>           ['type' => 'textArea'],
            'roomParamsList' => [
                'type' => 'childObjList',
                'htmlOptions' => [
                    'innerChildField' => '_innerRoomParams',
                    'valueField' => 'id',
                    'textField' => 'title',
                    'childClass' => 'RoomParam',
                    'editUrl' => '/remont/adminRoomParam',
                ]
            ],
            'visible' =>        ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'titleInner',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->childObjListBehavior->childObjListBeforeSave($model);

        $model->initRoomParams($model->_innerRoomParams);
        parent::beforeSave($model);
    }
}
