<?php

class m161112_063549_feedback_sections extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `feedbacksections` (
                `id`        int(11) NOT NULL AUTO_INCREMENT,
                `title`     varchar(128) NOT NULL DEFAULT '' COMMENT 'Заголовок',
                `email`     varchar(128) NOT NULL DEFAULT '' COMMENT 'Почта',

                `visible`   tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
                `orderNum`  int(10) NOT NULL DEFAULT '0' COMMENT 'Порядок сортировки',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`),
                KEY `orderNum` (`orderNum`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `feedbacksections_lang` (
                `l_id` int(11) NOT NULL AUTO_INCREMENT,
                `fbsId` int(11) NOT NULL,
                `lang_id` varchar(6) NOT NULL,
                `l_title` varchar(128) NOT NULL,
                PRIMARY KEY (`l_id`),
                KEY `fbsId` (`fbsId`),
                KEY `lang_id` (`lang_id`),
                CONSTRAINT `fk_feedbacksections_lang` FOREIGN KEY (`fbsId`) REFERENCES `feedbacksections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('feedbacksections_lang');
        $this->dropTable('feedbacksections');
    }
}
