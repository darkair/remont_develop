<?php
class UrlManager extends CUrlManager
{
    public $useArticlesUrl = false;

    // Список оригинальных урлов из конфига. Нужно для отслеживания неизменяемых пунктов меню
    private $originalRules = [];

    public function init()
    {
        Yii::import('modules.articles.models.Article');

        // Запоминаем исходные правила, заданные в конфиге
        $this->originalRules = $this->rules;

        // Добавляем правила для статей
        if ($this->useArticlesUrl) {
            $rules = $this->createArticleRules();
            $this->rules = array_merge($rules, $this->rules);
        }
        parent::init();
    }

    public function createUrl($route, $params=array(), $ampersand='&')
    {
        // Формируем стандартный URL без языка
        $url = parent::createUrl($route, $params, $ampersand);
        $domains = explode('/', ltrim($url, '/'));

        if (in_array($domains[0], array_keys(Yii::app()->params['languages']))) {
            if ($domains[0] == Yii::app()->sourceLanguage) {
                // Вырезем из урла дефолтный язык
                array_shift($domains);
                $url = '/' . implode('/', $domains);
            } else {
                // В урле задан язык, отличный от дефолтного, ничего делать не надо
            }
        } else {
            if (Yii::app()->language != Yii::app()->sourceLanguage) {
                // Язык не задан в урле, добавляем его
                array_unshift($domains, Yii::app()->language);
                $url = '/' . implode('/', $domains);
            } else {
                // Язык не задан, но он дефолтный, ничего подставлять не надо
            }
        }

        return $url;
    }

    /**
     * Является ли УРЛ неизменяемым (заданным через конфиг)
     */
    public function isFixedUrl($url)
    {
        // Пустые урлы не являются неизменяемыми
        if (empty($url))
            return false;
        
        $url = trim($url, '/');
        foreach ($this->originalRules as $k=>$v) {
            if (trim($k, '/') == $url)
                return true;
        }
        return false;
    }

    /**
     * Является ли УРЛ прописанным в статье
     */
    public function isArticleUrl($url)
    {
        $article = new Article();

        $command = Yii::app()->db->createCommand();
        $criteria = $article->onlyLinks()->getDbCriteria();
        $count = $article->count();
        $offs = 0;

        do {
            $articles = $command->reset()
                ->select($criteria->select)
                ->from($article->tableName())
                ->limit(20, $offs)
                ->queryAll();
            foreach ($articles as $article) {
                // Пропускаем пустые route
                if (empty($article['link']))
                    continue;
                
                // Прописываем в роуте link, чтобы можно было отличать разные статьи по одному роуту и переключатель языков работал
                $link = trim($article['link'], '/');        // Используем link, т.к. даже если имя поменяется, здесь все упадет
                $link = str_replace('/', '\\/', $link);
                $rules[$langPrefix.'/<link:('.$link.')>/'] = 'articles/articles/show';
                $rules['<link:('.$link.')>/'] = 'articles/articles/show';
            }
            $offs += 20;
        } while($offs < $count);
    }

    private function createArticleRules()
    {
        // нельзя Article::model(), т.к. это убивает переключение языков
        $article = new Article();

        $command = Yii::app()->db->createCommand();
        $criteria = $article->onlyLinks()->getDbCriteria();
        $count = $article->count();
        $offs = 0;

        $langArr = array_keys(Yii::app()->params['languages']);
        $langPrefix = '<language:('. implode('|', $langArr) .')>';
        
        $rules = array();
        do {
            $articles = $command->reset()
                ->select($criteria->select)
                ->from($article->tableName())
                ->limit(20, $offs)
                ->queryAll();
            foreach ($articles as $article) {
                // Пропускаем пустые route
                if (empty($article['link']))
                    continue;
                
                // Прописываем в роуте link, чтобы можно было отличать разные статьи по одному роуту и переключатель языков работал
                $link = trim($article['link'], '/');        // Используем link, т.к. даже если имя поменяется, здесь все упадет
                $link = str_replace('/', '\\/', $link);
                $rules[$langPrefix.'/<link:('.$link.')>/'] = 'articles/articles/show';
                $rules['<link:('.$link.')>/'] = 'articles/articles/show';
            }
            $offs += 20;
        } while($offs < $count);
        return $rules;
    }
}
