<?php

Yii::import('modules.faq.models.*');

class FaqWidget extends ExtendedWidget
{
    public function run()
    {
        $faq = Faq::model()->onSite()->findAll();

        $this->render('faq', array(
            'faq' => $faq,
        ));
    }
}
