<?php

class GalleryControllerBehavior extends CBehavior
{
    public $galleryItemClass = null;
    public $imagesField = 'images';
    public $innerImagesField = '_images';
    public $innerRemoveField = '_removeGalleryImageFlags';
    public $imageWidth = null;
    public $imageHeight = null;
    public $resizeType = 'contain';     // contain / cover

    public function galleryBeforeSave($model, $storagePath)
    {
        // path to original
        $storageOrig = $storagePath.'original/';

        if (!empty($model->{$this->innerRemoveField})) {
            foreach ($model->{$this->innerRemoveField} as $imgName) {
                // removing file
                // set attribute to null
                @unlink( $storagePath.$imgName );
                @unlink( $storageOrig.$imgName );

                // Remove gallery item
                if ($this->galleryItemClass !== null) {
                    $className = $this->galleryItemClass;
                    $galleryItem = $className::model()->findByAttributes([
                        'fileName' => $imgName
                    ]);
                    if ($galleryItem)
                        $galleryItem->delete();
                }

                $imgArr = $model->{$this->imagesField};
                $key = array_search($imgName, $imgArr);
                if ($key !== false)
                    unset($imgArr[$key]);
                $model->{$this->imagesField} = $imgArr;
            }
        }

        $model->{$this->innerImagesField} = CUploadedFile::getInstances($model, $this->innerImagesField);

        if ($model->validate(array($this->innerImagesField)) && !empty($model->{$this->innerImagesField})) {
            // saving file from CUploadFile instance $model->{$this->innerImagesField}
            if (!is_dir($storagePath))
                mkdir($storagePath, 0755, true);
            if (!is_dir($storageOrig))
                mkdir($storageOrig, 0755, true);

            $images = [];
            foreach ($model->{$this->innerImagesField} as $img) {
                $imageName = basename($img->name);
                $ext = strrchr($imageName, '.');
                $imageName = md5(time().$imageName).($ext?$ext:'');

                $img->saveAs( $storageOrig.$imageName );
            
                // Загружаем данные изображения
                $image = Yii::app()->image->load($storageOrig.$imageName);
    
                if (!empty($this->imageWidth) && !empty($this->imageHeight)) {
                    $cW = $image->width / $this->imageWidth;
                    $cH = $image->height / $this->imageHeight;
                    switch ($this->resizeType) {
                        // Изображение всегда будет видно полностью, т.о. оно может быть меньше контента
                        case 'contain':
                            if ($cW > $cH) {
                                // Выравниваем по ширине
                                $resizeW = $this->imageWidth;
                                $resizeH = $image->height * $this->imageWidth / $image->width;
                            } else {
                                // Выравниваем по высоте
                                $resizeH = $this->imageHeight;
                                $resizeW = $image->width * $this->imageHeight / $image->height;
                            }
                            break;

                        // Изображение всегда заполняет весь контент, т.о. оно может быть обрезано
                        case 'cover':
                            if ($cW <= $cH) {
                                $resizeW = $this->imageWidth;
                                $resizeH = $image->height * $this->imageWidth / $image->width;
                            } else {
                                $resizeH = $this->imageHeight;
                                $resizeW = $image->width * $this->imageHeight / $image->height;
                            }
                            break;
                    }
                    $image->resize($resizeW, $resizeH)
                          ->crop($this->imageWidth, $this->imageHeight);
                } else
                if (!empty($this->imageHeight)) {
                    // Resize by height
                    $image->resize(null, $this->imageHeight);
                } else
                if (!empty($this->imageWidth)) {
                    // Resize by width
                    $image->resize($this->imageWidth, null);
                }

                // rewrite under other name
                $image->save($storagePath.$imageName);

                $images[] = $imageName;

                // Create gallery item
                if ($this->galleryItemClass !== null) {
                    $className = $this->galleryItemClass;
                    $galleryItem = $className::model()->findByAttributes([
                        'fileName' => $imageName
                    ]);
                    if (!$galleryItem) {
                        $galleryItem = new $className;
                        $galleryItem->setAttributes([
                            'title' => '',
                            'fileName' => $imageName
                        ]);
                        $galleryItem->save();
                    }
                }
            }
            if (!is_array($model->{$this->imagesField}))
                $model->{$this->imagesField} = array();
            $model->{$this->imagesField} = array_merge($model->{$this->imagesField}, $images);
        }
    }
}