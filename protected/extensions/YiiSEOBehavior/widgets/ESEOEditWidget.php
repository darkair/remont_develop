<?php

/**
 * Widget for adding SEO options to model editing form
 *
 * @version 1.0
 * @package YiiSeoBehavior
 */

class ESEOEditWidget extends CWidget
{
    public $model;
    public $attribute;
    public $form;
        
    public function run()
    {
        $this->render('ESEOEditWidget', array(
            'model' => $this->model,
        ));
    }
}
