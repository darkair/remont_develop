<?php

class DocumentsListBehavior extends CActiveRecordBehavior
{
    public $documentsItemClass = null;
    public $storagePath = '';
    public $docsField = '';
    public $innerField = '_docs';
    public $innerRemoveField = '_removeDocsFlags';


    public function documentsListLabels()
    {
        $arr = array(
            $this->docsField => Yii::t('app', 'Документы'),
        );
        return $arr;
    }

    public function documentsListRules()
    {
        $rules = array();
        $rules[] = array($this->innerField, 'safe');
        $rules[] = array($this->innerRemoveField, 'safe');
        return $rules;
    }

    public function getStorePath()
    {
        return Yii::getPathOfAlias('webroot.store.'.$this->storagePath).'/';
    }

    public function getDocumentsItemByFileName($filename)
    {
        $class = $this->documentsItemClass;
        if (!$class)
            return null;

        return $class::model()->findByAttributes([
            'filename' => $filename
        ]);
    }

    public function getDocumentOriginalFileName($filename)
    {
        $docItem = $this->getDocumentsItemByFileName($filename);
        if (!$docItem)
            return '';
        return $docItem->orig;
    }

    public function getDocumentFileNameByFilePart($filePart)
    {
        foreach ($this->owner->{$this->docsField} as $filename) {
            if ($filePart === StringUtils::extractFileName($filename))
                return $filename;
        }
        return false;
    }

    public function getDocumentsItems()
    {
        $class = $this->documentsItemClass;
        if (!$class)
            return null;

        $items = [];
        foreach ($this->owner->{$this->docsField} as $filename) {
            $item = $this->getDocumentsItemByFileName($filename);
            if ($item)
                $items[] = $item;
        }
        return $items;
    }

    public function getDocumentAbsoluteUrl($filename)
    {
        return $this->getStorePath().$filename;
    }

    public function afterDelete($event)
    {
        if (!empty($this->owner->{$this->docsField})) {
            foreach ($this->owner->{$this->docsField} as $docInfo) {
                $fileName = $docInfo['name'];
                @unlink( $this->getStorePath().$fileName );
            }
        }
        $this->owner->{$this->docsField} = [];
    }

    public function afterFind($event)
    {
        $this->owner->{$this->docsField} = json_decode($this->owner->{$this->docsField}, true);
        if (!is_array($this->owner->{$this->docsField}))
            $this->owner->{$this->docsField} = array();

        // Сбрасываем ключи, чтобы можно было брать первый файл через индекс [0]
        $this->owner->{$this->docsField} = array_values($this->owner->{$this->docsField});
    }

    public function beforeSave($event)
    {
        $this->owner->{$this->docsField} = json_encode($this->owner->{$this->docsField});
    }
}