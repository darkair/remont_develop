<?php

class m160623_072452_article_on_main extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE articles ADD COLUMN `onMain` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Статья на главной' AFTER images");

        // Set first article
        $this->execute("UPDATE articles SET `onMain` = 1 LIMIT 1");
    }

    public function safeDown()
    {
        $this->dropColumn('articles', 'onMain');
    }
}