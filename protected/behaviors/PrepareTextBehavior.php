<?php

/**
 * Подготовка текста
 * подключить в behaviors:
            'prepareTextBehavior' => array(
                'class' => 'application.behaviors.PrepareTextBehavior',
                'textFields' => ['field1', 'field2']
            ),
 */

class PrepareTextBehavior extends CActiveRecordBehavior
{
    public $textFields = [];

    public function beforeSave($event)
    {
        if (!empty($this->textFields) && !is_array($this->textFields))
            $this->textFields = [$this->textFields];

        foreach ($this->textFields as $field) {
            $this->owner->{$field} = $this->prepareExternalLinks($this->owner->{$field});
            foreach (Yii::app()->params['languages'] as $lang => $langName) {
                $fieldLang = $field.'_'.$lang;
                if (isset($this->owner->{$fieldLang}))
                    $this->owner->{$fieldLang} = $this->prepareExternalLinks($this->owner->{$fieldLang});
            }
        }
    }

    /**
     * Выносим картинки из абзацев
     * Выравниваем картинки
     */
    public function prepareImages($str, $align='center')
    {
        Yii::import('application.extensions.image.*');

        $alias = Yii::getPathOfAlias('webroot');
        
        // Обрабатываем картинки
        $str = preg_replace('/<p.*?>(<img.*?>)<\/p>/i', '$1', $str);                    // Вырезаем обрамляющий <p>
        $str = preg_replace('/<p.*?>(<img.*?>)(.*?)<\/p>/i', '$1<p>$2</p>', $str);      // Если после картинки был текст, обрамляем его в <p>
        
        // Выравниваем
        switch ($align) {
            // по центру
            case 'center':
                $str = preg_replace('/(<img.*?>)/i', '<div class="text-center"><div class="inner">$1</div></div>', $str);
                break;
        }
        return $str;
    }

    /**
     * Обрабатываем Iframe
     */
    public function prepareIframes($str)
    {
        // Добавляем класс js-iframe
        $str = preg_replace_callback('/<iframe.*?>/i', function ($matches) {
            return (preg_match('/class=[\'"].*?[\'"]/i', $matches[0]) >= 1)
                ? preg_replace('/(class=[\'"].*?)([\'"])/i', '$1 js-iframe $2', $matches[0])
                : preg_replace('/(<iframe)(.*?>)/i', '$1 class="js-iframe" $2', $matches[0]);
        }, $str);

        return $str;
    }

    /**
     * Обрабатываем внешние ссылки
     */
    public function prepareExternalLinks($str)
    {
        // Добавляем во все внешние ссылки без target свой target='_blank'
        $str = preg_replace('/(\<a(?![^>]*\btarget)[^>]*?\bhref\s*=\s*[\'"](http(?:|s):\/\/[^\'"]*?)[\'"][^>]*?)(\>)/i', '$1 target="_blank"$3', $str);
        return $str;
    }
}
