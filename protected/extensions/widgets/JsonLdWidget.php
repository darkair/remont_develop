<?php

class JsonLdWidget extends ExtendedWidget
{
    public function run()
    {
        $url = Yii::app()->createAbsoluteUrl(Yii::app()->getHomeUrl());

        $doc = (object)array_merge(
            array(
                "@context" => "http://schema.org",
                "url" => $url,
                "name" => Yii::app()->name,
            ),
            Yii::app()->params['jsonld']
        );
        $res = json_encode($doc, JSON_UNESCAPED_SLASHES);
        echo '<script type="application/ld+json">';
        echo $res;
        echo '</script>';
    }
}
