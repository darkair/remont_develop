<h2>Данные заказа</h2>

Имя заказчика: <?= $name ?><br/>
Телефон: <?= $phone ?><br/>
E-mail заказчика: <?= $email ?><br/>
Количество комнат: <?= $roomsNumber ?><br/>
Площадь: <?= $square ?> кв.м.<br/>
<hr/>
<h2>Что необходимо сделать</h2>

<?= $roomParam ?>
<hr/>
<h2>Что мы делаем</h2>

Выбор стилей: <?= $resultStyles ?><br/>
Готовый проект: <?= $resultProject ?><br/>
Комплектация: <?= $resultComplectation ?><br/>
Надзор: <?= $resultNadzor ?><br/>
Полный ремонт под ключ: <?= $resultFullremont ?><br/>
<?php
    if (!empty($subparagraphs)):
?>
В том числе:<br/>
<?= $subparagraphs ?><br/>
<?php
    endif;
?>

Итоговая стоимость: <?= $totalPrice ?><br/>
