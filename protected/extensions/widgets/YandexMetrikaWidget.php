<?php

class YandexMetrikaWidget extends ExtendedWidget
{
    public function run()
    {
        PrefetchHelper::dnsPrefetch('//mc.yandex.ru/');

        $this->render('yandexMetrika');
    }
}
