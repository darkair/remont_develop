<?php

/**
 * Review model
 */
class Review extends CActiveRecord
{
    const IMAGE_W = 127;
    const IMAGE_H = 115;

    public $_image = null;
    public $_removeImageFlag = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'review';
    }

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageBehavior',
                'storagePath' => 'reviews',
                'imageLabel' => 'Изображение '.self::IMAGE_W.'x'.self::IMAGE_H,
                'imageField' => 'image',
                'imageExt' => 'png, jpeg, jpg',
                'required' => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->imageBehavior->imageLabels(),
            array(
                'text' => 'Текст отзыва',
                'info' => 'Информация об авторе',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->imageBehavior->imageRules('', 'migrate'),
            array(
                array('image', 'safe', 'on'=>'migrate'),
                array('text', 'required'),
                array('info', 'safe'),
                array('visible', 'boolean'),

                array('visible', 'safe', 'on'=>'search'),
            )
        );
    }

    /**
     * Отмечаем значком "required"
     */
    public function isAttributeRequired($attribute)
    {
        if (in_array($attribute, array('_image')))
            return true;
        return parent::isAttributeRequired($attribute);
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
            'orderDefault' => array(
                'order' => $alias.'.id ASC',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getImageStorePath()
    {
        return $this->imageBehavior->getStorePath();
    }

    public function getImageUrl()
    {
        return $this->imageBehavior->getImageUrl();
    }
}
