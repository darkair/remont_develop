<?php

Yii::import('modules.admin.controllers.*');

final class AdminUploadSmetaFileController extends AdminUploadFileController
{
    protected function getStorePath()
    {
        return Yii::getPathOfAlias('webroot.store.pdf.smeta').'/';
    }

    protected function getRoutePath()
    {
        return 'adminUploadSmetaFile';
    }
}
