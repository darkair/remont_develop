<?php

class HeaderWidget extends ExtendedWidget
{
    public function run()
    {
        PrefetchHelper::linkPrefetch( Yii::app()->createAbsoluteUrl(Yii::app()->baseUrl) );

        $projectFilePdf = $this->getPdfFile('webroot.store.pdf.project');
        $smetaFilePdf = $this->getPdfFile('webroot.store.pdf.smeta');

        $city = SypexGeoHelper::getCityName();
        $this->render('header', [
            'cityName' => $city,
            'projectFilePdf' => $projectFilePdf,
            'smetaFilePdf' => $smetaFilePdf,
        ]);
    }

    public function getPdfFile($alias)
    {
        $storePath = Yii::getPathOfAlias($alias).'/';
        $filePdf = false;
        $objs = glob($storePath."*.pdf");
        if ($objs) {
            foreach($objs as $obj) {
                if (is_file($obj)) {
                    $filePdf = basename($obj);
                    break;
                }
            }
        }
        return $filePdf;
    }
}
