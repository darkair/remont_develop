<?php

class m161122_123711_tags extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `tags` (
                `id`            int(11)         NOT NULL AUTO_INCREMENT,
                `name`          varchar(64)     NOT NULL COMMENT 'Тег',
                `refCount`      int(11)         NOT NULL DEFAULT 0 COMMENT 'Количество внешних ссылок',
                PRIMARY KEY (`id`),
                KEY `name` (`name`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('tags');
    }
}