<?php

class RoomsController extends Controller
{
    public function actionGetRoomData()
    {
        if (!Yii::app()->request->isPostRequest || !Yii::app()->request->isAjaxRequest)
            throw new CHttpException(404);

        $roomId = Yii::app()->request->getPost('roomId');
        if (!$roomId)
            throw new CHttpException(404);

        $roomType = RoomType::model()->onSite()->findByPk($roomId);
        if (!$roomType)
            throw new CHttpException(404);

        $this->render('/roomData', [
            'roomType' => $roomType
        ]);
    }

    public function actionSendForm()
    {
        $hash           = Yii::app()->request->getPost('hash');
        $storagePath    = Yii::getPathOfAlias('application.runtime.saved_pictures.'.$hash).'/';

        $roomType       = Yii::app()->request->getPost('roomType');
        $square         = Yii::app()->request->getPost('square', 0);
        
        $resultStyles           = Yii::app()->request->getPost('result_styles', 0);
        $resultProject          = Yii::app()->request->getPost('result_project', 0);
        $resultComplectation    = Yii::app()->request->getPost('result_complectation', 0);
        $resultNadzor           = Yii::app()->request->getPost('result_nadzor', 0);
        $resultFullremont       = Yii::app()->request->getPost('result_fullremont', 0);
        $totalPrice             = Yii::app()->request->getPost('total_price', 0);

        $name           = Yii::app()->request->getPost('name', '');
        $phone          = Yii::app()->request->getPost('phone', '');
        $email          = Yii::app()->request->getPost('email', '');

        $roomParam      = Yii::app()->request->getPost('roomParam', []);
        $subitems       = Yii::app()->request->getPost('resultSubitem', []);

        $roomType = RoomType::model()->findByPk($roomType);
        if (!$roomType)
            throw new CHttpException(404, 'Неправильный Id комнаты');

        if (!is_numeric($square) && $square <= 0)
            throw new CHttpException(404, 'Неправильная площадь');

        if (!is_numeric($resultStyles) || $resultStyles < 0)
            throw new CHttpException(404, 'Неправильная стоимость выбора стилей');
        if (!is_numeric($resultProject) || $resultProject < 0)
            throw new CHttpException(404, 'Неправильная стоимость проекта');
        if (!is_numeric($resultComplectation) || $resultComplectation < 0)
            throw new CHttpException(404, 'Неправильная стоимость комплектации');
        if (!is_numeric($resultNadzor) || $resultNadzor < 0)
            throw new CHttpException(404, 'Неправильная стоимость надзора');
        if (!is_numeric($resultFullremont) || $resultFullremont < 0)
            throw new CHttpException(404, 'Неправильная стоимость полного ремонта');

        if (empty($name))
            throw new CHttpException(404, 'Не указано имя');
        if (empty($phone))
            throw new CHttpException(404, 'Не указан телефон');

        if (!empty($email)) {
            $tmpEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
            if (filter_var($tmpEmail, FILTER_VALIDATE_EMAIL) === false || $tmpEmail != $email) {
                // remove folder with images
                CFileHelper::removeDirectory($storagePath);
                throw new CHttpException(404, 'Неправильный email');
            }
        }

        if (empty($roomParam))
            throw new CHttpException(404, 'Не указаны параметры');

        $roomParam = $this->createRoomParamsText($roomParam);

        // Заполняем дополнительные чекбоксы
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', array_keys($subitems));
        $subparagraphs = Subparagraph::model()->onSite()->orderDefault()->findAll($criteria);
        $subparagraphs = $this->createSubparagraphsText($subparagraphs);

        // Отправка письма админу сайта
        if (!YII_DEBUG) {
            $mail = MailHelper::createYiiMailer(
                'order',
                Yii::t('app', 'Заказ с сайта'),
                Yii::app()->localConfig->getParam('contact-info.email'),
                [
                    'name'                  => $name,
                    'phone'                 => $phone,
                    'email'                 => empty($email) ? Yii::t('app', 'Не указан') : $email,
                    'roomsNumber'           => $roomType->roomsNumber,
                    'square'                => $square,
                    'resultStyles'          => empty($resultStyles)         ? Yii::t('app','Нет') : $resultStyles,
                    'resultProject'         => empty($resultProject)        ? Yii::t('app','Нет') : $resultProject,
                    'resultNadzor'          => empty($resultNadzor)         ? Yii::t('app','Нет') : $resultNadzor,
                    'resultComplectation'   => empty($resultComplectation)  ? Yii::t('app','Нет') : $resultComplectation,
                    'resultFullremont'      => empty($resultFullremont)     ? Yii::t('app','Нет') : $resultFullremont,
                    'totalPrice'            => $totalPrice,
                    'roomParam'             => $roomParam,
                    'subparagraphs'         => $subparagraphs,
                    'adminPhone'            => Yii::app()->localConfig->getParam('contact-info.phone'),
                ]
            );
            $files = CFileHelper::findFiles($storagePath, ['absolutePaths'=>true]);
            foreach ($files as $file) {
                $mail->setAttachment($file);
            }
            if (!$mail->send()) {
                // Отправляем общую ошибку в поле name
                header('HTTP/1.1 400 '.$mail->getError());
                echo CJSON::encode(['success'=>'false']);
                Yii::log($mail->getError(), CLogger::LEVEL_ERROR, 'mail');

                // remove folder with images
                CFileHelper::removeDirectory($storagePath);
                Yii::app()->end();
            }
        }

        if (!empty($email)) {
            // Отправка письма пользователю
            $mail = MailHelper::createYiiMailer(
                'orderForUser',
                Yii::t('app', 'Заказ с сайта'),
                $email,
                [
                    'name'                  => $name,
                    'roomsNumber'           => $roomType->roomsNumber,
                    'square'                => $square,
                    'resultStyles'          => empty($resultStyles)         ? Yii::t('app','Бесплатно') : $resultStyles.' <span class="fa fa-rub"></span>',
                    'resultProject'         => empty($resultProject)        ? Yii::t('app','Нет') : $resultProject.' <span class="fa fa-rub"></span>',
                    'resultNadzor'          => empty($resultNadzor)         ? Yii::t('app','Нет') : $resultNadzor.' <span class="fa fa-rub"></span>',
                    'resultComplectation'   => empty($resultComplectation)  ? Yii::t('app','Нет') : $resultComplectation.' <span class="fa fa-rub"></span>',
                    'resultFullremont'      => empty($resultFullremont)     ? Yii::t('app','Нет') : $resultFullremont.' <span class="fa fa-rub"></span>',
                    'totalPrice'            => $totalPrice,
                    'roomParam'             => $roomParam,
                    'subparagraphs'         => $subparagraphs,
                ]
            );
            foreach ($files as $file) {
                $mail->setAttachment($file);
            }
            if (!$mail->send()) {
                // Отправляем общую ошибку в поле name
                header('HTTP/1.1 400 '.$mail->getError());
                echo CJSON::encode(['success'=>'false']);
                Yii::log($mail->getError(), CLogger::LEVEL_ERROR, 'mail');

                // remove folder with images
                CFileHelper::removeDirectory($storagePath);
                Yii::app()->end();
            }
        }

        echo CJSON::encode(['success'=>'true']);

        // remove folder with images
        CFileHelper::removeDirectory($storagePath);
        Yii::app()->end();
    }

    public function actionSavePicture()
    {
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest)
            throw new CHttpException(404);

        $data = Yii::app()->request->getPost('data');
        $hash = Yii::app()->request->getPost('hash');
        if (!$data || !$hash)
            throw new CHttpException(400);

        // Generate random string
        $fileName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);
        $fileName = $fileName.".png";

        // Get the base-64 string from data
        $filteredData = substr($data, strpos($data, ",") + 1);

        // Decode the string
        $unencodedData = base64_decode($filteredData);

        // Save the image
        $storagePath = Yii::getPathOfAlias('application.runtime.saved_pictures.'.$hash).'/';
        if (!is_dir($storagePath))
            mkdir($storagePath, 0755, true);

        file_put_contents($storagePath.$fileName, $unencodedData);

        Yii::app()->end();
    }

    public function actionRecalc()
    {
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest)
            throw new CHttpException(404);

        $data = Yii::app()->request->getPost('data');
        if (!$data)
            throw new CHttpException(400);

        // Для тестов возвращаем JSON без запроса
/*        if (YII_DEBUG) {
            $return = CJSON::encode([
                "material_price" => 503860,             // стоимость материалов
                "black_material_price" => 144393,       // стоимость черновых материалов
                "work_price" => 609010,                 // стоимость работ
                "price" => 1257263                      // общая стоимость (сумма)
            ]);
            echo CJSON::encode([
                'res' => print_r($return, true),
                'error' => ''
            ]);
            Yii::app()->end();
        }
*/
        $login = Yii::app()->localConfig->getParam('1c.login');
        $pass = Yii::app()->localConfig->getParam('1c.password');
        $data = CJSON::encode($data);
        $url = "http://".Yii::app()->localConfig->getParam('1c.ip')."/roomsme/hs/orders/order";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $login . ":" . $pass);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        $info = curl_getinfo($ch);

        echo CJSON::encode([
            'res' => print_r($return, true),
            'errCode' => $info['http_code'],
            'error' => print_r(curl_error($ch), true)
        ]);
        curl_close($ch);

        Yii::app()->end();
    }

    private function createRoomParamsText(&$roomParams)
    {
        // NOTE: Логика формирования индексов массива в шаблоне roomData.twig
        $text = '';
        foreach ($roomParams as $blockId=>$groups) {
            $tmp = explode('_', $blockId);
            if ($tmp === false || count($tmp) < 2)
                throw new CHttpException(404, 'Неправильный Id комнатного блока '.$blockId);
            $blockId = $tmp[1];

            $roomBlock = RoomBlock::model()->onSite()->findByPk($blockId);
            if (!$roomBlock)
                throw new CHttpException(404, 'Не найден комнатный блок '.$blockId);
            $text .= '<h2>'.$roomBlock->title."</h2><br/>\r\n";

            foreach ($groups as $groupId=>$paramId) {
                $tmp = explode('_', $groupId);
                if ($tmp === false || count($tmp) < 2)
                    throw new CHttpException(404, 'Неправильный Id группы параметров '.$groupId);
                $groupId = $tmp[1];

                if (isset($roomBlock->roomGroupsList[$groupId])) {
                    $group = &$roomBlock->roomGroupsList[$groupId];
                    // Hide invisible groups
                    if ($group->visible) {
                        $text .= '    '.$group->title.': ';
                        if (isset($group->roomParamsList[$paramId])) {
                            $param = &$group->roomParamsList[$paramId];
                            $text .= $param->title;
                        }
                        $text .= "<br/>\r\n";
                    }
                }
            }
        }
        return $text;
    }

    private function createSubparagraphsText(&$subparagraphs)
    {
        $text = '';
        foreach ($subparagraphs as $subitem) {
            $text .= '    '.$subitem->title;
            //$text .= ' ('.$subitem->cost.' <span class="fa fa-rub"></span>)';
            $text .= "<br/>\r\n";
        }
        return $text;
    }
}