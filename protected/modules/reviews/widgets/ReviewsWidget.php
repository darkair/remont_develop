<?php

Yii::import('modules.reviews.models.*');

class ReviewsWidget extends ExtendedWidget
{
    public function run()
    {
        $review = Review::model()->onSite()->orderDefault()->findAll();

        $this->render('reviews', array(
            'review' => $review,
        ));
    }
}
