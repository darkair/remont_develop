<?php

Yii::import('modules.gallery.models.Gallery');

class m171015_130641_main_page_gallery extends CDbMigration
{
    public function safeUp()
    {
        $gallery = new Gallery();
        $gallery->type = Gallery::TYPE_MAIN_PAGE;
        $gallery->title = 'Галерея на главной странице';
        $gallery->visible = true;
        if (!$gallery->save())
            throw new Exception('Не получилось создать галерею');
    }

    public function safeDown()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('type = '.Gallery::TYPE_MAIN_PAGE);
        $gallery = Gallery::model()->find($criteria);
        if ($gallery)
            $gallery->delete();
    }
}