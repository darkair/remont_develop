<?php

class Theme extends CTheme
{
    private $_subtheme = '';

    public function __construct($name, $basePath, $baseUrl, $subtheme)
    {

        $this->_subtheme = $subtheme;
        parent::__construct($name, $basePath, $baseUrl);
    }

    public function getSubTheme()
    {
        return $this->_subtheme;
    }
}
