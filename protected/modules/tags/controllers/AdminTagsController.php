<?php

class AdminTagsController extends MAdminController
{
    public $modelName = 'Tags';
    public $modelHumanTitle = array('тег', 'тегов', 'тегов');
    public $allowedRoles = 'admin, moderator';

    public function getEditFormElements($model)
    {
        return array(
            'name' => array(
                'type' => 'textField',
            ),
        );
    }

    public function getTableColumns()
    {
        $buttons = $this->getButtonsColumn();
        $buttons['deleteButtonOptions']['visible'] = '$data->refCount == 0';
        $attributes = array(
            'id',
            'name',
            'refCount',
            $buttons,
        );

        return $attributes;
    }
}
