<?php

/**
 * Отдельный документ
 */
class DocumentsItem extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'documentsitem';
    }

    public function behaviors()
    {
        return array(
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'DocumentsItemLang',
                'langTableName'         => 'documentsitem_lang',
                'langForeignKey'        => 'documentsItemId',
                'localizedAttributes'   => array('title'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Заголовок',
            )),
            [
                'filename'  => 'Имя файла',
                'orig'      => 'Оригинальное имя файла',
            ]
        );
    }

    public function rules()
    {
        return array_merge(
            array(
                array('title', 'safe'),
                array('filename, orig', 'safe'),
                //array('title', 'ext.validators.CapsValidator'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getVisibleTitle()
    {
        if (!empty($this->title))
            return $this->title;
        return $this->orig;
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }
}
