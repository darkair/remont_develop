<?php

class AdminReviewsController extends MAdminController
{
    public $modelName = 'Review';
    public $modelHumanTitle = array('отзыв', 'отзывов', 'отзывов');
    public $allowedActions = 'add,edit,delete,update';

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageControllerBehavior',
                'imageField' => 'image',
                'imageWidth' => Review::IMAGE_W,
                'imageHeight' => Review::IMAGE_H,
                'resizeType' => 'cover',
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array(
            'text' => ['type' => 'ckEditor'],
            '_image' => array(
                'class' => 'ext.ImageFileRowWidget',
                'uploadedFileFieldName' => '_image',
                'removeImageFieldName' => '_removeImageFlag',
            ),
            'info' => ['type' => 'textArea'],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            $this->getImageColumn('image', 'getImageUrl()'),
            array(
                'name' => 'text',
                'value' => 'StringUtils::html2txt($data->text)',
            ),
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->imageBehavior->imageBeforeSave($model, $model->getImageStorePath());
    }
}
