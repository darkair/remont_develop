<?php

class FaqModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.faq.models.*');
    }
}
