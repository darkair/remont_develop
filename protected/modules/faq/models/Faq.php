<?php

/**
 * FAQ model
 */
class Faq extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'faq';
    }

    public function attributeLabels()
    {
        return array(
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'visible' => 'Показывать',
        );
    }

    public function rules()
    {
        return array(
            array('question, answer', 'safe'),
            array('visible', 'boolean'),

            array('visible', 'safe', 'on'=>'search'),
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }
}
