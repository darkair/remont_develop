<?php

Yii::import('application.models.Menu');

class ArticleMenuWidget extends MenuWidget
{
    public function run()
    {
        $link = trim(Yii::app()->request->getPathInfo(), '/');

        $menuItem = MenuItem::model()->onSite()->byLink($link)->find();
        if (!$menuItem)
            return;

        if (!empty($menuItem->parentItemId)) {
            $menuItem = MenuItem::model()->onSite()->findByPk($menuItem->parentItemId);
            if (!$menuItem)
                return;
        }

        $this->template = 'articleMenu';
        $this->menuId = Menu::MAIN_MENU;

        $items = $this->createMenuItemData($menuItem);

        $this->beforeRender($items);
        $this->render($this->template, array_merge($this->options, ['menu'=>$items]));
    }
}