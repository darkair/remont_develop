<?php

Yii::import('modules.gallery.models.*');

class AdminGalleryItemController extends MAdminController
{
    public $modelName = 'GalleryItem';
    public $modelHumanTitle = array('изображение', 'изображений', 'изображений');
    public $allowedRoles = 'admin, moderator';
    public $allowedActions = 'editRaw';

    public function getEditFormElements($model)
    {
        return array_merge(
            $this->getLangField('title',    ['type' => 'textArea'])
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'id',
            'title',
            $this->getButtonsColumn()
        );
        return $attributes;
    }
}
