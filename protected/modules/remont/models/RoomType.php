<?php

/**
 * Тип комнаты
 */
class RoomType extends CActiveRecord
{
    public $roomCompartmentsList = [];
    public $_innerRoomCompartments = [];

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'roomtype';
    }

    public function behaviors()
    {
        return array(
            'childListBehavior' => array(
                'class'                 => 'application.behaviors.ChildListBehavior',
                'label'                 => 'Помещения',
                'childField'            => 'roomCompartments',
                'childObjField'         => 'roomCompartmentsList',
                'innerChildField'       => '_innerRoomCompartments',
                'childClass'            => 'RoomCompartment',
                'unique'                => false
            ),
            'orderBehavior' => array(
                'class' => 'application.behaviors.OrderBehavior',
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->childListBehavior->childListLabels(),
            $this->orderBehavior->orderLabels(),
            array(
                'roomsNumber' => 'Количество комнат',
                'square' => 'Площадь (м.кв.)',
                'squareMin' => 'Минимальная площадь (м.кв.)',
                'complectationCost' => 'Стоимость метра комплектации',
                'fullremontCost' => 'Стоимость метра полного ремонта',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->childListBehavior->childListRules(),
            $this->orderBehavior->orderRules(),
            array(
                array('roomsNumber', 'required'),
                array('square', 'numerical'),
                array('squareMin', 'numerical'),
                array('complectationCost', 'numerical', 'integerOnly'=>true),
                array('fullremontCost', 'numerical', 'integerOnly'=>true),
                array('visible', 'boolean'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
            'orderDefault' => array(
                'order' => $alias.'.orderNum ASC',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

//        $alias = $this->getTableAlias();
//        $criteria->compare($alias.'.position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'orderNum' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function setAttribute($name, $value)
    {
        $this->orderBehavior->orderSetAttribute($name, $value);
        return parent::setAttribute($name, $value);
    }

    public function initRoomCompartments($ids)
    {
        $this->childListBehavior->initObjects($ids);
    }
}
