<?php

class m161130_103900_rooms_structure extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `roomtype` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `roomsNumber`   varchar(32) NOT NULL DEFAULT 1  COMMENT 'Количество комнат',
                `roomBlocks`    TEXT NOT NULL                   COMMENT 'Id комнатных блоков в JSON',
                `orderNum`      int(10) NOT NULL DEFAULT 0      COMMENT 'Порядок сортировки',
                `visible`       tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `orderNum` (`orderNum`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `roomblock` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `title`         varchar(255) NOT NULL DEFAULT ''    COMMENT 'Заголовок',
                `image`         varchar(100) NOT NULL               COMMENT 'Картинка',
                `roomGroups`    TEXT NOT NULL                       COMMENT 'Id групп параметров в JSON',
                `visible`       tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `roomparamgroup` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `title`         varchar(255) NOT NULL DEFAULT ''    COMMENT 'Заголовок',
                `titleInner`    varchar(255) NOT NULL DEFAULT ''    COMMENT 'Внутренний заголовок',
                `desc`          text NOT NULL                       COMMENT 'Описание',
                `roomParams`    TEXT NOT NULL                       COMMENT 'Id параметров в JSON',
                `visible`       tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `roomparam` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `title`         varchar(255) NOT NULL DEFAULT ''    COMMENT 'Заголовок',
                `image`         varchar(100) NOT NULL               COMMENT 'Картинка',
                `minCost`       int(11) NOT NULL DEFAULT 0          COMMENT 'Стоимость услуги',
                `maxCost`       int(11) NOT NULL DEFAULT 0          COMMENT 'Стоимость услуги',
                `visible`       tinyint(1) NOT NULL DEFAULT '1'     COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
    }

    public function safeDown()
    {
        $this->dropTable('roomtype');
        $this->dropTable('roomblock');
        $this->dropTable('roomparamgroup');
        $this->dropTable('roomparam');
    }
}