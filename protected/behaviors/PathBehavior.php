<?php

class PathBehavior extends CActiveRecordBehavior
{
    public $childClass = null;
    public $label = '';
    public $childField = '';            // Поле для JSON
    public $innerChildField = '';       // Внутреннее поле для редактирования

    private $pathPoints = null;


    public function pathLabels()
    {
        $arr = array(
            $this->childField => $this->label,
        );
        return $arr;
    }

    public function pathRules()
    {
        $rules = array(
            array($this->childField, 'safe'),
            array($this->innerChildField, 'safe')
        );
        return $rules;
    }

    public function afterFind($event)
    {
        if (!empty($this->owner->{$this->childField})) {
            $this->owner->{$this->childField} = CJSON::decode($this->owner->{$this->childField});
            if (!is_array($this->owner->{$this->childField}))
                $this->owner->{$this->childField} = array();
        }
    }

    public function beforeSave($event)
    {
        $this->owner->{$this->childField} = CJSON::encode($this->owner->{$this->childField});
    }

    public function initPath($path)
    {
        $class = $this->childClass;
        if (!$class)
            return false;

        if (!empty($path) && is_array($path)) {
            reset($path);
            if (count($path) == 1 && current($path) === "")
                $path = [];

            // remove old points
            $parentClass = get_class($this->owner);
            $m = $parentClass::model()->findByPk($this->owner->primaryKey);
            $oldPath = $m->{$this->childField};
            foreach ($path as $p) {
                if ($p['id'] == 0)
                    continue;
                $key = array_search($p['id'], $oldPath);
                if ($key !== false)
                    unset($oldPath[$key]);
            }
            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $oldPath);
            $class::model()->deleteAll($criteria);

            $ids = [];
            foreach ($path as $p) {
                if ($p['id'] == 0) {
                    // Create new point
                    $point = new $class;
                    $point->lng = $p['lng'];
                    $point->lat = $p['lat'];
                    if (!$point->save())
                        throw new CException(print_r($point->getErrors(), true));
                    $ids[] = $point->id;
                } else {
                    $ids[] = $p['id'];
                }
            }
            $this->owner->{$this->childField} = $ids;
        }
    }

    public function getPathPoint($id)
    {
        $class = $this->childClass;
        if (!$class)
            return null;

        return $class::model()->findByPk($id);
    }

    public function getAllPathPoints()
    {
        if ($this->pathPoints === null) {
            $class = $this->childClass;
            if (!$class)
                return null;

            $ids = $this->owner->{$this->childField};
            if (empty($ids))
                return null;

            $criteria = new CDbCriteria;
            $criteria->addInCondition('id', $ids);
            $this->pathPoints = $class::model()->findAll($criteria);
        }
        return $this->pathPoints;
    }
}
