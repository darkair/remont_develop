<?php

Yii::import('modules.remont.models.*');

class m161201_122122_rooms_data extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->renameColumn('roomtype', 'roomBlocks', 'roomCompartments');

        $paramsId = [];
        $paramsId['plintus'][] = $this->createRoomParam('Классический белый', 10);
        $paramsId['plintus'][] = $this->createRoomParam('Современный белый', 15);
        $paramsId['plintus'][] = $this->createRoomParam('Классический в цвет стен', 20);
        $paramsId['plintus'][] = $this->createRoomParam('Современный в цвет стен', 25);
        $paramsId['wall'][] = $this->createRoomParam('Серебристый', 10);
        $paramsId['wall'][] = $this->createRoomParam('Зеленый', 15);
        $paramsId['wall'][] = $this->createRoomParam('Желтый', 20);
        $paramsId['wall'][] = $this->createRoomParam('Белый', 25);
        $paramsId['wall'][] = $this->createRoomParam('Темно-оранжевый', 30);
        $paramsId['wall'][] = $this->createRoomParam('Темно-серый', 35);
        $paramsId['floor'][] = $this->createRoomParam('Светлый ламинат', 10);
        $paramsId['floor'][] = $this->createRoomParam('Темный ламинат', 15);
        $paramsId['floor'][] = $this->createRoomParam('Паркет', 20);
        $paramsId['floor'][] = $this->createRoomParam('Темный паркет', 25);
        $paramsId['floor'][] = $this->createRoomParam('Светлый дубовый паркет', 30);
        $paramsId['door'][] = $this->createRoomParam('Матовая дверь', 10);
        $paramsId['door'][] = $this->createRoomParam('Филенчатая дверь', 15);

        $paramsGroupId = [];
        $paramsGroupId[] = $this->createRoomParamGroup('Плинтус', 'Описание плинтуса', $paramsId['plintus']);
        $paramsGroupId[] = $this->createRoomParamGroup('Цвет стен', 'Описание стен', $paramsId['wall']);
        $paramsGroupId[] = $this->createRoomParamGroup('Цвет пола', 'Описание пола', $paramsId['floor']);
        $paramsGroupId[] = $this->createRoomParamGroup('Двери', 'Описание дверей', $paramsId['door']);

        $blocksId = [];
        $blocksId[1] = $this->createRoomBlock('Первая комната', $paramsGroupId);
        $blocksId[2] = $this->createRoomBlock('Вторая комната', $paramsGroupId);
        $blocksId[3] = $this->createRoomBlock('Прихожая и кухня', $paramsGroupId);

        $this->createRoom(1, [$blocksId[1], $blocksId[3]]);
        $this->createRoom(2, [$blocksId[1], $blocksId[2], $blocksId[3]]);
    }

    public function safeDown()
    {
        $this->deleteAllModels('RoomType');
        $this->deleteAllModels('RoomBlock');
        $this->deleteAllModels('RoomParamGroup');
        $this->deleteAllModels('RoomParam');
        $this->renameColumn('roomtype', 'roomCompartments', 'roomBlocks');
    }

    private function createRoomParam($title, $minCost)
    {
        $roomParam = new RoomParam('migrate');
        $roomParam->title = $title;
        $roomParam->minCost = $minCost;
        $roomParam->image = '';
        
        $roomParam->save();
        return $roomParam->id;
    }

    private function createRoomParamGroup($title, $desc, $paramsId)
    {
        $roomParamGroup = new RoomParamGroup('migrate');
        $roomParamGroup->title = $title;
        $roomParamGroup->titleInner = $title;
        $roomParamGroup->desc = $desc;
        $roomParamGroup->roomParams = $paramsId;
        $roomParamGroup->save();
        return $roomParamGroup->id;
    }

    private function createRoomBlock($title, $paramsGroupId)
    {
        $roomBlock = new RoomBlock('migrate');
        $roomBlock->title = $title;
        $roomBlock->roomGroups = $paramsGroupId;
        $roomBlock->image = '';
        $roomBlock->save();
        return $roomBlock->id;
    }

    private function createRoom($roomNumbers, $blocksId)
    {
        $room = new RoomType('migrate');
        $room->roomsNumber = $roomNumbers;
        $room->roomCompartments = $blocksId;
        $room->save();
        return $room->id;
    }
}