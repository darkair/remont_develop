<?php

/**
 * Время
 * подключить в behaviors:
            'timeBehavior' => array(
                'class' => 'application.behaviors.TimeBehavior',
                'timeField' => 'createTime',
            ),
 * добавить названия поля в attributeLabels:
            $this->timeBehavior->timeLabels(),
 * добавить правила в rules:
            $this->timeBehavior->timeRules(),
 * добавить колонку в adminController::getTableColumns()
            '_createTime',
 * добавить, если необходимо, поле в форму в adminController::getEditFormElements()
                'createTime' => [
                    'type' => 'datepicker',
                    'htmlOptions' => [
                        'options' => [
                            'format' => 'dd.mm.yyyy'
                        ],
                    ],
                ]
*/

class TimeBehavior extends CActiveRecordBehavior
{
    public $timeField = '';
    public $required = false;

    public $createTimeDate = '';
    public $createTimeTime = '';


    public function timeLabels()
    {
        return array(
            'createTimeTime' => 'Время создания',
            'createTimeDate' => 'Дата создания'
        );
    }

    public function timeRules()
    {
        $arr = $this->required
            ? array('createTimeTime, createTimeDate', 'required')
            : array('createTimeTime, createTimeDate', 'safe');

        return array(
            array($this->timeField, 'numerical', 'integerOnly'=>true),
            $arr
        );
    }

    public function afterFind($event)
    {
        $this->createTimeDate = date('d.m.Y', $this->owner->{$this->timeField});
        $this->createTimeTime = date('H:i', $this->owner->{$this->timeField});
    }

    public function beforeSave($event)
    {
        if (!empty($this->createTimeDate)) {
            if (empty($this->createTimeTime))
                $this->createTimeTime = '00:00';
            $this->owner->{$this->timeField} = strtotime($this->createTimeDate.' '.$this->createTimeTime);
        }

        if (empty($this->owner->{$this->timeField}))
            $this->owner->{$this->timeField} = time();
    }
}