<?php

class FeedbackModule extends CWebModule
{
    public $defaultController='admin';

    public function init()
    {
        parent::init();
        Yii::import('modules.feedback.models.*');
    }
}
