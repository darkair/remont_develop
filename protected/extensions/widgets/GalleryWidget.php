<?php

class GalleryWidget extends ExtendedWidget
{
    // v1.0
    public $images = [];

    // v2.0
    public $items = null;

    public function init()
    {
        TwigFunctions::importResource('js', 'jquery.colorbox.js');
        TwigFunctions::importResource('css', 'colorbox.css');
        
        parent::init();
    }

    public function run()
    {
        if ($this->items !== null) {
            $this->render('gallery2', array(
                'items' => $this->items
            ));            
        } else {
            $this->render('gallery', array(
                'images' => $this->images
            ));
        }
    }
}
