<?php

class CallBackHunterWidget extends ExtendedWidget
{
    public function run()
    {
        PrefetchHelper::dnsPrefetch('//callbackhunter.com/');

        $this->render('callBackHunter');
    }
}