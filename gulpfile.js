// npm install --global gulp-cli
// npm install --save-dev gulp (in project folder)
// ln -s /usr/bin/nodejs /usr/bin/node

var gulp = require('gulp'),
    less = require('gulp-less'),
    path = require('path'),
    rename = require('gulp-rename'),
    minifyCSS = require('gulp-minify-css'),
    stripCssComments = require('gulp-strip-css-comments'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    watch = require('gulp-watch');
require('es6-promise').polyfill();

var prod = false;

gulp.task('prod:set', function(done) {
    prod = true;
    done();
});

gulp.task('app:css', function (done) {
    lessCompile(lessPaths);
    done();
});

var lessPaths = [
    'protected/extensions/widgets/less/**/*.less',
    'protected/modules/**/*.less',
    'protected/views/less/**/*.less',
    'www/ckeditor/less/**/*.less',
    'www/themes/**/*.less'
];

function lessCompile(paths)
{
    gulp.src(paths, {base: './'})
        .pipe(less({javascriptEnabled: true}))
        .pipe(rename( function(path) {
            path.dirname += "/../css";
        }))
        .pipe(autoprefixer({
            browsers: ['last 20 versions', 'Android > 2.0.0']
        }))
        .pipe(prod ? stripCssComments({preserve:false}) : gutil.noop())
        .pipe(prod ? minifyCSS(): gutil.noop())
        .pipe(gulp.dest('.'));
}

gulp.task('watch', function(done) {
    // Watch .less files
    gulp.watch(lessPaths, function (event) {
        gutil.log('File ' + event.path + ' was ' + event.type);
        var currentDirectory = path.dirname(event.path);
        lessCompile(currentDirectory+'/*.less');
    });
    gulp.watch([
        'protected/views/less_include/**/*.less',
        'www/themes/*/views/less_include/**/*.less'
    ], function (event) {
        gutil.log('File ' + event.path + ' was ' + event.type + '. All files will be recompiled.');
        lessCompile(lessPaths);
    });
    done();
});

gulp.task('default', gulp.series('app:css', 'watch'), function(done) {
    done();
});
gulp.task('prod', gulp.series('prod:set', 'app:css', 'default', function(done) {
    done();
}));

