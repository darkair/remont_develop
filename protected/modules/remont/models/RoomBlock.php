<?php

/**
 * Комнатный блок
 */
class RoomBlock extends CActiveRecord
{
    const IMAGE_W = 981;
    const IMAGE_H = 589;

    public $_image = null;
    public $_removeImageFlag = false;

    public $roomGroupsList = [];
    public $_innerRoomGroups = [];

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'roomblock';
    }

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageBehavior',
                'storagePath' => 'rooms',
                'imageLabel' => 'Изображение '.self::IMAGE_W.'x'.self::IMAGE_H,
                'imageField' => 'image',
                'imageExt' => 'png, jpeg, jpg',
                'required' => true,
            ),
            'childListBehavior' => array(
                'class'                 => 'application.behaviors.ChildListBehavior',
                'label'                 => 'Группы параметров',
                'childField'            => 'roomGroups',
                'childObjField'         => 'roomGroupsList',
                'innerChildField'       => '_innerRoomGroups',
                'childClass'            => 'RoomParamGroup',
            )
        );
    }


    public function attributeLabels()
    {
        return array_merge(
            $this->imageBehavior->imageLabels(),
            $this->childListBehavior->childListLabels(),
            array(
                'title' => 'Заголовок',
                'projectCost' => 'Стоимость проекта',
                'nadzorCost' => 'Стоимость надзора',
                'visible' => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->imageBehavior->imageRules('', 'migrate'),
            $this->childListBehavior->childListRules(),
            array(
                array('image', 'safe', 'on'=>'migrate'),
                array('title', 'required'),
                array('projectCost, nadzorCost', 'numerical', 'except'=>'migrate'),
                array('visible', 'boolean'),
            )
        );
    }

    /**
     * Отмечаем значком "required"
     */
    public function isAttributeRequired($attribute)
    {
        if (in_array($attribute, array('_image')))
            return true;
        return parent::isAttributeRequired($attribute);
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

//        $alias = $this->getTableAlias();
//        $criteria->compare($alias.'.position', $this->position);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getImageStorePath()
    {
        return $this->imageBehavior->getStorePath();
    }

    public function getImageUrl()
    {
        return $this->imageBehavior->getImageUrl();
    }

    public function initRoomParamGroups($ids)
    {
        $this->childListBehavior->initObjects($ids);
    }
}
