<?php

class m160621_082845_articles_gallery extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE articles ADD COLUMN `images` TEXT NOT NULL COMMENT 'Галлерея в JSON' AFTER link");
    }

    public function safeDown()
    {
        $this->dropColumn('articles', 'images');
    }
}