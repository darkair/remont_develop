<?php

Yii::import('modules.remont.models.Subparagraph');

class m170608_132105_subparagraphs_data extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createSubparagraph('Нужен демонтаж старого ремонта', 14000);
        $this->createSubparagraph('Нужна разводка электрики', 42000);
        $this->createSubparagraph('Нужно возвести внутренние стены', 44000);
        $this->createSubparagraph('Нужна стяжка полов', 11000);
    }

    public function safeDown()
    {
        $this->deleteAllModels('Subparagraph');
    }

    private function createSubparagraph($title, $cost)
    {
        $item = new Subparagraph();
        $item->title = $title;
        $item->cost = $cost;
        $item->visible = 1;
        if (!$item->save())
            throw new Exception('Не получилось создать подпункт');
    }
}