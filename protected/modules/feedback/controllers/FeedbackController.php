<?php

class FeedbackController extends Controller
{
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                //'backColor' => 'transparent',
                //'foreColor' => 0x308000,
                //'width' => 200,
                //'height' => 60,
//                'testLimit' => 1,
            ),
        );
    }

    public function actionFeedback()
    {
        if (!Yii::app()->request->isAjaxRequest || !Yii::app()->request->isPostRequest)
            throw new CHttpException(404);

        if (!isset($_POST['FeedbackForm']))
            throw new CHttpException(404);

        $form = new FeedbackForm;
        $form->attributes = $_POST['FeedbackForm'];
        if (!$form->validate()) {
            // Формируем список ошибок и даем полям правильные имена, используемые в форме
            $errors = array();
            foreach ($form->getErrors() as $key=>$value) {
                $errors[CHtml::activeId($form, $key)] = $value;
            }
            echo CJSON::encode(array('errors'=>$errors));
            Yii::app()->end();
        }

        $section = FeedbackSection::model()->onSite()->findByPk($form->section);
        if (!$section)
            throw new CHttpException(400);

        // Отправка письма
        $mail = MailHelper::createYiiMailer(
            'feedback',
            Yii::t('app', 'Обратная связь с сайта'),
            Yii::app()->localConfig->getParam('contact-info.email'),
//            $section->email,
            [
                'name' => $form->name,
                'phone' => $form->phone,
                'email' => $form->email,
                'text' => $form->text,
                'section' => $section->title,
            ]
        );

        if (!$mail->send()) {
            // Отправляем общую ошибку в поле name
            header('HTTP/1.1 400 '.$mail->getError());
            echo CJSON::encode(['success'=>'false']);
            Yii::log($mail->getError(), CLogger::LEVEL_ERROR, 'mail');
            Yii::app()->end();
        }

        echo CJSON::encode(array('success'=>'true'));
        Yii::app()->end();
    }
}
