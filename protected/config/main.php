<?php

Yii::setPathOfAlias('lib', realpath(__DIR__ . '/../../lib'));
Yii::setPathOfAlias('modules', realpath(__DIR__ . '/../modules'));

if (is_file('../protected/config/params.php')) {
    $params = include '../protected/config/params.php';
} else {
    $params = include '../protected/config/params-global.php';
}

$res = array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => $params['appName'],
    'theme' => 'remont',
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.models.forms.*',
        'application.components.*',
        'application.helpers.*',
        'lib.CurlHelper.*',
        'lib.ImageHelper.*',
        'ext.mAdmin.*',
        'ext.YiiMailer.YiiMailer',
        'ext.localConfig.*',
        'modules.contentBlocks.models.*',
        'modules.tags.models.*',
    ),
    'modules' => array(
        'admin',
        'sitemenu',
        'contentBlocks',
        'articles',
        'tags',
        'documentsList',
        'gallery',
        'mainPage',
        'remont',
        'feedback',
        'faq',
        'reviews',
    ),
    'components' => array(
        'clientScript' => array(
            'packages' => require('packages.php')
        ),
        // 'assetManager' => array(
        //     'forceCopy' => true
        // ),
        'themeManager'=>array(
            'class' => 'application.components.ThemeManager',
            'subthemeFunc' => ''
        ),
        'simplepie' => array(
            'class' => 'ext.yii-simplepie.simplepie-library.bootstrap'
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => array('site/login'),
        ),
        'request' => array(
            'class' => 'application.components.HttpRequest',
            'baseUrl' => $params['baseUrl'],
            'enableCsrfValidation' => true,     // Включаем CSRF защиту для форм
        ),
        'urlManager' => array(
            'class' => 'application.components.UrlManager',
            'urlFormat' => 'path',
            'urlSuffix' => '',
            'showScriptName' => false,
            'rules' => array(),
            'useStrictParsing' => true,         // Запрещаем обращаться к контроллерам напрямую, только через route
            'useArticlesUrl' => true,
        ),
        'db' => array(
            'connectionString' => 'mysql:host=' . $params['dbHost'] . ';dbname=' . $params['dbName'],
            'emulatePrepare' => true,
            'username' => $params['dbLogin'],
            'password' => $params['dbPassword'],
            'charset' => 'utf8',
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        ),
        'fs' => array(
            'class' => 'FileSystem',
            'nestedFolders' => 1,
        ),
        'session' => array(
            'class' => 'CDbHttpSession',
            'autoCreateSessionTable' => true,
            'connectionID' => 'db',
            'timeout' => 3600,       // 60 minutes
            'gcProbability' => 100,
        ),
        'viewRenderer' => array(
            'class' => 'lib.twig-renderer.ETwigViewRenderer',
            'twigPathAlias' => 'lib.twig.lib.Twig',
            'options' => array(
                'autoescape' => true,
            ),
            'functions' => array(
                'widget'    => array(
                    0 => 'TwigFunctions::widget',
                    1 => array('is_safe' => array('html')),
                ),
                'const'                     => 'TwigFunctions::constGet',
                'static'                    => 'TwigFunctions::staticCall',
                'call'                      => 'TwigFunctions::call',
                'import'                    => 'TwigFunctions::importResource',
                'importLink'                => 'TwigFunctions::importLink',
                'registerPackages'          => 'TwigFunctions::registerPackages',
                'absLink'                   => 'TwigFunctions::absLink',
                'plural'                    => 'TwigFunctions::plural',
                'dump'                      => 'TwigFunctions::dump',
                'isDebug'                   => 'TwigFunctions::isDebug',
                'link'                      => 'CHtml::normalizeUrl',
                '_t'                        => 'Yii::t',                                        // см. messages/config.php
            ),
            'filters' => array(
                'unset'                     => 'TwigFunctions::filterUnset',
                'translit'                  => 'TwigFunctions::filterTranslit',
                'text'                      => 'TwigFunctions::filterText',
                'externalLink'              => 'TwigFunctions::filterExternalLink',
                'fixSkype'                  => 'TwigFunctions::filterFixSkype',
                'extractFileName'           => 'TwigFunctions::filterExtractFileName',

                'formatDate'                => 'TwigFunctions::filterFormatDate',
                'formatTime'                => 'TwigFunctions::filterFormatTime',
                'formatDateTime'            => 'TwigFunctions::filterFormatDateTime',
                'formatMonthYear'           => 'TwigFunctions::filterFormatMonthYear',
            ),
        ),
        'adminComponent' => array(
            'class' => 'ext.mAdmin.components.AdminComponent'
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'image' => array(
            'class' => 'ext.image.CImageComponent',
            'driver' => $params['imageDriver'],
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'categories' => 'application'
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                    'categories' => 'mail',
                    'logFile' => 'mail.log'
                ],
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                    'categories' => 'admin.*',
                    'logFile' => 'admin.log'
                ],
            ),
        ),
        'localConfig' => array(
            'class' => 'ext.localConfig.LocalConfigExtension'
        ),
        'cache'=>array(
            'class'=>'system.caching.CDbCache',
            'connectionID'=>'db',
        ),
    ),
    'controllerMap' => array(
        'barcodegenerator' => array(
            'class' => 'ext.barcodegenerator.BarcodeGeneratorController',
        ),
    ),
    'params' => array_merge(
        $params,
        array(
            'md5Salt' => 'ThisIsMymd5Salt(*&^%$#',
        )
    ),
);


$langArr = array_keys($res['params']['languages']);
$langPrefix = '<language:('. implode('|', $langArr) .')>';

$res['sourceLanguage'] = 'ru';
$res['language'] = $langArr[0];

// NOTE: в правилах сперва должен идти роут без action, чтобы преобразования <module>/<contoller>/index давали простые урлы

$res['components']['urlManager']['rules'] = array(
    $langPrefix.'/file/<path:[\w\W]+>/<filename:[\w\W]+>'   => 'mainPage/mainPage/getFile',
    'file/<path:[\w\W]+>/<filename:[\w\W]+>'                => 'mainPage/mainPage/getFile',
    $langPrefix.'/'                                 => 'mainPage/mainPage/index',
    '/'                                             => 'mainPage/mainPage/index',

    $langPrefix.'/login/'                           => 'site/login',
    'login/'                                        => 'site/login',
    $langPrefix.'/logout/'                          => 'site/logout',
    'logout/'                                       => 'site/logout',

    $langPrefix.'/feedback'                         => 'feedback/feedback/feedback',
    '/feedback'                                     => 'feedback/feedback/feedback',
    $langPrefix.'/captcha'                          => 'feedback/feedback/captcha',
    '/captcha'                                      => 'feedback/feedback/captcha',

//    $langPrefix.'/reviews'                          => 'reviews/reviews/index',
//    '/reviews'                                      => 'reviews/reviews/index',

    $langPrefix.'/politic'                          => 'site/politic',
    '/politic'                                      => 'site/politic',

    $langPrefix.'/sendForm'                         => 'remont/rooms/sendForm',
    '/sendForm'                                     => 'remont/rooms/sendForm',
    $langPrefix.'/roomData'                         => 'remont/rooms/getRoomData',
    '/roomData'                                     => 'remont/rooms/getRoomData',
    $langPrefix.'/savePicture'                      => 'remont/rooms/savePicture',
    '/savePicture'                                  => 'remont/rooms/savePicture',
    $langPrefix.'/recalc'                           => 'remont/rooms/recalc',
    '/recalc'                                       => 'remont/rooms/recalc',

    // Rooms API
    $langPrefix.'/api/roomsInfo'                    => 'remont/roomsApi/getRoomsInfo',
    '/api/roomsInfo'                                => 'remont/roomsApi/getRoomsInfo',
    $langPrefix.'/api/roomsData'                    => 'remont/roomsApi/getRoomsData',
    '/api/roomsData'                                => 'remont/roomsApi/getRoomsData',

    $langPrefix.'/documents/file/<id:\w+>/<filename:[\w\W]+>'   => 'site/getFile',
    'documents/file/<id:\w+>/<filename:[\w\W]+>'                => 'site/getFile',

    // Admin
    'admin/<action:\w+>/'                           => 'admin/admin/<action>',      // Обработка ошибок
    'admin/'                                        => 'admin',
    'admin/<module:\w+>/<controller:\w+>/'          => '<module>/admin<controller>',
    'admin/<module:\w+>/<controller:\w+>/<action:\w+>/' => '<module>/admin<controller>/<action>',

    $langPrefix.'/<action:\w+>/'                    => 'site/<action>',
    '/<action:\w+>/'                                => 'site/<action>',
);

return $res;
