<?php

/**
 * Рубрика обратной связи
 */
class FeedbackSection extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'feedbacksections';
    }

    public function behaviors()
    {
        return array(
            'orderBehavior' => array(
                'class' => 'application.behaviors.OrderBehavior',
            ),
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'FeedbackSectionLang',
                'langTableName'         => 'feedbacksections_lang',
                'langForeignKey'        => 'fbsId',
                'localizedAttributes'   => array('title'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->orderBehavior->orderLabels(),
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Заголовок',
            )),
            array(
                'email'     => 'Почта',
                'visible'   => 'Показывать?',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->orderBehavior->orderRules(),
            array(
                array('title', 'required'),
                array('email', 'required'),
                array('email', 'email'),
                array('visible', 'boolean'),

                array('title, email, visible', 'safe', 'on'=>'search'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
            'orderDefault' => array(
                'order' => $alias.'.orderNum DESC',
            ),
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.title', $this->title, true);
        $criteria->compare($alias.'.email', $this->email, true);
        $criteria->compare($alias.'.visible', $this->visible);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'orderNum' => CSort::SORT_DESC,
                )
            )
        ));
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }

    public function setAttribute($name, $value)
    {
        $this->orderBehavior->orderSetAttribute($name, $value);
        return parent::setAttribute($name, $value);
    }
}
