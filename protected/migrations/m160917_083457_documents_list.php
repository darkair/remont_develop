<?php

Yii::import('modules.documentsList.models.DocumentsList');

class m160917_083457_documents_list extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `documentslist` (
                `id`            int(11) NOT NULL AUTO_INCREMENT,
                `type`          int(11) NOT NULL COMMENT 'Тип',
                `title`         VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'Заголовок',
                `docs`          TEXT NOT NULL COMMENT 'Файлы в JSON',
                `visible`       tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Видимость',
                PRIMARY KEY (`id`),
                KEY `title` (`title`),
                KEY `visible` (`visible`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `documentslist_lang` (
                `l_id` int(11) NOT NULL AUTO_INCREMENT,
                `documentsListId` int(11) NOT NULL,
                `lang_id` varchar(6) NOT NULL,
                `l_title` text NOT NULL,
                PRIMARY KEY (`l_id`),
                KEY `documentsListId` (`documentsListId`),
                KEY `lang_id` (`lang_id`),
                CONSTRAINT `fk_documents_list_lang` FOREIGN KEY (`documentsListId`) REFERENCES `documentslist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");

    }

    public function safeDown()
    {
        $this->deleteAllModels('DocumentsList');
        $this->dropTable('documentslist_lang');
        $this->dropTable('documentslist');
    }
}
