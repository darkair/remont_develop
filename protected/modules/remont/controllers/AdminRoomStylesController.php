<?php

Yii::import('application.helpers.*');
Yii::import('lib.SypexGeo.SxGeo22_API.SxGeo');

class AdminRoomStylesController extends MAdminController
{
    public $modelName = 'RoomType';
    public $modelHumanTitle = array('стиль', 'стилей', 'стилей');
    protected $templateList = '/admin/roomStyles';
}
