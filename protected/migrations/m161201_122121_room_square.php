<?php

class m161201_122121_room_square extends CDbMigration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE roomtype ADD COLUMN `square`   int(10) NOT NULL DEFAULT 0 COMMENT 'Площадь' AFTER roomBlocks");

        $this->execute("ALTER TABLE roomtype ADD COLUMN `complectationCost` int(10) NOT NULL DEFAULT 0 COMMENT 'Стоимость комплектации' AFTER square");
        $this->execute("ALTER TABLE roomtype ADD COLUMN `fullremontCost` int(10) NOT NULL DEFAULT 0 COMMENT 'Стоимость полного ремонта' AFTER square");

        $this->execute("ALTER TABLE roomtype ADD COLUMN `squareMin`   int(10) NOT NULL DEFAULT 0 COMMENT 'Минимальная площадь' AFTER square");
        $this->execute('UPDATE roomtype SET squareMin=square WHERE 1');
    }

    public function safeDown()
    {
        $this->dropColumn('roomtype', 'squareMin');

        $this->dropColumn('roomtype', 'complectationCost');
        $this->dropColumn('roomtype', 'fullremontCost');

        $this->dropColumn('roomtype', 'square');
    }
}
