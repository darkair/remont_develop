<?php

/**
 * Добавление тегов предзагрузки в шапку сайта
 */

class PrefetchHelper
{
    private static $dnsPrefetch = [];
    private static $linkPrefetch = [];

    public static function dnsPrefetch($url)
    {
        $key = md5($url);
        self::$dnsPrefetch[$key] = $url;
    }

    public static function linkPrefetch($url)
    {
        $key = md5($url);
        self::$linkPrefetch[$key] = $url;
    }

    public static function render()
    {
        $html = '';
        foreach (self::$dnsPrefetch as $url)
            $html .= CHtml::linkTag('dns-prefetch', null, $url)."\n";
        foreach (self::$linkPrefetch as $url)
            $html .= CHtml::linkTag('prefetch', null, $url)."\n";
        return $html;
    }
}