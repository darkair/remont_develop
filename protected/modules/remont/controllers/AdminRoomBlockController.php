<?php

class AdminRoomBlockController extends MAdminController
{
    public $modelName = 'RoomBlock';
    public $modelHumanTitle = array('комнатный блок', 'комнатного блока', 'комнатных блоков');

    public function behaviors()
    {
        return array(
            'imageBehavior' => array(
                'class' => 'application.behaviors.ImageControllerBehavior',
                'imageField' => 'image',
                'imageWidth' => RoomBlock::IMAGE_W,
                'imageHeight' => RoomBlock::IMAGE_H,
                'resizeType' => 'cover',
            ),
        );
    }

    public function getEditFormElements($model)
    {
        return array(
            'title' => ['type' => 'textField'],
            '_image' => array(
                'class' => 'ext.ImageFileRowWidget',
                'uploadedFileFieldName' => '_image',
                'removeImageFieldName' => '_removeImageFlag',
            ),
            'roomGroupsList' => [
                'type' => 'childList',
                'htmlOptions' => [
                    'innerChildField' => '_innerRoomGroups',
                    'valueField' => 'id',
                    'textField' => 'titleInner',
                    'childClass' => 'RoomParamGroup',
                    'editUrl' => '/remont/adminRoomParamGroup',
                    'useEditRaw' => true
                ]
            ],
            'projectCost' => ['type' => 'textField'],
            'nadzorCost' => ['type' => 'textField'],
            'visible' => ['type' => 'checkBox'],
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'title',
            $this->getImageColumn('image', 'getImageUrl()'),
            'projectCost',
            'nadzorCost',
            $this->getVisibleColumn(),
            $this->getButtonsColumn(),
        );

        return $attributes;
    }

    public function beforeSave($model)
    {
        $this->imageBehavior->imageBeforeSave($model, $model->getImageStorePath());
        $model->initRoomParamGroups($model->_innerRoomGroups);
        parent::beforeSave($model);
    }
}
