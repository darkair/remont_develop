<?php

class TwigFunctions
{
    /**
     * @param string $class
     * @param array $properties
     * @return string
     */
    public static function widget($class, $properties = array())
    {
        $className = Yii::import($class, true);
        foreach ($properties as $propertyName => $value)
        {
            if (!property_exists($className, $propertyName) && !method_exists($className, 'set'.$propertyName))
                unset($properties[$propertyName]);
        }

        $c = Yii::app()->getController();
        return $c->widget($class, $properties, true);
    }

    /**
     * @param string $class
     * @param string $property
     * @return mixed
     */
    public static function constGet($class, $property)
    {
        $c = new ReflectionClass($class);
        return $c->getConstant($property);
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $params
     * @return mixed
     */
    public static function staticCall($class, $method, $params = array())
    {
        return call_user_func_array($class . '::' . $method, $params);
    }

    public static function call($function, $params = array())
    {
        return call_user_func_array($function, $params);
    }

    /**
     * Добавить CSS
     */
    public static function importResource($type, $filename, $alias=false, $media='')
    {
        $assetsPath = self::getThemeAssetsPath($type, $filename, $alias);

        switch ($type) {
            case 'css':
                if (!$assetsPath) {
                    if ($alias === false)
                        $alias = 'application.views.css';
                    $assetsPath = Yii::getPathOfAlias($alias);
                }
                $assetsPath = self::publishFile($assetsPath, $filename);
                Yii::app()->getClientScript()->registerCssFile($assetsPath, $media);
                break;

            case 'js':
                if (!$assetsPath) {
                    if ($alias === false)
                        $alias = 'application.views.js';
                    $assetsPath = Yii::getPathOfAlias($alias);
                }
                $assetsPath = self::publishFile($assetsPath, $filename);
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile($assetsPath);
                break;
        
            case 'jsx':
                if (!$assetsPath) {
                    if ($alias === false)
                        $alias = 'application.views.js';
                    $assetsPath = Yii::getPathOfAlias($alias);
                }
                $assetsPath = self::publishFile($assetsPath, $filename);
                $cs = Yii::app()->getClientScript();
                $cs->registerScriptFile($assetsPath, $cs->defaultScriptFilePosition, [
                    'type' => 'text/babel'
                ]);
                break;
        }
    }

    private static function publishFile($assetsPath, $filename)
    {
        // Публикуем директорию, чтобы AssetManager и registerCoreScript правильно получали файлы
        Yii::app()->assetManager->publish($assetsPath);

        // Публикуем отдельный файл, чтобы сайт подхватывал изменения в этом файле
        return Yii::app()->assetManager->publish($assetsPath."/".$filename);
    }

    /**
     * Добавить LINK
     */
    public static function importLink($filename, $alias=false, $params=array())
    {
        $assetsPath = self::getThemeAssetsPath('css', $filename, $alias);
        if (!$assetsPath) {
            if ($alias === false)
                $alias = 'application.views.css';
            $assetsPath = Yii::getPathOfAlias($alias);
        }
        
        $relation = 'stylesheet';
        if (!empty($params['relation'])) {
            $relation = $params['relation'];
            unset($params['relation']);
        }

        $type = 'text/css';
        if (!empty($params['type'])) {
            $type = $params['type'];
            unset($params['type']);
        }

        $media = null;
        if (!empty($params['media'])) {
            $media = $params['media'];
            unset($params['media']);
        }

        $assetsPath = Yii::app()->assetManager->publish($assetsPath."/".$filename);
        Yii::app()->getClientScript()->registerLinkTag($relation, $type, $assetsPath, $media, $params);
    }

    private static function getThemeAssetsPath($type, $filename, $alias)
    {
        $theme = Yii::app()->theme;
        if ($theme === null)
            return false;

        if ($alias === false)
            $alias = 'webroot.themes.'.$theme->name.'.views.'.$type;
        
        $assetsPath = Yii::getPathOfAlias($alias);
        if ($assetsPath === false)
            return false;

        if (strpos($assetsPath, Yii::app()->basePath) === 0) {
            $pathPart = substr($assetsPath, strlen(Yii::app()->basePath));
            $basePath = Yii::getPathOfAlias('webroot.themes.'.$theme->name);
            $assetsPath = $basePath.$pathPart;
        }
        if (!is_dir($assetsPath))
            return false;

        // Проверяем существование файла по этому пути, иначе придется реализовывать все шаблоны в указанном алиасе
        if (!is_file($assetsPath."/".$filename))
            return false;
        
        return $assetsPath;
    }


    /**
     * Подгрузить пакет
     * Также можно использовать так:
     * {{ void(App.getClientScript().registerPackage('somepackage')) }}
     */
    public static function registerPackages($packages)
    {
        $packages = !is_array($packages) ? [$packages] : $packages;
        foreach ($packages as $package)
            Yii::app()->getClientScript()->registerPackage($package);
        return '';
    }


    /**
     * Создать абсолютую ссылку
     */
    public static function absLink($link)
    {
        return Yii::app()->request->hostInfo.'/'.ltrim($link,'/');
    }

    /**
     * Множественная форма
     * @param  integer $num  Число для сравнения
     * @param  array $vars Варианты
     * @return string       результат
     */
    public static function plural($num, $vars)
    {
        return $num % 10 == 1 && $num % 100 != 11
            ? $vars[0]
            : $num % 10 >= 2 && $num % 10 <= 4 && ($num % 100 < 10 || $num % 100 >= 20)
                ? $vars[1]
                : $vars[2];
    }

    /**
     * Вывести дамп переменной
     */
    public static function dump($v)
    {
        echo '<pre>';
        var_dump($v);
        echo '</pre>';
    }

    /**
     * Дебаг?
     */
    public static function isDebug()
    {
        return !!YII_DEBUG;
    }

    public static function filterUnset($array, $elementName)
    {
        unset($array[$elementName]);
        return $array;
    }

    public static function filterFormatDateTime($string)
    {
        return DateHelper::formatDateTime($string);
    }

    public static function filterFormatMonthYear($string)
    {
        return DateHelper::formatMonthYear($string);
    }

    /**
     * $date - дата в формате строки
     */
    public static function filterFormatDate($date, $format='dd.LL.yyyy')
    {
        return DateHelper::formatDate($date, $format);
    }

    /**
     * $time - seconds from day start
     */
    public static function filterFormatTime($time)
    {
        return DateHelper::formatTime($time);
    }

    public static function filterTranslit($st)
    {
        // Сначала заменяем "односимвольные" фонемы.
        $st = strtr($st,"абвгдеёзийклмнопрстуфхъыэ ", "abvgdeeziyklmnoprstufh'ie_");
        $st = strtr($st,"АБВГДЕЁЗИЙКЛМНОПРСТУФХЪЫЭ", "ABVGDEEZIYKLMNOPRSTUFH'IE");
        
        // Затем - "многосимвольные".
        $st = strtr($st, array(
            "ж"=>"zh", "ц"=>"ts", "ч"=>"ch", "ш"=>"sh", 
            "щ"=>"shch","ь"=>"", "ю"=>"yu", "я"=>"ya",
            "Ж"=>"ZH", "Ц"=>"TS", "Ч"=>"CH", "Ш"=>"SH", 
            "Щ"=>"SHCH","Ь"=>"", "Ю"=>"YU", "Я"=>"YA",
            "ї"=>"i", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye"
        ));
        return $st;
    }

    public static function filterText($str)
    {
        return StringUtils::txt($str);
    }

    public static function filterExternalLink($url)
    {
        if (strpos($url, 'http')===0)
            return $url;
        return 'http://'.$url;
    }

    public static function filterFixSkype($str)
    {
        return LocalConfigHelper::fixSkype($str);
    }

    public static function filterExtractFileName($filename)
    {
        return StringUtils::extractFileName($filename);
    }
}
