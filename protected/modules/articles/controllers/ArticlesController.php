<?php

class ArticlesController extends Controller
{
    public function behaviors()
    {
        return array(
            'seoBehavior' => array(
                'class' => 'ext.YiiSEOBehavior.ESEOControllerBehavior'
            ),
        );
    }

    /**
     * Отображение статьи
     */
    public function actionShow()
    {
        $link = Yii::app()->request->getQuery('link');
        if (!$link)
            throw new CHttpException(404);

        $article = Article::model()->byLink($link)->onSite()->find();
        if (!$article)
            throw new CHttpException(404);

        $this->registerSEO($article);

        $this->setBreadcrumbs([ $article->title => CHtml::normalizeUrl([0=>'/articles/articles/show', 'id'=>$article->id]) ]);

        $this->render('/article', [
            'article' => $article
        ]);        
    }
}