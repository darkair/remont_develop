const webpack = require('webpack');
const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    context: __dirname,
    entry: {
        app: ["babel-polyfill", "./protected/webpack/src/app.js"],
    },
    output: {
        path: path.resolve(__dirname, "./protected/webpack/dist"),
        filename: "[name]-[chunkhash].js",
        chunkFilename: "[id].chunk.js",
        sourceMapFilename: "[name]-[chunkhash].js.map",
    },
    module:{
        rules: [
            {
                test: /\.vue$/,
                include: [
                    path.resolve(__dirname, "./protected/modules"),
                    path.resolve(__dirname, "./protected/webpack/src")
                ],
                loader: 'vue-loader',
                options: {
                    buble: {
                    }
                }
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "./protected/modules"),
                    path.resolve(__dirname, "./protected/webpack/src"),
                    path.resolve(__dirname, "./node_modules/v-mask/src"),
                    path.resolve(__dirname, "./node_modules/accounting-js")
                ],
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'url-loader'
            }
        ],
        loaders: [
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            underscore:'underscore'
        }),
        new CleanWebpackPlugin(['./protected/webpack/dist']),
        new ManifestPlugin({
            fileName: 'rev-manifest.json'
        })
    ]
};
