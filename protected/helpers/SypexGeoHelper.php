<?php

class SypexGeoHelper
{
    private static $sxGeo = null;

    public static function getCityName()
    {
        $sxGeo = self::getInstance();
        $ip = self::getIP();

        $city = $sxGeo->get($ip);
        if (!$city)
            return false;

        if (!isset($city['city']))
            return false;

        if (!isset($city['city']['name_ru'])) {
            return (!isset($city['city']['name_en']))
                ? false
                : $city['city']['name_en'];
        }
        return $city['city']['name_ru'];
    }

    private static function getInstance()
    {
        if (self::$sxGeo === null) {
            $path = Yii::getPathOfAlias('lib.SypexGeo.SxGeo_Updater').'/SxGeoCity.dat';
            self::$sxGeo = new SxGeo($path);
        }
        return self::$sxGeo;
    }

    private static function getIP()
    {        
        $ip = Yii::app()->getRequest()->getUserHostAddress();
        //$ip = '188.226.125.132';
        return $ip;
    }
}