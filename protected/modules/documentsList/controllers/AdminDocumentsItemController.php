<?php

Yii::import('modules.documentsList.models.*');

class AdminDocumentsItemController extends MAdminController
{
    public $modelName = 'DocumentsItem';
    public $modelHumanTitle = array('документ', 'документа', 'документов');
    public $allowedRoles = 'admin, moderator';
    public $allowedActions = 'editRaw';

    public function getEditFormElements($model)
    {
        return array_merge(
            $this->getLangField('title',    ['type' => 'textArea'])
        );
    }

    public function getTableColumns()
    {
        $attributes = array(
            'id',
            'title',
            $this->getButtonsColumn()
        );
        return $attributes;
    }
}
