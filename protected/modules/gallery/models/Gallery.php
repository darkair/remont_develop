<?php

/**
 * Галерея
 */
class Gallery extends CActiveRecord
{
    const TYPE_NONE         = 0;
    const TYPE_MAIN_PAGE    = 1;

    public $_images = [];                       // UploadedFile[]
    public $_removeGalleryImageFlags = [];      // bool

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'gallery';
    }

    public function behaviors()
    {
        return array(
            'galleryBehavior' => array(
                'class'                 => 'application.behaviors.GalleryBehavior',
                'storagePath'           => 'gallery',
                'imagesField'           => 'images',
                'galleryItemClass'      => 'GalleryItem',
            ),
            'languageBehavior' => array(
                'class'                 => 'application.behaviors.MultilingualBehavior',
                'langClassName'         => 'GalleryLang',
                'langTableName'         => 'gallery_lang',
                'langForeignKey'        => 'galleryId',
                'localizedAttributes'   => array('title'),
                'languages'             => Yii::app()->params['languages'],
                'defaultLanguage'       => Yii::app()->sourceLanguage,
                'dynamicLangClass'      => true,
            ),
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            $this->galleryBehavior->galleryLabels(),
            $this->languageBehavior->languageLabels(array(
                'title'     => 'Заголовок',
            )),
            array(
                'type'      => 'Место размещения',
                'visible'   => 'Показывать',
            )
        );
    }

    public function rules()
    {
        return array_merge(
            $this->galleryBehavior->galleryRules(),
            array(
                array('title', 'safe'),
                array('type', 'numerical', 'integerOnly'=>true),
                array('visible', 'boolean'),
                array('title', 'ext.validators.CapsValidator'),

                array('title, visible', 'safe', 'on'=>'search'),
            )
        );
    }

    public function scopes()
    {
        $alias = $this->getTableAlias();
        return array(
            'onSite' => array(
                'condition' => $alias.'.visible = 1',
            ),
        );
    }

    public function defaultScope()
    {
        return $this->languageBehavior->localizedCriteria();
    }

    public function byType($type)
    {
        $alias = $this->getTableAlias();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $alias.'.type = '.$type,
        ));
        return $this;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $alias = $this->getTableAlias();
        $criteria->compare($alias.'.visible', $this->visible);
        $criteria->compare($alias.'.title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>20,
            ),
            'sort' => array(
                'defaultOrder' => array(
                    'id' => CSort::SORT_ASC,
                )
            )
        ));
    }

    public function getGalleryStorePath()
    {
        return $this->galleryBehavior->getStorePath();
    }

    public function getGalleryImageUrl($img)
    {
        return $this->galleryBehavior->getImageUrl($img);
    }

    public function getAbsoluteImagesUrl()
    {
        return $this->galleryBehavior->getAbsoluteImagesUrl();
    }

    public function getGalleryItems()
    {
        return $this->galleryBehavior->getGalleryItems();
    }

    public function getGalleryItemByFileName($imgName)
    {
        return $this->galleryBehavior->getGalleryItemByFileName($imgName);
    }

    protected function beforeFind()
    {
        // Поддержка многих языков после загрузки модели
        $this->languageBehavior->multilang();
        return parent::beforeFind();
    }
}
