<?php

class ChildObjListControllerBehavior extends CBehavior
{
    public $childListField = '';
    public $innerField = '';
    public $childClass = null;

    public function childObjListBeforeSave(&$model)
    {
        if (empty($this->childListField))
            throw new Exception('childListField have to be filled');

        if (empty($this->innerField))
            throw new Exception('innerField have to be filled');

        if (empty($this->childClass))
            throw new Exception('childClass have to be filled');

        // Вычисляем записи, которые нужно удалить
        $modelName = get_class($model);
        $newData = $_POST[$modelName][$this->innerField];
        $oldData = $model->{$this->childListField};
        $remove = empty($oldData) ? [] : array_diff($oldData, $newData);

        $childClass = $this->childClass;

        if (!empty($remove)) {
            $table = $model->getTableSchema();
            if (!is_string($table->primaryKey))
                throw new Exception('Primary key of '.$modelName.' is not a string');

            $criteria = new CDbCriteria;
            $criteria->addInCondition($table->primaryKey, $remove);

            $models = $childClass::model()->findAll($criteria);
            foreach ($models as $obj)
                $obj->delete();
        }
    }
}