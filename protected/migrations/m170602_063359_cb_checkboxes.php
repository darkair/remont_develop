<?php

Yii::import('modules.contentBlocks.models.ContentBlocks');

class m170602_063359_cb_checkboxes extends ExtendedDbMigration
{
    public function safeUp()
    {
        $this->createContentBlock(
            'Описание чекбокса "проект"',
            'Фото, чертежи, инструкции бригаде, расчет материалов.',
            ContentBlocks::POS_CHECKBOX_PROJECT
        );
        $this->createContentBlock(
            'Подсказка чекбокса "проект"',
            'Проект поможет вам все правильно спланировать. Если у вас будет проект, вы точно будете знать, какие материалы и в каком количестве понадобятся, рабочие места будут иметь четкие инструкции и не придется ничего переделывать.',
            ContentBlocks::POS_POPOVER_PROJECT
        );


        $this->createContentBlock(
            'Описание чекбокса "комплектация"',
            'Доставим полный комплект всех материалов для вашего проекта.',
            ContentBlocks::POS_CHECKBOX_COMPLECTATION
        );
        $this->createContentBlock(
            'Подсказка чекбокса "комплектация"',
            'Комплектация',
            ContentBlocks::POS_POPOVER_COMPLECTATION
        );


        $this->createContentBlock(
            'Описание чекбокса "надзор"',
            'Мы будем вместо вас приезжать и контролировать качество выполнения работ и соответствие проекту.',
            ContentBlocks::POS_CHECKBOX_NADZOR
        );
        $this->createContentBlock(
            'Подсказка чекбокса "надзор"',
            'Надзор и контроль качества',
            ContentBlocks::POS_POPOVER_NADZOR
        );


        $this->createContentBlock(
            'Описание чекбокса "полный ремонт"',
            'Вы вибираете дизайн, передаете ключи и получаете готовую квартиру точно в срок. Мы подбираем бригаду, контролируем качество и сроки.',
            ContentBlocks::POS_CHECKBOX_FULLREMONT
        );
        $this->createContentBlock(
            'Подсказка чекбокса "полный ремонт"',
            'Полный ремонт под ключ',
            ContentBlocks::POS_POPOVER_FULLREMONT
        );
    }

    public function safeDown()
    {
        $this->deleteContentBlock(ContentBlocks::POS_CHECKBOX_PROJECT);
        $this->deleteContentBlock(ContentBlocks::POS_CHECKBOX_COMPLECTATION);
        $this->deleteContentBlock(ContentBlocks::POS_CHECKBOX_NADZOR);
        $this->deleteContentBlock(ContentBlocks::POS_CHECKBOX_FULLREMONT);
        $this->deleteContentBlock(ContentBlocks::POS_POPOVER_PROJECT);
        $this->deleteContentBlock(ContentBlocks::POS_POPOVER_COMPLECTATION);
        $this->deleteContentBlock(ContentBlocks::POS_POPOVER_NADZOR);
        $this->deleteContentBlock(ContentBlocks::POS_POPOVER_FULLREMONT);
    }
}